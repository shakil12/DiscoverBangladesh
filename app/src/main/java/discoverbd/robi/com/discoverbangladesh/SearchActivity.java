package discoverbd.robi.com.discoverbangladesh;

import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import Adapters.HorizontalCategoryRecycleViewAdapter;
import AllStatic.AllStaticDataHolder;
import Fragments.SearchFragment;
import Holder.HorizontalCategoryRecycleViewDatasetHolder;

public class SearchActivity extends AppCompatActivity implements TextView.OnEditorActionListener, TextWatcher, SearchFragment.OnSearchFragmentListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private EditText et_search;

    SearchFragment newestFragment, ratingFragment, checkedInFragment, popularityFragment;

    private RecyclerView rcyView_horizontal_selected_categories;
     ArrayList<HorizontalCategoryRecycleViewDatasetHolder> categoryDatasetHolders, selected_categoriesDatasetHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Search");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.


        newestFragment = new SearchFragment();
        newestFragment.setFragmentNo(0);
        newestFragment.add_context(this);
        ratingFragment = new SearchFragment();
        ratingFragment.setFragmentNo(1);
        ratingFragment.add_context(this);
        checkedInFragment = new SearchFragment();
        checkedInFragment.setFragmentNo(2);
        checkedInFragment.add_context(this);
        popularityFragment = new SearchFragment();
        popularityFragment.setFragmentNo(3);
        popularityFragment.add_context(this);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        et_search = (EditText) findViewById(R.id.et_search);
        et_search.setOnEditorActionListener(this);
        et_search.addTextChangedListener(this);


      /*  categoryDatasetHolders= new ArrayList<>();
        for(int i = 0; i<20; i++){
            HorizontalCategoryRecycleViewDatasetHolder holder = new HorizontalCategoryRecycleViewDatasetHolder();
            holder.setId((double)i);
            holder.setSelected(false);
            holder.setText("item " + Integer.toString(i));
            categoryDatasetHolders.add(holder);
        }*/
        add_categoies();


        selected_categoriesDatasetHolder = new ArrayList<>();
        rcyView_horizontal_selected_categories = (RecyclerView) findViewById(R.id.rcyView_horizontal_categories);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rcyView_horizontal_selected_categories.setLayoutManager(layoutManager);
        rcyView_horizontal_selected_categories.setHasFixedSize(false);
        rcyView_horizontal_selected_categories.setAdapter(new HorizontalCategoryRecycleViewAdapter(this, selected_categoriesDatasetHolder, 0));

        update_lists();

    }

    private void add_categoies(){
        categoryDatasetHolders= new ArrayList<>();
        if(AllStaticDataHolder.ALL_ATTRACTIONS_TAGS != null){
            for (HorizontalCategoryRecycleViewDatasetHolder dt :
                    AllStaticDataHolder.ALL_ATTRACTIONS_TAGS) {
                HorizontalCategoryRecycleViewDatasetHolder dd = new HorizontalCategoryRecycleViewDatasetHolder();
                dd.setId(dt.getId());
                dd.setText(dt.getText());
                dd.setSelected(false);
                categoryDatasetHolders.add(dd);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        update_lists();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        update_lists();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        else if(item.getItemId() == R.id.action_search_category){


            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.search_category_layout,null);

            LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll_search_category);

            for (int i=0; i<categoryDatasetHolders.size(); i++) {
                HorizontalCategoryRecycleViewDatasetHolder holder = categoryDatasetHolders.get(i);

                AppCompatCheckBox cb = new AppCompatCheckBox(view.getContext());
                cb.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                cb.setText(holder.getText());
                cb.setPadding(0,5,0,5);
                cb.setChecked(holder.getSelected());

                cb.setTag(i);
                cb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AppCompatCheckBox cb = (AppCompatCheckBox) view;
                        int i = (int) view.getTag();
                        HorizontalCategoryRecycleViewDatasetHolder holder = categoryDatasetHolders.get(i);
                        holder.setSelected(cb.isChecked());
                    }
                });
                ll.addView(cb);
            }


            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Select Category");
            builder.setView(view);
            builder.setCancelable(false);
            builder.setPositiveButton("Search", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if(selected_categoriesDatasetHolder.size() > 0)
                        selected_categoriesDatasetHolder.clear();
                    for (HorizontalCategoryRecycleViewDatasetHolder holder :
                            categoryDatasetHolders) {
                        if (holder.getSelected()){
                            selected_categoriesDatasetHolder.add(holder);
                        }
                    }
                    rcyView_horizontal_selected_categories.getAdapter().notifyDataSetChanged();

                    updateSearch();
                }
            });
            builder.create().show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void updateSearch(){
        update_lists();
    }

    // category select listener
   /* @Override
    public void onHorizontalCategoryRecycleViewItemClick(int tag, int position) {
        Log.d("Fahim", Integer.toString(position));
        HorizontalCategoryRecycleViewDatasetHolder holder =  selected_categoriesDatasetHolder.get(position);
        holder.setSelected(false);
        selected_categoriesDatasetHolder.remove(position);
        rcyView_horizontal_selected_categories.getAdapter().notifyDataSetChanged();
    }
*/
    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i == EditorInfo.IME_ACTION_SEARCH) {
            update_lists();
            return true;
        }
        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        update_lists();
    }


    private void update_lists(){
        newestFragment.update_newsest(et_search.getText().toString(),selected_categoriesDatasetHolder);
        ratingFragment.update_rating(et_search.getText().toString(),selected_categoriesDatasetHolder);
        checkedInFragment.update_checkedIn(et_search.getText().toString(),selected_categoriesDatasetHolder);
        popularityFragment.update_popularity(et_search.getText().toString(),selected_categoriesDatasetHolder);
    }

    @Override
    public void onSearchFragmentBookmarkedClicked() {
        update_lists();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    /*public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }


        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_search, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }*/

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return newestFragment;
                case 1:
                    return ratingFragment;
                case 2:
                    return checkedInFragment;
                case 3:
                    return popularityFragment;
            }
            return newestFragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Newest";
                case 1:
                    return "Rating";
                case 2:
                    return "Checked in";
                case 3:
                    return "Popularity";
            }
            return null;
        }
    }
}
