package discoverbd.robi.com.discoverbangladesh;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import AllStatic.AllStaticDataHolder;
import AllStatic.AllStaticMethods;
import AllStatic.ArticlesLink;
import AllStatic.AttractionsLink;
import AllStatic.LoginLinks;
import Fragments.ArticlesHomeFragment;
import Fragments.AttractionsHomeFragment;
import Fragments.BookmarksHomeFragment;
import Fragments.WishlistHomeFragment;
import Holder.ArticlesRecycleViewDatasetHolder;
import Holder.AttractionsGridViewDatasetHolder;
import Holder.HorizontalCategoryRecycleViewDatasetHolder;
import de.hdodenhof.circleimageview.CircleImageView;


public class HomeScreenActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener, AttractionsHomeFragment.OnAttractionsMainFragmentListener, WishlistHomeFragment.OnWishlistMainFragmentListener, ArticlesHomeFragment.OnArticlesMainFragmentListener, BookmarksHomeFragment.OnBookmarksMainFragmentListener {


    private SectionsPagerAdapter mSectionsPagerAdapter;


    private CircleImageView civ_profile_image;
    private TextView tv_user_name, tv_user_email;


    ////fragments
    private AttractionsHomeFragment attractionsFragment;
    private ArticlesHomeFragment articlesFragment;
    private WishlistHomeFragment wishlistFragment;
    private BookmarksHomeFragment bookmarksFragment;


    private ViewPager mViewPager;


    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        //toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);

        //tabs
        attractionsFragment = new AttractionsHomeFragment();
        attractionsFragment.add_context(this);
        articlesFragment = new ArticlesHomeFragment();
        articlesFragment.add_context(this);
        wishlistFragment = new WishlistHomeFragment();
        wishlistFragment.add_context(this);
        bookmarksFragment = new BookmarksHomeFragment();
        bookmarksFragment.add_context(this);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//////////////////////////  to notify fragment is navigated so that we can check for new data entry//////////////////
                if(position == 0) {
                    //Log.d("Fahim","navigated");
                    attractionsFragment.update_list();
                }
                else if(position == 1)
                {
                    articlesFragment.update_list();
                }
                else if(position == 2)
                {
                    wishlistFragment.update_wishlist();
                }
                else if(position == 3){
                    bookmarksFragment.update_list();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);


        //nav drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View v = navigationView.getHeaderView(0);

        //initialize
        civ_profile_image = (CircleImageView) v.findViewById(R.id.imageView_profile);
        tv_user_name = (TextView) v.findViewById(R.id.tv_user_name);
        tv_user_email = (TextView) v.findViewById(R.id.tv_user_email);


        load_user_data_in_nav_drawer_header();

        ///google auth
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //////////////load
        new LoadFeaturedAttractionAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new LoadAllAttractionsTagsAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new LoadAllAttractionsAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        new LoadAllArticlesAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    @Override
    protected void onResume() {
        super.onResume();
        attractionsFragment.update_list();
        articlesFragment.update_list();
        wishlistFragment.update_wishlist();
        bookmarksFragment.update_list();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        attractionsFragment.add_context(this);
        articlesFragment.add_context(this);
        wishlistFragment.add_context(this);
        bookmarksFragment.add_context(this);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Exit");
            builder.setMessage("Do you really want to exit?");
            builder.setCancelable(true);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                    homeIntent.addCategory( Intent.CATEGORY_HOME );
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            Intent intent = new Intent(HomeScreenActivity.this,SearchActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_attractions) {
            mViewPager.setCurrentItem(0);
        } else if (id == R.id.nav_articles) {
            mViewPager.setCurrentItem(1);
        } else if (id == R.id.nav_wishlist) {
            mViewPager.setCurrentItem(2);
        } else if (id == R.id.nav_bookmarks) {
            mViewPager.setCurrentItem(3);
        } else if (id == R.id.nav_upload) {
            Intent intent = new Intent(HomeScreenActivity.this, UploadActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_logout) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Log out");
            builder.setMessage("Do you really want to log out?");
            builder.setCancelable(true);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    AllStaticMethods.set_user_logout(HomeScreenActivity.this);
                    if(AccessToken.getCurrentAccessToken() != null)
                        LoginManager.getInstance().logOut();
                    else if(mGoogleApiClient.isConnected())
                        googleSignOut();

                    Intent intent = new Intent(HomeScreenActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void googleSignOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // ...
                    }
                });
    }


    private void load_user_data_in_nav_drawer_header()
    {
        tv_user_name.setText(AllStaticMethods.get_user_name(this));
        String s = AllStaticMethods.get_user_email(this);
        tv_user_email.setText(s);

        try {
            // Create MD5 Hash
          /*  MessageDigest digest;
            digest = MessageDigest.getInstance("MD5");
            digest.reset();
            digest.update(s.getBytes());
            byte[] a = digest.digest();
            int len = a.length;
            StringBuilder sb = new StringBuilder(len << 1);
            for (int i = 0; i < len; i++) {
                sb.append(Character.forDigit((a[i] & 0xf0) >> 4, 16));
                sb.append(Character.forDigit(a[i] & 0x0f, 16));
            }


            String gravatarUrl = "http://www.gravatar.com/avatar/" + sb.toString() + "?s=204&d=404"; */
           // Log.d("Fahim",gravatarUrl);
            Picasso.with(this)
                    .load(AllStaticMethods.get_gravatar_link(s))
                    .placeholder(R.drawable.ic_account_circle_black_24dp)
                    .into(civ_profile_image);
        } catch (Exception e) {
            civ_profile_image.setImageResource(R.drawable.drawable_default_image);
        }
    }




    public class SectionsPagerAdapter extends FragmentPagerAdapter {



        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0) {
                return attractionsFragment;
            }
            else if(position == 1){
                return articlesFragment;
            }
            else if(position == 2){
                return wishlistFragment;
            }
            else if(position == 3){
                return bookmarksFragment;
            }
            return attractionsFragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Attractions";
                case 1:
                    return "Articles";
                case 2:
                    return "Wishlist";
                case 3:
                    return "Bookmarks";

            }
            return null;
        }
    }



    //// ALL ATTRACTIONS ASYNC
    private class LoadAllAttractionsAsync extends AsyncTask<Void, Void, Void>{

        private boolean load_completed = false;
        private ArrayList<AttractionsGridViewDatasetHolder> gridDatasetHolder;
        private String result;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            load_completed = false;
            gridDatasetHolder = new ArrayList<>();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            result = new HttpDataHandler().GetHTTPData(AttractionsLink.ALL_ATTRACTIONS_LINK + AllStaticMethods.get_api_token(HomeScreenActivity.this) );
            if(result != null){
                load_json();
            }
            else {
                if(load_from_cache())
                    load_json();
                else
                    load_completed = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(load_completed){
                load_to_cache();

                AllStaticDataHolder.ALL_ATTRACTIONS = gridDatasetHolder;
                attractionsFragment.update_list();
                wishlistFragment.update_wishlist();
            }
        }


        private void load_json(){
            try {
                JSONArray jsonArray = new JSONArray(result);


                for(int i =0; i<jsonArray.length();i++){
                    AttractionsGridViewDatasetHolder datasetHolder = new AttractionsGridViewDatasetHolder();

                    JSONObject object = jsonArray.getJSONObject(i);
                    datasetHolder.setId((double) object.getInt("id"));
                    datasetHolder.setTitle(object.getString("title"));
                    datasetHolder.setShort_des(object.getString("description"));

                    String st_date = object.getString("updated_at");
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                    SimpleDateFormat format2 = new SimpleDateFormat("dd MMM,yyyy",Locale.US);
                    Date date = format1.parse(st_date);

                    datasetHolder.setCreated_date(date);
                    datasetHolder.setTime(format2.format(date));

                    datasetHolder.setRating_value(object.getInt("rating_value"));
                    datasetHolder.setReviews_value(object.getInt("reviews_count"));
////////////////////////

                    String shr = LoginLinks.BASE_URL + "/attractions/" + object.getString("slug");
                    datasetHolder.setShareLink(shr);

                    datasetHolder.setIs_checkedIn(false);//


                    String st = object.getString("featured_image");
                    if(st.startsWith("/")){
                        st= LoginLinks.BASE_URL + st;
                    }
                    datasetHolder.setImage_link(st);

                    JSONObject loc_obj = object.getJSONObject("location");
                    datasetHolder.setLocation(loc_obj.getString("title"));

                    JSONArray wishlist_array = object.getJSONArray("wishlists");
                    if(wishlist_array.length()==0)
                        datasetHolder.setIs_wished(false);
                    else
                        datasetHolder.setIs_wished(true);

                    JSONArray tag_array = object.getJSONArray("tags");
                    for(int j = 0; j<tag_array.length();j++){
                        JSONObject tag_obj = tag_array.getJSONObject(j);
                        HorizontalCategoryRecycleViewDatasetHolder dst = new HorizontalCategoryRecycleViewDatasetHolder();
                        dst.setId((double) tag_obj.getInt("tag_id"));
                        dst.setSelected(true);

                        JSONObject j_o = tag_obj.getJSONObject("tag");
                        dst.setText(j_o.getString("title"));

                        datasetHolder.add_to_tag_list(dst);
                    }
                    gridDatasetHolder.add(datasetHolder);
                }


                load_completed = true;
            }catch (Exception e){

                load_completed = false;
            }
        }

        private void load_to_cache(){
            try {
                File cacheDir = getCacheDir();
                File file = new File(cacheDir.getAbsolutePath(), "AttractionList.dbd");
                FileOutputStream fout = new FileOutputStream(file);
                fout.flush();
                fout.write(result.getBytes());
                fout.close();
            }catch (Exception e){}
        }

        private boolean load_from_cache(){
            try {
                File cacheDir = getCacheDir();
                File file = new File(cacheDir.getAbsolutePath(), "AttractionList.dbd");
                FileInputStream fin = new FileInputStream(file);
                InputStream in = new BufferedInputStream(fin);

                // Read the BufferedInputStream
                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    sb.append(line);
                }
                result = sb.toString();
                // End reading...............
                in.close();
                r.close();
                fin.close();
                return true;

            }catch (Exception e){
                return false;
            }
        }
    }

    //// FEATURED ATTRACTION ASYNC
    private class LoadFeaturedAttractionAsync extends AsyncTask<Void, Void, Void>{

        private boolean load_completed = false;
        private AttractionsGridViewDatasetHolder gridDatasetHolder;
        private String result;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            load_completed = false;
            gridDatasetHolder = new AttractionsGridViewDatasetHolder();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            result = new HttpDataHandler().GetHTTPData(AttractionsLink.FEATURED_ATTRACTION_LINK+ AllStaticMethods.get_api_token(HomeScreenActivity.this) );
            if(result != null){
                load_json();
            }
            else {
                if(load_from_cache())
                    load_json();
                else
                    load_completed = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(load_completed){
                load_to_cache();

                AllStaticDataHolder.FEATURED_ATTRACTION = gridDatasetHolder;
               // attractionsFragment.update_list();
               // wishlistFragment.update_wishlist();
            }
        }


        private void load_json(){
            try {
                    JSONObject object = new JSONObject(result);
                    gridDatasetHolder.setId((double) object.getInt("id"));
                    gridDatasetHolder.setTitle(object.getString("title"));
                    gridDatasetHolder.setShort_des(object.getString("description"));

                    String st_date = object.getString("updated_at");
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                    SimpleDateFormat format2 = new SimpleDateFormat("dd MMM,yyyy",Locale.US);
                    Date date = format1.parse(st_date);

                    gridDatasetHolder.setCreated_date(date);
                    gridDatasetHolder.setTime(format2.format(date));

                    gridDatasetHolder.setRating_value(object.getInt("rating_value"));
                    gridDatasetHolder.setReviews_value(object.getInt("reviews_count"));
////////////////////////
                    String shr = LoginLinks.BASE_URL + "/attractions/" + object.getString("slug");
                    gridDatasetHolder.setShareLink(shr);
                    gridDatasetHolder.setIs_checkedIn(false);//


                    String st = object.getString("featured_image");
                    if(st.startsWith("/")){
                        st= LoginLinks.BASE_URL + st;
                    }
                    gridDatasetHolder.setImage_link(st);

                    JSONObject loc_obj = object.getJSONObject("location");
                    gridDatasetHolder.setLocation(loc_obj.getString("title"));

                    JSONArray wishlist_array = object.getJSONArray("wishlists");
                    if(wishlist_array.length()==0)
                        gridDatasetHolder.setIs_wished(false);
                    else
                        gridDatasetHolder.setIs_wished(true);

                    JSONArray tag_array = object.getJSONArray("tags");
                    for(int j = 0; j<tag_array.length();j++){
                        JSONObject tag_obj = tag_array.getJSONObject(j);
                        HorizontalCategoryRecycleViewDatasetHolder dst = new HorizontalCategoryRecycleViewDatasetHolder();
                        dst.setId((double) tag_obj.getInt("tag_id"));
                        dst.setSelected(true);

                        JSONObject j_o = tag_obj.getJSONObject("tag");
                        dst.setText(j_o.getString("title"));

                        gridDatasetHolder.add_to_tag_list(dst);
                    }

                load_completed = true;
            }catch (Exception e){

                load_completed = false;
            }
        }

        private void load_to_cache(){
            try {
                File cacheDir = getCacheDir();
                File file = new File(cacheDir.getAbsolutePath(), "FeaturedAttraction.dbd");
                FileOutputStream fout = new FileOutputStream(file);
                fout.flush();
                fout.write(result.getBytes());
                fout.close();
            }catch (Exception e){}
        }

        private boolean load_from_cache(){
            try {
                File cacheDir = getCacheDir();
                File file = new File(cacheDir.getAbsolutePath(), "FeaturedAttraction.dbd");
                FileInputStream fin = new FileInputStream(file);
                InputStream in = new BufferedInputStream(fin);

                // Read the BufferedInputStream
                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    sb.append(line);
                }
                result = sb.toString();
                // End reading...............
                in.close();
                r.close();
                fin.close();
                return true;

            }catch (Exception e){
                return false;
            }
        }
    }




    //// ALL ARTICLES ASYNC
    private class LoadAllArticlesAsync extends AsyncTask<Void, Void, Void>{

        private boolean load_completed = false;
        private ArrayList<ArticlesRecycleViewDatasetHolder> gridDatasetHolder;
        private String result;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            load_completed = false;
            gridDatasetHolder = new ArrayList<>();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            result = new HttpDataHandler().GetHTTPData(ArticlesLink.ALL_ARTICLES_LINK + AllStaticMethods.get_api_token(HomeScreenActivity.this) );
            if(result != null){
                load_json();
            }
            else {
                if(load_from_cache())
                    load_json();
                else
                    load_completed = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(load_completed){
                load_to_cache();

                AllStaticDataHolder.ALL_ARTICLES = gridDatasetHolder;
                articlesFragment.update_list();
                bookmarksFragment.update_list();
            }
        }


        private void load_json(){
            try {
                JSONArray jsonArray = new JSONArray(result);


                for(int i =0; i<jsonArray.length();i++){
                    ArticlesRecycleViewDatasetHolder datasetHolder = new ArticlesRecycleViewDatasetHolder();

                    JSONObject object = jsonArray.getJSONObject(i);
                    datasetHolder.setId((double) object.getInt("id"));
                    datasetHolder.setTitle(object.getString("title"));
                    datasetHolder.setDescription(object.getString("description"));

////////////////////////
                    JSONObject j_user = object.getJSONObject("user");

                    datasetHolder.setAuthor(j_user.getString("name"));
                    datasetHolder.setAuthor_email(j_user.getString("email"));
                    datasetHolder.setLike_num(Integer.toString(object.getInt("likes_count")));

                    String st_date = object.getString("created_at");
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                    SimpleDateFormat format2 = new SimpleDateFormat("dd MMM,yyyy",Locale.US);
                    Date date = format1.parse(st_date);
                    datasetHolder.setTime(format2.format(date));

                    String shr = LoginLinks.BASE_URL + "/articles/" + object.getString("slug");
                    datasetHolder.setShareLink(shr);
                    datasetHolder.setBookmarked(false);// not needed for 1st phase

                    JSONArray jArray_like = object.getJSONArray("likes");
                    if(jArray_like.length() == 0)
                        datasetHolder.setLiked(false);
                    else
                        datasetHolder.setLiked(true);


                    String st = object.getString("featured_image");
                    if(st.startsWith("/")){
                        st= LoginLinks.BASE_URL + st;
                    }
                    datasetHolder.setImage_link(st);



                    JSONArray tag_array = object.getJSONArray("tags");
                    for(int j = 0; j<tag_array.length();j++){
                        JSONObject tag_obj = tag_array.getJSONObject(j);
                        HorizontalCategoryRecycleViewDatasetHolder dst = new HorizontalCategoryRecycleViewDatasetHolder();
                        dst.setId((double) tag_obj.getInt("tag_id"));
                        dst.setSelected(true);

                        JSONObject j_o = tag_obj.getJSONObject("tag");
                        dst.setText(j_o.getString("title"));

                        datasetHolder.add_to_tag_list(dst);
                    }
                    gridDatasetHolder.add(datasetHolder);
                }


                load_completed = true;
            }catch (Exception e){

                load_completed = false;
            }
        }

        private void load_to_cache(){
            try {
                File cacheDir = getCacheDir();
                File file = new File(cacheDir.getAbsolutePath(), "ArticleList.dbd");
                FileOutputStream fout = new FileOutputStream(file);
                fout.flush();
                fout.write(result.getBytes());
                fout.close();
            }catch (Exception e){}
        }

        private boolean load_from_cache(){
            try {
                File cacheDir = getCacheDir();
                File file = new File(cacheDir.getAbsolutePath(), "ArticleList.dbd");
                FileInputStream fin = new FileInputStream(file);
                InputStream in = new BufferedInputStream(fin);

                // Read the BufferedInputStream
                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    sb.append(line);
                }
                result = sb.toString();
                // End reading...............
                in.close();
                r.close();
                fin.close();
                return true;

            }catch (Exception e){
                return false;
            }
        }
    }


    //// ALL ATTRACTIONS TAG ASYNC
    private class LoadAllAttractionsTagsAsync extends AsyncTask<Void, Void, Void>{

        private boolean load_completed = false;
        private ArrayList<HorizontalCategoryRecycleViewDatasetHolder> gridDatasetHolder, articlesTags;
        private String result;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            load_completed = false;
            gridDatasetHolder = new ArrayList<>();
            articlesTags = new ArrayList<>();
           // Log.d("Fahim","On Pre");
        }

        @Override
        protected Void doInBackground(Void... voids) {
           // Log.d("Fahim","On doin");
            result = new HttpDataHandler().GetHTTPData(AttractionsLink.ALL_ATTRACTIONS_TAGS_LINK + AllStaticMethods.get_api_token(HomeScreenActivity.this) );
            if(result != null){
                load_json();
            }
            else {
                if(load_from_cache())
                    load_json();
                else
                    load_completed = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
         //   Log.d("Fahim","On Post");
            if(load_completed){
                load_to_cache();

                AllStaticDataHolder.ALL_ATTRACTIONS_TAGS = gridDatasetHolder;
                AllStaticDataHolder.ALL_ARTICLES_TAGS = articlesTags;
                //articlesFragment.update_list();
               // bookmarksFragment.update_list();
                attractionsFragment.update_tag_list();
                articlesFragment.update_tag_list();
            }
        }


        private void load_json(){
            try {
                JSONArray jsonArray = new JSONArray(result);


                for(int i =0; i<jsonArray.length();i++){
                    HorizontalCategoryRecycleViewDatasetHolder datasetHolder = new HorizontalCategoryRecycleViewDatasetHolder();
                    HorizontalCategoryRecycleViewDatasetHolder dt_article = new HorizontalCategoryRecycleViewDatasetHolder();


                    JSONObject object = jsonArray.getJSONObject(i);
                    datasetHolder.setId((double) object.getInt("id"));
                    datasetHolder.setText(object.getString("title"));
                    datasetHolder.setSelected(true);

                    dt_article.setId((double) object.getInt("id"));
                    dt_article.setText(object.getString("title"));
                    dt_article.setSelected(true);


                    gridDatasetHolder.add(datasetHolder);
                    articlesTags.add(dt_article);
                }


                load_completed = true;
            }catch (Exception e){

                load_completed = false;
            }
        }

        private void load_to_cache(){
            try {
                File cacheDir = getCacheDir();
                File file = new File(cacheDir.getAbsolutePath(), "AttractionTag.dbd");
                FileOutputStream fout = new FileOutputStream(file);
                fout.flush();
                fout.write(result.getBytes());
                fout.close();
            }catch (Exception e){}
        }

        private boolean load_from_cache(){
            try {
                File cacheDir = getCacheDir();
                File file = new File(cacheDir.getAbsolutePath(), "AttractionTag.dbd");
                FileInputStream fin = new FileInputStream(file);
                InputStream in = new BufferedInputStream(fin);

                // Read the BufferedInputStream
                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    sb.append(line);
                }
                result = sb.toString();
                // End reading...............
                in.close();
                r.close();
                fin.close();
                return true;

            }catch (Exception e){
                return false;
            }
        }
    }



    ///fragment interfaces
    @Override
    public void onAttractionFragmentWishlistClicked() {
        wishlistFragment.update_wishlist();
    }

    @Override
    public void onWishlistFragmentWishlistClicked() {
        attractionsFragment.update_list();
    }

    @Override
    public void onArticlesFragmentBookmarkedClicked() {
        bookmarksFragment.update_list();
    }

    @Override
    public void onBookmarksFragmentBookmarkedClicked() {
        articlesFragment.update_list();
    }
}
