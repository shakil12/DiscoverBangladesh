package discoverbd.robi.com.discoverbangladesh;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.HashMap;

import AllStatic.AllStaticMethods;
import AllStatic.LoginLinks;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {

    LinearLayout ll_main;
    private ProgressDialog progressDialog_wait;
    Button btn_signup;
    EditText et_name, et_userName, et_email, et_password, et_confirmPassword;
    private ImageButton ib_fb;


    // for user fblogin
    private CallbackManager callbackManager;
    private LoginButton btn_loginfb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        getSupportActionBar().setTitle("Sign Up");

        ll_main = (LinearLayout) findViewById(R.id.activity_sign_up);

        btn_signup = (Button) findViewById(R.id.btn_sign_up);
        btn_signup.setOnClickListener(this);

        et_name = (EditText) findViewById(R.id.tv_signUp_name);
        et_userName = (EditText) findViewById(R.id.tv_signUp_userName);
        et_email = (EditText) findViewById(R.id.tv_signUp_email);
        et_password = (EditText) findViewById(R.id.tv_signUp_password);
        et_confirmPassword = (EditText) findViewById(R.id.tv_signUp_confirmPassword);

        et_name.addTextChangedListener(this);
        et_userName.addTextChangedListener(this);
        et_email.addTextChangedListener(this);
        et_password.addTextChangedListener(this);
        et_confirmPassword.addTextChangedListener(this);


        btn_loginfb = (LoginButton) findViewById(R.id.btn_login_fb);
        callbackManager = CallbackManager.Factory.create();
        btn_loginfb.setReadPermissions("public_profile", "email");
        btn_loginfb.registerCallback(callbackManager, fbCallBack);


        ib_fb = (ImageButton) findViewById(R.id.ib_login_fb);
        ib_fb.setOnClickListener(this);

        try {
            et_name.setText(getIntent().getStringExtra("name"));
            et_email.setText(getIntent().getStringExtra("email"));
        }catch (Exception e){}

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            ll_main.setBackgroundResource(R.drawable.signup_back_hori);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            ll_main.setBackgroundResource(R.drawable.signup_back);
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_sign_up){
            check_provided_info();
        }
        else if(view.getId() == R.id.ib_login_fb){

            progressDialog_wait = new ProgressDialog(SignUpActivity.this);
            progressDialog_wait.setMessage("Loading....");
            progressDialog_wait.show();

            btn_loginfb.performClick();

            btn_loginfb.setPressed(true);

            btn_loginfb.setPressed(false);
        }
    }


    private void check_provided_info()
    {
        if((et_name.getText().toString().length()==0)
                || (et_userName.getText().toString().length()==0)
                || (et_email.getText().toString().length()==0)
                || (et_password.getText().toString().length()==0)
                || (et_confirmPassword.getText().toString().length()==0)){

            AllStaticMethods.show_alert(this,"Incomplete Field","Please complete all fields");
        }
        else if (et_userName.getText().length()<2
                || !AllStaticMethods.is_valid_userName(et_userName.getText().toString())){
            AllStaticMethods.show_alert(this,"Invalid User Name","Please check your user name.");
        }
        else if(!AllStaticMethods.is_valid_email(et_email.getText().toString())){
            AllStaticMethods.show_alert(this,"Invalid Email", "Please insert a valid email");
        }
        else if(!et_password.getText().toString().equals(et_confirmPassword.getText().toString())
                || et_password.getText().toString().length()<6
                || et_confirmPassword.getText().toString().length()<6){
            AllStaticMethods.show_alert(this,"Wrong Password","Please check your password & confirm password.");
        }
        else {
            new CreateAccountAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if(et_name.getText().toString().length()<=0){
            et_name.setError("Required");
        }
        else {
            et_name.setError(null);
        }

        if(et_userName.getText().toString().length()<2){
            et_userName.setError("AT least 2 Characters");
        }
        else if(!AllStaticMethods.is_valid_userName(et_userName.getText().toString())){
            et_userName.setError("Invalid user name. Only can contain \'a-z\' , \'A-Z\' , \'_\'");
        }
        else {
            et_userName.setError(null);
        }

        if(et_email.getText().toString().length()<=0){
            et_email.setError("Required");
        }
        else if(!AllStaticMethods.is_valid_email(et_email.getText().toString())){
            et_email.setError("Invalid Email");
        }
        else {
            et_email.setError(null);
        }

        if(et_password.getText().toString().length()<=0){
            et_password.setError("Required");
        }
        else if(et_password.getText().toString().length()<6){
            et_password.setError("At least 6 characters required.");
        }
        else {
            et_password.setError(null);
        }

        if(et_confirmPassword.getText().toString().length()<=0){
            et_confirmPassword.setError("Required");
        }
        else if(et_confirmPassword.getText().toString().length()<6){
            et_confirmPassword.setError("At least 6 characters required.");
        }
        else if(!et_password.getText().toString().equals(et_confirmPassword.getText().toString())){
            et_confirmPassword.setError("Passwords don\'t match");
        }
        else {
            et_confirmPassword.setError(null);
        }
    }


    private void navigate_to_home_screen(){
        Intent intent = new Intent(this,HomeScreenActivity.class);
        startActivity(intent);
    }



/////////////
///////////////fb callback
private FacebookCallback<LoginResult> fbCallBack = new FacebookCallback<LoginResult>(){

    @Override
    public void onSuccess(LoginResult loginResult) {

        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        String  email = null, name = null ;
                        try {

                            progressDialog_wait.cancel();


                            try{
                                name = object.getString("name");
                            }catch (Exception e) {
                                e.printStackTrace();
                                name = "";
                            }

                            try {
                                email = object.getString("email");
                            }catch (Exception e) {
                                e.printStackTrace();
                                email = null;

                            }

                            if(email != null && name != null) {
                                progressDialog_wait.cancel();

                                /////////////////////////////
                                et_email.setText(email);
                                et_name.setText(name);

                            }else {
                                progressDialog_wait.cancel();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, email, gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();

    }

    @Override
    public void onCancel() {
        progressDialog_wait.cancel();
    }

    @Override
    public void onError(FacebookException error) {
        progressDialog_wait.cancel();
        AllStaticMethods.show_alert(SignUpActivity.this,"Error","Something goes wrong. Please try again.");
    }
};



/////////////////// async task//////////////////////////////////////
    private class CreateAccountAsync extends AsyncTask<Void, Void, Void> {
        private boolean accountCreated = false;
        private String st_name, st_userName, st_email, st_password, st_passConfirm, token, name, email;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            accountCreated = false;

            st_name = et_name.getText().toString();
            st_userName = et_userName.getText().toString();
            st_email = et_email.getText().toString();
            st_password = et_password.getText().toString();
            st_passConfirm = et_confirmPassword.getText().toString();


            progressDialog_wait = new ProgressDialog(SignUpActivity.this);
            progressDialog_wait.setMessage("Loading....");
            progressDialog_wait.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("name", st_name);
            hashMap.put("username",st_userName);
            hashMap.put("email",st_email);
            hashMap.put("password",st_password);
            hashMap.put("password_confirmation",st_passConfirm);

            String result = new HttpDataHandler().performPostCall(LoginLinks.BASE_URL + LoginLinks.SIGN_UP_LINK , hashMap);
            if(result != null){
                try {
                    JSONObject jsonObject = new JSONObject(result);

                    boolean is_success = jsonObject.getBoolean("success");

                    if(is_success){
                        JSONObject object = jsonObject.getJSONObject("data");
                        token = object.getString("api_token");
                        name = object.getString("name");
                        email = object.getString("email");
                        accountCreated = true;
                    }
                    else {
                        accountCreated = false;
                    }
                }catch (Exception e){

                    accountCreated = false;
                }
            }
            else {

                accountCreated = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            progressDialog_wait.cancel();

            if(accountCreated){
                AllStaticMethods.set_user_login_info(SignUpActivity.this,token,name,email);
                navigate_to_home_screen();
            }

            else {
                AllStaticMethods.show_alert(SignUpActivity.this,"Error","Account Creation failed. Please try again.");
            }
        }
    }



}
