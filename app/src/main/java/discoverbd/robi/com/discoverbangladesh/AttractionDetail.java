package discoverbd.robi.com.discoverbangladesh;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import Adapter.AdapterOverView;
import Adapter.AdapterViewPager;
import AllStatic.AllStaticDataHolder;
import AllStatic.AllStaticMethods;
import AllStatic.AttractionDetailApi;
import Fragment.FragmentArticle;
import Fragment.FragmentGallery;
import Fragment.FragmentMap;
import Fragment.FragmentOverview;
import Fragment.FragmentReview;
import Helper.AppBarStateChangeListener;
import Holder.AttractionsGridViewDatasetHolder;
import ModelClass.EmnoModel;
import ModelClass.OverviewModel;
import ModelClass.ReviewModel;

import android.support.v7.graphics.Palette;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AttractionDetail extends AppCompatActivity implements FragmentOverview.OnTextLocDetailOutOfScreen,
        FragmentReview.OnReviewTopOutOfSpace,
        FragmentOverview.OnActivityCreatedOverView,
        FragmentReview.OnActivityCreatedReview,
        FragmentGallery.OnActivityCreatedGallery,
        FragmentMap.OnActivityCreatedMap,
        FragmentArticle.OnActivityCreatedArticle
                                                                   {
    // for toolbar
    private ImageView imgv_backButton;
    private TextView txtv_tlbarTitle;
    private boolean toolbarCollapsed = false;
    private int currentPage = 0;

    // for tab
    private Toolbar toolbar;
    private TabLayout tabs;
    private ViewPager viewPager;
    private AdapterViewPager adapterViewPager;



    // for overView
    private AppBarLayout appBarLayout_attraction;
    private FragmentOverview fragmentOverview;

    // for scroll and collapse toolbar other than recyclerview scroll
    private boolean collapseOtherThanRecv = false;

    // for ownView
    private ImageView imgv_htab_header;
    private ToggleButton tb_ownWishList, tb_ownCheckIn;
    private String ownWishList = null, ownCheckIn = null;
    public boolean dataLoaded = false;

    // for attraction detail overview data
    public String   title  = null, name  = null, description = null, latitude = null,
                    longitude = null, reviewsCount = null, ratingValue = null,
                    rating_value = null, checkInCount = null, wishlistsCount = null,
                    featuredImageUrl = null, shareLink = null, attraction_id = null;
    public List<OverviewModel> listOverViews;

    // for review data
    public List<ReviewModel> listReviews;

    // for article data
    public List<EmnoModel> listArticles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attraction_detail);
        /*toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();*/
        // for collapsing toolbar
        final Toolbar toolbar = (Toolbar) findViewById(R.id.htab_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // for collapsing toolbar layout

        appBarLayout_attraction = (AppBarLayout) findViewById(R.id.appBarLayout_attraction);

        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.htab_collapse_toolbar);
        collapsingToolbarLayout.setTitleEnabled(false);

        ImageView header = (ImageView) findViewById(R.id.htab_header);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                R.mipmap.test_locimg1);

        /*Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @SuppressWarnings("ResourceType")
            @Override
            public void onGenerated(Palette palette) {

                int vibrantColor = palette.getVibrantColor(R.color.colorPrimary);
                int vibrantDarkColor = palette.getDarkVibrantColor(R.color.colorPrimaryDark);
                collapsingToolbarLayout.setContentScrimColor(R.color.colorPrimary);
                collapsingToolbarLayout.setStatusBarScrimColor(R.color.colorPrimaryDark);
            }
        });*/

        //FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //fab.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View view) {
        //        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
        //                .setAction("Action", null).show();
        //    }
        //});
        initCustomToolbarLayout();

        // retrieing real id from parent activity
        if(getIntent() != null) {
            attraction_id = String.valueOf((getIntent().getIntExtra("id", 1)));
            Log.d("SHAKIL", "attraction id "+attraction_id);
        }

        init_layout();
        setAppBarLayout_attraction();
        // api calling
        new AttractionDetailAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void setAppBarLayout_attraction() {
        // for collapsing other layout other than toolbar we have to add a scrollChangeListener to see if the toolbar
        // is collapsed so we also make some of our views gone so user can see the recyclerview list properly
        // fragmentOverview = (FragmentOverview) getSupportFragmentManager().
        // findFragmentById(Integer.parseInt("android:switcher:" + R.id.viewPager + ":"
        //        + viewPager.getCurrentItem()));
        appBarLayout_attraction.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                AdapterViewPager adapterViewPagers = (AdapterViewPager) viewPager.getAdapter();
                if(state == State.COLLAPSED) {
                    toolbarCollapsed = true;
                    //Log.d("SHAKIL", "yep toolbar is collapsed and toolbarCollapsed = "+toolbarCollapsed);
                    // this is for solve the bug in review as the toolbar got collapsed automatically
                    //if(currentPage == 1)
                        //appBarLayout_attraction.setExpanded(true, true);
                    if(currentPage == 0) {
                        try {
                            if (!collapseOtherThanRecv) {
                                adapterViewPagers.setOtherLayout(true, 0);
                                collapseOtherThanRecv = false;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if(currentPage == 1) {
                        adapterViewPagers.setOtherLayout(true, 1);
                    }
                }else if(state == State.EXPANDED) {
                    toolbarCollapsed = false;
                    //Log.d("SHAKIL", "yep toolbar is expanded and toolbarCollapsed = "+toolbarCollapsed);
                    if(currentPage == 0) {
                        try {
                            adapterViewPagers.setOtherLayout(false, 0);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        collapseOtherThanRecv = false;
                    }
                } else if(currentPage == 1) {
                    adapterViewPagers.setOtherLayout(false, 1);
                }
            }
        });
    }

    private void init_layout() {
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        setupViewPager(viewPager);

        tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        // for the toolbar image
        imgv_htab_header = (ImageView) findViewById(R.id.htab_header);

        tb_ownCheckIn = (ToggleButton) findViewById(R.id.tb_ownCheckIn);
        tb_ownCheckIn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(attraction_id != null)
                    AllStaticMethods.add_to_checkIn(Integer.parseInt(attraction_id), b);
            }
        });
        tb_ownWishList = (ToggleButton) findViewById(R.id.tb_ownWishList);
        tb_ownWishList.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(attraction_id != null)
                    AllStaticMethods.add_to_wishlist(Integer.parseInt(attraction_id), b);
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        AdapterViewPager adapter = new AdapterViewPager(getSupportFragmentManager());
        adapterViewPager = adapter;
        adapter.addFrag(new FragmentOverview(), "Overview");
        adapter.addFrag(new FragmentReview(), "Reviews");
        adapter.addFrag(new FragmentGallery(), "Gallery");
        adapter.addFrag(new FragmentMap(), "Map");
        adapter.addFrag(new FragmentArticle(), "Article");
        viewPager.setAdapter(adapter);
        // to expand on a new tab is selected if on previous tab the toolbar is collapsed and then navigate to another tab
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
                if(toolbarCollapsed) {
                    appBarLayout_attraction.setExpanded(true, true);
                    //Log.d("SHAKIL", "yep toolbar is collapsed and setExpnded funtion is called");
                }
               // if(dataLoaded)
               //     callFragmentOnDataAvailable();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initCustomToolbarLayout() {
        imgv_backButton = (ImageView) findViewById(R.id.imgv_backButton);
        imgv_backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        txtv_tlbarTitle = (TextView) findViewById(R.id.txtv_tlbarTitle);
        txtv_tlbarTitle.setText("Zow Tiang");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void collapseToolbaronTextOutOfScreen(boolean collapse) {
        Log.d("SHAKIL", "yep toolbar is to be collapsed get called from ..");
        collapseOtherThanRecv = true;
        appBarLayout_attraction.setExpanded(!collapse, true);
    }


    public class AttractionDetailAsync extends AsyncTask<Void, String, String>{
        private JSONObject jsonObject, jsonObject2, jsonObjectReview;
        private JSONArray jsonArray, jsonArray2, jsonArrayReview;
        private String url = null, result = null;
        /*private String title  = null, description = null, latitude = null,
                longitude = null, reviews_count = null, rating_count = null,
                rating_value = null, checkin_count = null, wishlists_count = null,
                featured_image = null;*/
        @Override
        protected String doInBackground(Void... voids) {
            url = AttractionDetailApi.getAttractionDetailApi(attraction_id,
                    AllStaticMethods.get_api_token(getApplicationContext()));
                    //"eKafAoIHW4vrV05kzxpOJy4B9fRiiYW2eQkg8tZgGhH20LPGE1PXas6m4U6V");
            Log.d("SHAKIL", "url = "+url);
            result = new HttpDataHandler().GetHTTPData(url);
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Log.d("SHAKIL", "result = "+s);
            if (isValidString(s)) {
                //Log.d("SHAKIL", "yep valid string");
                try {
                    jsonObject = new JSONObject(s);
                    title = jsonObject.getString("title");
                    try {
                    name = jsonObject.getJSONObject("location").getString("title");
                    }catch (Exception e) {
                        if(AllStaticDataHolder.ALL_ATTRACTIONS != null)
                        for (AttractionsGridViewDatasetHolder dt :
                                AllStaticDataHolder.ALL_ATTRACTIONS) {
                            if(dt.getId()==Double.parseDouble(attraction_id)){
                                name = dt.getLocation();
                                break;
                            }
                        }
                    }
                    description = jsonObject.getString("description");
                    reviewsCount = jsonObject.getString("reviews_count");
                    checkInCount = jsonObject.getString("checkin_count");
                    wishlistsCount = jsonObject.getString("wishlists_count");
                    featuredImageUrl = jsonObject.getString("featured_image");
                    if(featuredImageUrl.startsWith("/")) {
                        featuredImageUrl = "http://db.softwindtech-dev.com"+featuredImageUrl;
                        Log.d("SHAKIL", "featured image url = "+featuredImageUrl);
                    }
                    latitude = jsonObject.getString("latitude");
                    longitude = jsonObject.getString("longitude");
                    ratingValue = jsonObject.getString("rating_value");

                    ownWishList = jsonObject.getString("own_wishlist");
                    ownCheckIn = jsonObject.getString("own_checkin");
                    shareLink = jsonObject.getString("url");
                    if(ownCheckIn != null && ownCheckIn != null) {
                        // handle the values and set them exactly
                    }
                    Log.d("SHAKIL", "yep featured image url "+featuredImageUrl);
                    getSupportActionBar().setTitle(title);
                    setFeaturedImage(featuredImageUrl);
                    Log.d("SHAKIL", "title = "+title+" description = "+description);
                    // now data for recv_overView
                    jsonArray = jsonObject.getJSONArray("tags");
                    String tags = "";
                    for(int i = 0; i<jsonArray.length(); i++) {
                        if(i == (jsonArray.length() - 1))
                            tags += jsonArray.getJSONObject(i).getString("title");
                        else
                            tags += jsonArray.getJSONObject(i).getString("title")+",";
                    }
                    if(listOverViews == null) {
                        listOverViews = new ArrayList<OverviewModel>();
                    }
                    listOverViews.add(new OverviewModel("", description, tags, ""));
                    jsonArray2 = jsonObject.getJSONArray("metaValues").getJSONObject(0).getJSONArray("meta_values");
                    for(int j = 0; j<jsonArray2.length(); j++) {
                        jsonObject2 = jsonArray2.getJSONObject(j);
                        String titleAns = jsonObject2.getString("value");
                        JSONObject jObject = jsonObject2.getJSONObject("meta");
                        String titleQuery = jObject.getString("title");
                        String metaId = jObject.getString("id");
                        String metaIconLink = jObject.getString("icon");
                        listOverViews.add(new OverviewModel(metaId, titleQuery, titleAns, metaIconLink));
                    }
                    //Log.d("SHAKIL", "tags = "+tags+" list len = "+listOverViews.size());

                    // for review
                    if(listReviews == null) {
                        listReviews = new ArrayList<ReviewModel>();
                    }
                    jsonArrayReview = jsonObject.getJSONArray("reviews");
                    for (int j = 0; j < jsonArrayReview.length(); j++) {
                        jsonObjectReview = jsonArrayReview.getJSONObject(j);
                        String title = jsonObjectReview.getString("title");
                        String description = jsonObjectReview.getString("description");
                        String rating = jsonObjectReview.getString("rating");
                        String user_id = jsonObjectReview.getString("user_id");
                        String likes_count = jsonObjectReview.getString("likes_count");
                        String dislikes_count = jsonObjectReview.getString("dislikes_count");
                        String created_at = jsonObjectReview.getString("created_at");
                        String uName = jsonObjectReview.getJSONObject("user").getString("name");
                        String uEmail = jsonObjectReview.getJSONObject("user").getString("email");
                        listReviews.add(new ReviewModel(uName, rating, created_at, description, title, likes_count, dislikes_count, uEmail));
                    }
                    dataLoaded = true;
                    //Log.d("SHAKIL", "yep now dataloaded "+dataLoaded);
                    callFragmentOnDataAvailable();
                } catch (JSONException e) {
                    Log.d("SHAKIL", "yep jsonException and error = "+e.toString());
                    e.printStackTrace();
                }
            }
        }
    }

    private void setFeaturedImage(String imgUrl) {
        if(isValidString(imgUrl)) {
            Glide.with(getApplicationContext())
                    .load(imgUrl)
                    .into(imgv_htab_header);
        }
    }

    private boolean isValidString(String string) {
        if(string != null && !string.isEmpty() && !string.matches("null"))
            return true;
        return false;
    }

    private void callFragmentOnDataAvailable() {
        AdapterViewPager adapterViewPagers = (AdapterViewPager) viewPager.getAdapter();
        adapterViewPagers.setDataOnFragment(currentPage);
    }

   @Override
   public void setDataToReview() {
       //Log.d("SHAKIL", "yep setDataToReviewCalled and dataloaded = "+dataLoaded);
      if(dataLoaded) {
         callFragmentOnDataAvailable();
      }
   }

   @Override
   public void setDataToGallery() {
       if(dataLoaded) {
           callFragmentOnDataAvailable();
       }
   }

   @Override
   public void setDataToMap() {
       if(dataLoaded) {
           callFragmentOnDataAvailable();
       }
   }

   @Override
   public void setDataToArticle() {
       if(dataLoaded) {
           callFragmentOnDataAvailable();
       }
   }

   @Override
   public void setDataToOverView() {
       if(dataLoaded) {
           callFragmentOnDataAvailable();
       }
   }

   @Override
   public void collapseToolbarOnReviewTopOutOfSpace(boolean collapse) {
       appBarLayout_attraction.setExpanded(!collapse, true);
   }

                                                                   }
