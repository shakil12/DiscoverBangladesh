package discoverbd.robi.com.discoverbangladesh;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import AllStatic.AddToLikedList;
import AllStatic.AllStaticDataHolder;
import AllStatic.AllStaticMethods;
import AllStatic.ArticlesLink;
import AllStatic.LoginLinks;
import Holder.ArticlesRecycleViewDatasetHolder;
import de.hdodenhof.circleimageview.CircleImageView;

public class ArticleDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private String shareLink;
    private boolean isBookmarked = false, isLiked = false;
    private int id;
    private ArticlesRecycleViewDatasetHolder datasetHolder;

    FloatingActionButton fab;
    WebView wv_detail;
    TextView tv_author, tv_time, tv_title, tv_like;
    ImageButton ib_share, ib_bookmark;
    ImageView iv_main;
    CircleImageView iv_author;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Article");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tv_author = (TextView) findViewById(R.id.tv_article_detail_author);
        tv_time = (TextView) findViewById(R.id.tv_article_detail_time);
        tv_title = (TextView) findViewById(R.id.tv_article_detail_title);
        tv_like = (TextView) findViewById(R.id.tv_article_detail_like_num);

        iv_main = (ImageView) findViewById(R.id.iv_main);
        iv_author = (CircleImageView) findViewById(R.id.iv_article_detail_author_pic);

        wv_detail = (WebView) findViewById(R.id.wv_article_detail);

        id = getIntent().getIntExtra("id",0);

        for (ArticlesRecycleViewDatasetHolder dt :
                AllStaticDataHolder.ALL_ARTICLES) {
            int k = (int) dt.getId();
            if(k == id){
                datasetHolder = dt;
                break;
            }
        }

        if(datasetHolder != null){
            shareLink = datasetHolder.getShareLink();
            isBookmarked = datasetHolder.isBookmarked();
            isLiked = datasetHolder.isLiked();
            tv_author.setText(datasetHolder.getAuthor());
            tv_time.setText(datasetHolder.getTime());
            tv_title.setText(datasetHolder.getTitle());
            tv_like.setText(datasetHolder.getLike_num());

            String contents= "<html><head>"
                    + "<style type=\"text/css\">img{width: 100%;}"
                    + "</style></head>"
                    + "<body>"
                    + add_image_header_link_in_html_text(datasetHolder.getDescription())
                    + "</body></html>";

            wv_detail.loadData(contents, "text/html; charset=UTF-8", null);
            Picasso.with(this)
                    .load(datasetHolder.getImage_link())
                    .into(iv_main);

            try {
                Picasso.with(this)
                        .load(AllStaticMethods.get_gravatar_link(datasetHolder.getAuthor_email()))
                        .placeholder(R.drawable.ic_account_circle_black_24dp)
                        .into(iv_author);
            }catch (Exception e){}


        }

        //shareLink = getIntent().getStringExtra("shareLink");
        //isBookmarked = getIntent().getBooleanExtra("bookmarked",false);


    //    wv_detail.loadUrl("https://www.google.com");

        ib_share = (ImageButton) findViewById(R.id.ib_article_detail_share);
        ib_share.setOnClickListener(this);

        ib_bookmark = (ImageButton) findViewById(R.id.ib_article_detail_bookmark);
        ib_bookmark.setOnClickListener(this);
        if(isBookmarked)
            ib_bookmark.setImageResource(R.drawable.ic_bookmark_black_48px);
        else
            ib_bookmark.setImageResource(R.drawable.ic_bookmark_border_black_48px);



      //  tv_author.setText(getIntent().getStringExtra("author"));
      //  tv_like.setText(getIntent().getStringExtra("like"));
      //  tv_title.setText(getIntent().getStringExtra("title"));
      //  tv_time.setText(getIntent().getStringExtra("time"));



        fab = (FloatingActionButton) findViewById(R.id.fab);
        if(isLiked){
            fab.setImageResource(R.drawable.drawable_wishlist_positive);
        }
        else {
            fab.setImageResource(R.drawable.drawable_like_num);
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isLiked){
                    fab.setImageResource(R.drawable.drawable_like_num);
                    isLiked = false;
                }
                else {
                    fab.setImageResource(R.drawable.drawable_wishlist_positive);
                    isLiked = true;
                }

                for (ArticlesRecycleViewDatasetHolder dt :
                        AllStaticDataHolder.ALL_ARTICLES) {
                    int k = (int) dt.getId();
                    if(k == id){
                        dt.setLiked(isLiked);
                        new AddToLikedList(ArticleDetailActivity.this,id,isLiked).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        break;
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.ib_article_detail_share){
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT, shareLink);
            startActivity(Intent.createChooser(share, "Share with..."));
        }

        else if(view.getId() == R.id.ib_article_detail_bookmark){
            if(isBookmarked) {
                ib_bookmark.setImageResource(R.drawable.ic_bookmark_border_black_48px);
                isBookmarked = false;
            }
            else {
                ib_bookmark.setImageResource(R.drawable.ic_bookmark_black_48px);
                isBookmarked = true;
            }

            for (ArticlesRecycleViewDatasetHolder dt :
                    AllStaticDataHolder.ALL_ARTICLES) {
                int k = (int) dt.getId();
                if(k == id){
                    dt.setBookmarked(isBookmarked);
                    break;
                }
            }
        }
    }


    private String add_image_header_link_in_html_text(String inputString){
        ArrayList<Integer> arrayList = new ArrayList<>();
// Assuming three times here
       // String inputString = "<html><img id=\"MathMLEq1\" style=\"vertical-align: middle;\" src=\"/SOCH/img.PNG\" alt=\"\"/></html> <html><img id=\"MathMLEq1\" style=\"vertical-align: middle;\" src=\"/SOCH/img.PNG\" alt=\"\"/></html> <html><img id=\"MathMLEq1\" style=\"vertical-align: middle;\" src=\"/SOCH/img.PNG\" alt=\"\"/></html>";
        StringBuffer q= new StringBuffer(inputString);
        String add = LoginLinks.BASE_URL;
//Get all indexed of the occurrence of /SOCH string
        for (int index = inputString.indexOf(ArticlesLink.IMAGE_FILE_NAME);
             index >= 0;
             index = inputString.indexOf(ArticlesLink.IMAGE_FILE_NAME, index + 1)){

            arrayList.add(index); //add the indexes to arrayList
        }
        int prev = 0;
        for (int i = 0; i < arrayList.size(); i++){ // for all indexes
            q = q.insert(prev+ arrayList.get(i),add); //Insert the add string at position (index + (number of times 'add' string appears * length of 'add' string)
            prev = (i+1)*add.length(); // calculate the next position where to insert the string

        }
        String result = q.toString(); //Gives the final output as desired.
        return result;
    }
}
