package discoverbd.robi.com.discoverbangladesh;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.HashMap;

import AllStatic.AllStaticMethods;
import AllStatic.LoginLinks;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {

    private RelativeLayout rl_main;

    private EditText et_email, et_password;
    private Button btn_login;
    private ImageButton ib_fb;
  //  private AlertDialog alertDialog_wait;
    private ProgressDialog progressDialog_wait;


    // for user fblogin
    private CallbackManager callbackManager;
    private LoginButton btn_loginfb;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle("Log in");

        rl_main = (RelativeLayout) findViewById(R.id.activity_log_in);

        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);

        et_email.addTextChangedListener(this);
        et_password.addTextChangedListener(this);

        btn_login = (Button) findViewById(R.id.btn_log_in);
        btn_login.setOnClickListener(this);

        btn_loginfb = (LoginButton) findViewById(R.id.btn_login_fb);
        callbackManager = CallbackManager.Factory.create();
        btn_loginfb.setReadPermissions("public_profile", "email");
        btn_loginfb.registerCallback(callbackManager, fbCallBack);


        ib_fb = (ImageButton) findViewById(R.id.ib_login_fb);
        ib_fb.setOnClickListener(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }



    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            rl_main.setBackgroundResource(R.drawable.login_back_hori);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            rl_main.setBackgroundResource(R.drawable.login_back);
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_log_in){
            check_provided_info();
        }
        else if(view.getId() == R.id.ib_login_fb){

            progressDialog_wait = new ProgressDialog(LoginActivity.this);
            progressDialog_wait.setMessage("Loading....");
            progressDialog_wait.show();

            btn_loginfb.performClick();

            btn_loginfb.setPressed(true);

            btn_loginfb.setPressed(false);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if(et_email.getText().toString().length()<=0){
            et_email.setError("Required");
        }
        else if(!AllStaticMethods.is_valid_email(et_email.getText().toString())){
            et_email.setError("Invalid Email");
        }
        else {
            et_email.setError(null);
        }


        if(et_password.getText().toString().length()<=0){
            et_password.setError("Required");
        }
        else if(et_password.getText().toString().length()<6){
            et_password.setError("At least 6 characters required.");
        }
        else {
            et_password.setError(null);
        }
    }


    private void check_provided_info()
    {
        if((et_email.getText().toString().length()==0)
                || (et_password.getText().toString().length()==0)
                ){

            AllStaticMethods.show_alert(this,"Incomplete Field","Please complete all fields.");
        }

        else if(!AllStaticMethods.is_valid_email(et_email.getText().toString())){
            AllStaticMethods.show_alert(this,"Invalid Email", "Please insert a valid email.");
        }
        else if(et_password.getText().toString().length()<6){
            AllStaticMethods.show_alert(this,"Wrong Password","Please check your password.");
        }
        else {
            new LoginAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }



    private void navigate_to_home_screen(){
        Intent intent = new Intent(this,HomeScreenActivity.class);
        startActivity(intent);
    }




///////////////fb callback
private FacebookCallback<LoginResult> fbCallBack = new FacebookCallback<LoginResult>(){

    @Override
    public void onSuccess(LoginResult loginResult) {

        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        String  email = null ;
                        try {

                            progressDialog_wait.cancel();

                            try {
                                email = object.getString("email");
                            }catch (Exception e) {
                                e.printStackTrace();
                                email = null;

                            }

                            if(email != null) {
                                progressDialog_wait.cancel();

                                /////////////////////////////
                                new FbAsync(email).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                            }else {
                                progressDialog_wait.cancel();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, email, gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();

    }

    @Override
    public void onCancel() {
        progressDialog_wait.cancel();
    }

    @Override
    public void onError(FacebookException error) {
        progressDialog_wait.cancel();
        AllStaticMethods.show_alert(LoginActivity.this,"Error","Something goes wrong. Please try again.");
    }
};

//////////// login async
    private class LoginAsync extends AsyncTask<Void, Void, Void>{

        private boolean loginDone = false;
        private String st_email, st_password, token, name, email;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            st_email = et_email.getText().toString();
            st_password = et_password.getText().toString();

            //alertDialog_wait = AllStaticMethods.show_wait_alert(LoginActivity.this);
            progressDialog_wait = new ProgressDialog(LoginActivity.this);
            progressDialog_wait.setMessage("Loading....");
            progressDialog_wait.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("email",st_email);
            hashMap.put("password",st_password);

            String result = new HttpDataHandler().performPostCall(LoginLinks.BASE_URL + LoginLinks.LOGIN_LINK , hashMap);
            if(result != null) {
                try {
                    JSONObject jsonObject = new JSONObject(result);

                    boolean is_success = jsonObject.getBoolean("success");

                    if(is_success){
                        JSONObject object = jsonObject.getJSONObject("data");
                        token = object.getString("api_token");
                        name = object.getString("name");
                        email = object.getString("email");
                        loginDone = true;
                    }
                    else {
                        loginDone = false;
                    }
                }catch (Exception e){
                    loginDone = false;
                }
            } else {
                loginDone = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //alertDialog_wait.cancel();
            progressDialog_wait.cancel();

            if(loginDone){
                AllStaticMethods.set_user_login_info(LoginActivity.this,token,name,email);
                navigate_to_home_screen();
            }

            else {
                AllStaticMethods.show_alert(LoginActivity.this,"Error","Login failed. Please try again.");
            }
        }
    }


    private class FbAsync extends AsyncTask<Void, Void, Void>{

        private boolean loginDone = false;
        private String st_email, token, email, name;


        public FbAsync(String mail){
            st_email = mail;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            //alertDialog_wait = AllStaticMethods.show_wait_alert(LoginActivity.this);
            progressDialog_wait = new ProgressDialog(LoginActivity.this);
            progressDialog_wait.setMessage("Loading....");
            progressDialog_wait.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {


            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("email",st_email);


            String result = new HttpDataHandler().performPostCall(LoginLinks.BASE_URL + LoginLinks.LOGIN_FB , hashMap);
            if(result != null) {
                try {
                    JSONObject jsonObject = new JSONObject(result);

                    boolean is_success = jsonObject.getBoolean("success");

                    if(is_success){
                        JSONObject object = jsonObject.getJSONObject("data");
                        token = object.getString("api_token");
                        name = object.getString("name");
                        email = object.getString("email");
                        loginDone = true;
                    }
                    else {
                        loginDone = false;
                    }
                }catch (Exception e){
                    loginDone = false;
                }
            } else {
                loginDone = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //alertDialog_wait.cancel();
            progressDialog_wait.cancel();

            if(loginDone){
                AllStaticMethods.set_user_login_info(LoginActivity.this,token,name,email);
                navigate_to_home_screen();
            }

            else {
                AllStaticMethods.show_alert(LoginActivity.this,"Error","Login failed. Please try again.");
            }
        }
    }
}
