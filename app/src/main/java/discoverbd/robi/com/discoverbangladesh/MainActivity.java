package discoverbd.robi.com.discoverbangladesh;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONObject;

import java.util.HashMap;

import AllStatic.AllStaticMethods;
import AllStatic.LoginLinks;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    Button btn_logIn, btn_signUp;
    private ImageButton ib_fb,ib_google;
    private ProgressDialog progressDialog_wait;
    private CallbackManager callbackManager;
    private LoginButton btn_loginfb;

    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        if(AllStaticMethods.get_api_token(this) != null)
            navigate_to_home_screen();

        btn_logIn = (Button) findViewById(R.id.btn_log_in);
        btn_signUp= (Button) findViewById(R.id.btn_sign_up);
        btn_logIn.setOnClickListener(this);
        btn_signUp.setOnClickListener(this);


        btn_loginfb = (LoginButton) findViewById(R.id.btn_login_fb);
        callbackManager = CallbackManager.Factory.create();
        btn_loginfb.setReadPermissions("public_profile", "email");
        btn_loginfb.registerCallback(callbackManager, fbCallBack);


        ib_fb = (ImageButton) findViewById(R.id.ib_login_fb);
        ib_fb.setOnClickListener(this);

        ib_google = (ImageButton) findViewById(R.id.btn_login_google);
        ib_google.setOnClickListener(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                Intent intent = new Intent(MainActivity.this, AttractionDetail.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LoginLinks.RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGoogleSignInResult(result);
        }
        else
            callbackManager.onActivityResult(requestCode,resultCode,data);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory( Intent.CATEGORY_HOME );
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_log_in){
            Intent intent = new Intent(MainActivity.this,LoginActivity.class);
            startActivity(intent);
        }
        else if(view.getId() == R.id.btn_sign_up){
            Intent intent = new Intent(MainActivity.this,SignUpActivity.class);
            startActivity(intent);
        }
        else if(view.getId() == R.id.ib_login_fb){

            progressDialog_wait = new ProgressDialog(MainActivity.this);
            progressDialog_wait.setMessage("Loading....");
            progressDialog_wait.show();

            btn_loginfb.performClick();

            btn_loginfb.setPressed(true);

            btn_loginfb.setPressed(false);
        }
        else if(view.getId() == R.id.btn_login_google){
            googleSignIn();
        }
    }


    private void handleGoogleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            try {

                GoogleSignInAccount acct = result.getSignInAccount();
                new FbAsync(acct.getDisplayName(), acct.getEmail()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }catch (Exception e){}

        } else {
            // Signed out, show unauthenticated UI.

        }
    }

    private void navigate_to_home_screen(){
        Intent intent = new Intent(this,HomeScreenActivity.class);
        startActivity(intent);
    }

    private void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, LoginLinks.RC_SIGN_IN);
    }

    ///////////////fb callback
    private FacebookCallback<LoginResult> fbCallBack = new FacebookCallback<LoginResult>(){

        @Override
        public void onSuccess(LoginResult loginResult) {

            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            String  email = null, name = null ;
                            try {

                                progressDialog_wait.cancel();

                                try{
                                    name = object.getString("name");
                                }catch (Exception e) {
                                    e.printStackTrace();
                                    name = "";
                                }

                                try {
                                    email = object.getString("email");
                                }catch (Exception e) {
                                    e.printStackTrace();
                                    email = null;

                                }

                                if(email != null && name != null) {
                                    progressDialog_wait.cancel();

                                    /////////////////////////////
                                    new FbAsync(name,email).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                                }else {
                                    progressDialog_wait.cancel();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id, name, email, gender, birthday");
            request.setParameters(parameters);
            request.executeAsync();

        }

        @Override
        public void onCancel() {
            progressDialog_wait.cancel();
        }

        @Override
        public void onError(FacebookException error) {
            progressDialog_wait.cancel();
            AllStaticMethods.show_alert(MainActivity.this,"Error","Something goes wrong. Please try again.");
        }
    };

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private class FbAsync extends AsyncTask<Void, Void, Void>{

        private boolean loginDone = false;
        private String st_email, st_name, token, email, name;


        public FbAsync(String n, String mail){
            st_name = n;
            st_email = mail;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            //alertDialog_wait = AllStaticMethods.show_wait_alert(LoginActivity.this);
            progressDialog_wait = new ProgressDialog(MainActivity.this);
            progressDialog_wait.setMessage("Loading....");
            progressDialog_wait.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {


            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("email",st_email);


            String result = new HttpDataHandler().performPostCall(LoginLinks.BASE_URL + LoginLinks.LOGIN_FB , hashMap);
            if(result != null) {
                try {
                    JSONObject jsonObject = new JSONObject(result);

                    boolean is_success = jsonObject.getBoolean("success");

                    if(is_success){
                        JSONObject object = jsonObject.getJSONObject("data");
                        token = object.getString("api_token");
                        name = object.getString("name");
                        email = object.getString("email");
                        loginDone = true;
                    }
                    else {
                        loginDone = false;
                    }
                }catch (Exception e){
                    loginDone = false;
                }
            } else {
                loginDone = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //alertDialog_wait.cancel();
            progressDialog_wait.cancel();

            if(loginDone){
                AllStaticMethods.set_user_login_info(MainActivity.this,token,name,email);
                navigate_to_home_screen();
            }

            else {
                Intent intent = new Intent(MainActivity.this,SignUpActivity.class);
                intent.putExtra("name", st_name);
                intent.putExtra("email",st_email);
                startActivity(intent);
            }
        }
    }
}
