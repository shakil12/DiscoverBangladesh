package Fragment;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

import Adapter.AdapterArticle;
import Adapter.AdapterOverView;
import Helper.AppBarStateChangeListener;
import ModelClass.EmnoModel;
import ModelClass.OverviewModel;
import discoverbd.robi.com.discoverbangladesh.AttractionDetail;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by Shakil on 22-Dec-16.
 */

public class FragmentOverview extends Fragment implements AdapterOverView.OnItemExpanded {

    // for locDetail
    private TextView txtv_locDetails;
    private ToggleButton tb_readMore;
    private ImageView imgv_shareLoc;
    //private RatingBar rb_loc;

    // for locQuery recyclerView
    private RecyclerView recv_overView;
    private List<OverviewModel> listOverview;
    private AdapterOverView adapterOverView;
    private AppBarLayout appBarLayout_attraction;
    private HorizontalScrollView hscrollv_locTag;
    RecyclerView.LayoutManager mLayoutManager;

    // for scroll and collapse toolbar other than recyclerview scroll
    OnTextLocDetailOutOfScreen mCallbacks;
    Helper.CustomScrollView scrlv_sp;
    Helper.CustomLinearLayout linearv_sp;
    private LinearLayout linearv_top;
    private TextView txtv_loc;
    private boolean touchTaken = false;
    private float firstTouchY = 0;

    // for setting data from api
    private TextView txtv_locTitle;
    private TextView txtv_locName;

    private RatingBar rb_loc;
    private TextView txtv_reviewCount;
    private TextView txtv_checkInCount;

    // for on Data loading
    private AttractionDetail attractionDetail;
    OnActivityCreatedOverView mCallback;


    public FragmentOverview() {
        // Required empty public constructor
    }

    public static FragmentOverview newInstance() {
        
        Bundle args = new Bundle();
        
        FragmentOverview fragment = new FragmentOverview();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_overview, container, false);

        // for locDetail
        txtv_locDetails = (TextView) rootView.findViewById(R.id.txtv_locDetails);
        txtv_locDetails.setVisibility(View.GONE);
        tb_readMore = (ToggleButton) rootView.findViewById(R.id.tb_readMore);
        tb_readMore.setVisibility(View.GONE);
        //txtv_loc = (TextView) rootView.findViewById(R.id.txtv_loc);

        //attractionDetail = (AttractionDetail) getActivity();
        txtv_locTitle = (TextView) rootView.findViewById(R.id.txtv_locTitle);
        txtv_locName = (TextView) rootView.findViewById(R.id.txtv_locName);
        txtv_reviewCount = (TextView) rootView.findViewById(R.id.txtv_reviewCount);
        txtv_checkInCount = (TextView) rootView.findViewById(R.id.txtv_checkInCount);
        rb_loc = (RatingBar) rootView.findViewById(R.id.rb_loc);
        imgv_shareLoc = (ImageView) rootView.findViewById(R.id.imgv_shareLoc);
        imgv_shareLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isValidString(attractionDetail.shareLink)) {
                    share(attractionDetail.shareLink);
                }
            }
        });

        // locQuery recyclerView
        recv_overView = (RecyclerView) rootView.findViewById(R.id.recv_overView);
        hscrollv_locTag = (HorizontalScrollView) rootView.findViewById(R.id.hscrollv_locTag);
        hscrollv_locTag.setVisibility(View.GONE);
        //scrlv_sp = (Helper.CustomScrollView) rootView.findViewById(R.id.scrlv_sp);

        // for scroll and collapse toolbar other than recyclerview scroll
        linearv_top = (LinearLayout) rootView.findViewById(R.id.linearv_top);
        linearv_sp = (Helper.CustomLinearLayout) rootView.findViewById(R.id.linearv_sp);
        setLayout4collapseToolabrOtherThanRecv();
        // for ondata loaded
        attractionDetail = (AttractionDetail) getActivity();
        return rootView;
    }

    private void setRecyclerView() {
        if(listOverview == null || listOverview.size() == 0)
            listOverview = new ArrayList<OverviewModel>();
        /*listOverview.add(new EmnoModel(getResources().getString(R.string.test_description), ""));
        listOverview.add(new EmnoModel("How to go ?", ""));
        listUsers.add(new EmnoModel("Where to eat ?", ""));
        listUsers.add(new EmnoModel("What are the best hotels ?", ""));
        listUsers.add(new EmnoModel("What are the best foods ?", ""));
        listUsers.add(new EmnoModel("Environment ?", ""));
        listUsers.add(new EmnoModel("Must see", ""));*/

        adapterOverView = new AdapterOverView(getActivity(), listOverview, this);
        mLayoutManager = new LinearLayoutManager(getContext());
        recv_overView.setLayoutManager(mLayoutManager);
        recv_overView.setItemAnimator(new DefaultItemAnimator());
        recv_overView.setAdapter(adapterOverView);
        //adapterOverView.notifyDataSetChanged();
        //new UserCmntAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        //Log.d("SHAKIL", "yep setting the recyclerview thing is just finished");
    }

    public void collapseOtherLayout(boolean collapse) {
        if(collapse) {
            txtv_locDetails.setVisibility(View.GONE);
            hscrollv_locTag.setVisibility(View.GONE);
            tb_readMore.setVisibility(View.GONE);
        } else {
            txtv_locDetails.setVisibility(View.VISIBLE);
            hscrollv_locTag.setVisibility(View.VISIBLE);
            tb_readMore.setVisibility(View.VISIBLE);
        }
    }

    public void expandOtherLayout() {
        txtv_locDetails.setVisibility(View.VISIBLE);
        hscrollv_locTag.setVisibility(View.VISIBLE);
        tb_readMore.setVisibility(View.VISIBLE);
    }

    /**
     * if the txtv_locDetails text is more than 3 lines then after 3 lines remaining lines will be collapsed and tb_readMore
     * togglebutton will be there so if it is checked then we will exopand the textview to whole and again if unchecked then
     * collapse back
     */
    private void setTb_readMore() {
        tb_readMore.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if(checked) {
                    expandTextView(txtv_locDetails);
                }else {
                    collapseTextView(txtv_locDetails, 3);
                }
            }
        });
    }

    /** for txtv_locDetails expansion or (scroll of layout otherthan recyclerview should collapse the toolbar)
     and it goes out of screen and scroll not working
     so we will try to collapse the toolbar then to solve this problem
     */
    private void setLayout4collapseToolabrOtherThanRecv() {
        linearv_sp.initLayout(getContext());
        linearv_sp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent ev) {
                float initTouchY = 0, finalY = 0;
                boolean scrollUp;
                switch (ev.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initTouchY = ev.getRawY();
                        //Log.d("SHAKIL", "yep down from custom linearv at "+ev.getRawY()+" ...................");
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if(!touchTaken) {
                            firstTouchY = ev.getRawY();
                            //Log.d("SHAKIL", "first touchY = "+firstTouchY);
                            touchTaken = true;
                        }
                        finalY = Math.abs(ev.getRawY() - firstTouchY);
                        if(convertPixelsToDp(finalY, getContext()) > 50) {
                            if(firstTouchY < ev.getRawY()) {
                                scrollUp = false;
                            } else {
                                scrollUp = true;
                            }
                            if(scrollUp) {
                                mCallbacks.collapseToolbaronTextOutOfScreen(true);
                            }
                            else {
                                mCallbacks.collapseToolbaronTextOutOfScreen(false);
                            }
                        }
                        //Log.d("SHAKIL", "yep move from custom linearv at "+finalY);
                        break;
                    case MotionEvent.ACTION_UP:
                        touchTaken = false;
                        firstTouchY = 0;
                        //Log.d("SHAKIL", "yep up from custom diff =  "+(ev.getRawY() - initTouchY)+" ////////////////////");
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }

    public static int getVisiblePercent(View v) {
        if (v.isShown()) {
            Rect r = new Rect();
            v.getGlobalVisibleRect(r);
            double sVisible = r.width() * r.height();
            double sTotal = v.getWidth() * v.getHeight();
            return (int) (100 * sVisible / sTotal);
        } else {
            return -1;
        }
    }

    private void expandTextView(TextView tv) {
        ObjectAnimator animation = ObjectAnimator.ofInt(tv, "maxLines", tv.getLineCount());
        animation.setDuration(200).start();
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if(getVisiblePercent(txtv_locDetails) < 100) {
                   //mCallback.collapseToolbaronTextOutOfScreen();
                }
                Log.d("SHAKIL", "visible "+getVisiblePercent(txtv_locDetails));
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

    }

    private void collapseTextView(TextView tv, int numLines) {
        ObjectAnimator animation = ObjectAnimator.ofInt(tv, "maxLines", numLines);
        animation.setDuration(200).start();
    }

    public interface OnTextLocDetailOutOfScreen {
        public void collapseToolbaronTextOutOfScreen(boolean collapse);
    }

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    private void cycleTextViewExpansion(TextView tv) {
        Log.d("SHAKIL", "yep expanding the texts on click of the button");
        int collapsedMaxLines = 2;
        ObjectAnimator animation = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            animation = ObjectAnimator.ofInt(tv, "maxLines",
                    tv.getMaxLines() == collapsedMaxLines? tv.getLineCount() : collapsedMaxLines);
        }
        animation.setDuration(200).start();
    }

    public void attachDataToView() {
        Log.d("SHAKIL", "yep attachDataToView is called from overView");
        initActivityRefs();
        if(attractionDetail == null)
            return;
        if(attractionDetail.dataLoaded) {
            txtv_locTitle.setText(attractionDetail.title);
            txtv_locName.setText(attractionDetail.name);

            rb_loc.setRating(Float.parseFloat(attractionDetail.ratingValue));
            txtv_reviewCount.setText("("+attractionDetail.reviewsCount+" review)");
            txtv_checkInCount.setText(attractionDetail.checkInCount+" people visited");

            // set the recv_overview
            if(attractionDetail.listOverViews != null) {
                recv_overView.setAdapter(null);
                adapterOverView = new AdapterOverView(getActivity(), attractionDetail.listOverViews, this);
                mLayoutManager = new LinearLayoutManager(getContext());
                recv_overView.setLayoutManager(mLayoutManager);
                recv_overView.setItemAnimator(new DefaultItemAnimator());
                recv_overView.setAdapter(adapterOverView);
                adapterOverView.notifyDataSetChanged();
                //for(OverviewModel overviewModel : attractionDetail.listOverViews) {
                //    listOverview.add(overviewModel);
                //    adapterOverView.notifyDataSetChanged();
                //}
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //Log.d("SHAKIL", "from onDestroy of Overview");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ///txtv_locDetails.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View view) {
        //        collapseTextView(txtv_locDetails, 3);
        //   }
        //});
        //if(txtv_locDetails.getLineCount() < 3)
        //    tb_readMore.setVisibility(View.GONE);
        //else {
        //tb_readMore.setOnClickListener(new View.OnClickListener() {
        //@Override
        // public void onClick(View view) {
        //       expandTextView(txtv_locDetails);
        //    }
        //});

        setTb_readMore();
        //}
        setRecyclerView();
        mCallback.setDataToOverView();
        Log.d("SHAKIL", "from onActivityCreated of Overview");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnActivityCreatedOverView) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
        try {
            mCallbacks = (OnTextLocDetailOutOfScreen) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnTextLocDetailOutOfScreen");
        }

    }

    public interface OnActivityCreatedOverView {
        public void setDataToOverView();
    }

    @Override
    public void scrollToPos(int pos) {
        mLayoutManager.scrollToPosition(pos);
    }

    public void share(String shareLink) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, shareLink);
        startActivity(Intent.createChooser(intent, "Share With"));
    }

    private boolean isValidString(String string) {
        if(string != null && !string.isEmpty() && !string.matches("null"))
            return true;
        return false;
    }

    public void initActivityRefs() {
        if(attractionDetail == null)
            attractionDetail = (AttractionDetail) getActivity();
    }

}
