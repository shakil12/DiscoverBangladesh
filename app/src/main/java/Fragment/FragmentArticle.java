package Fragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Adapter.AdapterArticle;
import ModelClass.EmnoModel;
import discoverbd.robi.com.discoverbangladesh.AttractionDetail;
import discoverbd.robi.com.discoverbangladesh.HttpDataHandler;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by Shakil on 22-Dec-16.
 */

public class FragmentArticle extends Fragment{

    private RecyclerView recv_article;
    private List<EmnoModel> listUsers;
    private AdapterArticle adapterArticle;

    // for on Data loading
    private AttractionDetail attractionDetail;
    OnActivityCreatedArticle mCallback;

    public FragmentArticle() {
        // Required empty public constructor
    }

    public static FragmentArticle newInstance() {
        
        Bundle args = new Bundle();
        
        FragmentArticle fragment = new FragmentArticle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_article, container, false);
        recv_article = (RecyclerView) rootView.findViewById(R.id.recv_article);
        //Log.d("SHAKIL", "yep from oncreateview of article fragment and recv_article = "+recv_article);
        setRecyclerView();
        // for checking if activity has implemented interface and accessing data var from attractionDetail
        attractionDetail = (AttractionDetail) getActivity();
        return rootView;
    }

    private void setRecyclerView() {
        if(listUsers == null || listUsers.size() == 0)
            listUsers = new ArrayList<EmnoModel>();
        listUsers.add(new EmnoModel("name", "sdfsf"));
        listUsers.add(new EmnoModel("name", "sdfsf"));
        listUsers.add(new EmnoModel("name", "sdfsf"));
        listUsers.add(new EmnoModel("name", "sdfsf"));
        listUsers.add(new EmnoModel("name", "sdfsf"));
        listUsers.add(new EmnoModel("name", "sdfsf"));
        listUsers.add(new EmnoModel("name", "sdfsf"));

        adapterArticle = new AdapterArticle(getActivity(), listUsers);
        GridLayoutManager glm = new GridLayoutManager(getActivity(), 2);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recv_article.setLayoutManager(mLayoutManager);
        recv_article.setItemAnimator(new DefaultItemAnimator());
        recv_article.setAdapter(adapterArticle);
        //adapterArticle.notifyDataSetChanged();

        //new UserCmntAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        //Log.d("SHAKIL", "yep setting the recyclerview thing is just finished");
    }

    private class UserCmntAsync extends AsyncTask<Void, Void, String> {
        String result = null;
        JSONObject jsonObject, jsonObject2;
        JSONArray jsonArray;
        @Override
        protected void onPreExecute() {
            //Log.d("SHAKIL", "onPostExecute of the comment async task ");
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {

            String url = "http://www.softwindtech.com/android-works/ATM_COMMENT.php";
            try {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("atm_id", Integer.toString(173));
                result = new HttpDataHandler().performPostCall(url, hashMap);
            } catch (Exception e) {

                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if(result != null && !result.isEmpty() && !result.equals("null")) {
                //Log.d("SHAKIL", "result = "+result);
                try {
                    jsonObject = new JSONObject(result);
                    jsonArray = jsonObject.getJSONArray("commentdata");
                    for(int i = 0; i<jsonArray.length(); i++) {
                        jsonObject2 = jsonArray.getJSONObject(i);
                        String comment = jsonObject2.getString("comment");
                        String user_fbid = jsonObject2.getString("user_fbid");
                        String comment_time = jsonObject2.getString("comment_time");
                        String name = jsonObject2.getString("name");
                        //comment += "\nat "+comment_time;
                        listUsers.add(new EmnoModel(name, comment));
                        //listUsers.add(new UserModel(user_fbid, name, comment, comment_time));
                        //listUsers.add(new UserModel(user_fbid, name, "yep very good atm booths but i thinks there could be some problem " +
                        //        "using this booth when you are going to that booth at mid night.", comment_time));
                        //listUsers.add(new UserModel(user_fbid, name, comment, comment_time));
                        adapterArticle.notifyDataSetChanged();

                    }
                    if(jsonArray.length() != 0) {
                        listUsers.add(new EmnoModel("", ""));
                        //listv_comment.setDividerHeight(2);
                    }else {
                        listUsers.add(new EmnoModel("Be first to comment", ""));
                        //listv_comment.setDividerHeight(0);
                    }
                    adapterArticle.notifyDataSetChanged();
                    //setListViewHeightBasedOnChildren(listv_comment);
                    //txtv_commentno.setText(Integer.toString(jsonArray.length()) + " Comments");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else {
                Toast.makeText(getActivity(), "Sorry something went wrong.", Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(s);
        }
    }

    public void attachDataToView() {
        initActivityRefs();
        if(attractionDetail == null)
            return;
        if(attractionDetail.listArticles != null) {
            recv_article.setAdapter(null);
            adapterArticle = new AdapterArticle(getActivity(), attractionDetail.listArticles);
            GridLayoutManager glm = new GridLayoutManager(getActivity(), 2);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            recv_article.setLayoutManager(mLayoutManager);
            recv_article.setItemAnimator(new DefaultItemAnimator());
            recv_article.setAdapter(adapterArticle);
            adapterArticle.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //Log.d("SHAKIL", "from onDestroy of Gallery");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mCallback.setDataToArticle();
        //Log.d("SHAKIL", "from onActivityCreated of Review");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnActivityCreatedArticle) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }

    }

    public interface OnActivityCreatedArticle {
        public void setDataToArticle();
    }

    public void initActivityRefs() {
        if(attractionDetail == null)
            attractionDetail = (AttractionDetail) getActivity();
    }
}
