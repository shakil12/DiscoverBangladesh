package Fragment;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.List;

import Adapter.AdapterArticle;
import Adapter.AdapterMap;
import ModelClass.EmnoModel;
import ModelClass.MapModel;
import discoverbd.robi.com.discoverbangladesh.AttractionDetail;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by Shakil on 22-Dec-16.
 */

public class FragmentMap extends Fragment implements
        OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    MapView mMapView;
    private GoogleMap googleMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LatLng uLatLng = null;
    private Marker currLocationMarker;

    private FloatingActionButton fab_uloc, fab_locDir;

    // for recyclerView
    private RecyclerView recv_map;
    private List<MapModel> listMap;
    private AdapterMap adapterMap;

    // for on Data loading
    private AttractionDetail attractionDetail;
    OnActivityCreatedMap mCallback;

    private boolean mapReady = false;
    private Marker locMarker = null;
    LatLng attractionLocation;

    public FragmentMap() {
        // Required empty public constructor
    }

    public static FragmentMap newInstance() {

        Bundle args = new Bundle();

        FragmentMap fragment = new FragmentMap();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflat and return the layout
        View v = inflater.inflate(R.layout.fragment_map, container, false);
        mMapView = (MapView) v.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        //
        fab_locDir = (FloatingActionButton) v.findViewById(R.id.fab_locDir);
        fab_uloc = (FloatingActionButton) v.findViewById(R.id.fab_uloc);
        fab_uloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showUserLocOnMap();
            }
        });

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        initMap();

        // for recyclerView
        recv_map = (RecyclerView) v.findViewById(R.id.recv_map);
        //Log.d("SHAKIL", "yep from oncreateview of article fragment and recv_article = "+recv_article);
        setRecyclerView();

        // for checking if activity has implemented interface and accessing data var from attractionDetail
        attractionDetail = (AttractionDetail) getActivity();
        return v;
    }

    private void initMap() {
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mapReady = true;
                googleMap = mMap;

                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(getContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(true);

                // For dropping a marker at a point on the Map
                LatLng sydney = new LatLng(23.793564, 90.408673);
                locMarker = googleMap.addMarker(new MarkerOptions()
                        .position(sydney)
                        .title("")
                        .snippet("This place is awesome"));

                animateCameraToLocation(sydney);
                Log.d("SHAKIL", "yep google map is ready");
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }

    protected synchronized void buildGoogleApiClient() {
        //Toast.makeText(getActivity(), "buildGoogleApiClient", Toast.LENGTH_SHORT).show();
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void showUserLocOnMap()
    {
        if(uLatLng != null && googleMap != null) {
            //zoom to current position:
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(uLatLng).zoom(10).build();

            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
        }
    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            uLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        }
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000); //5 seconds
        mLocationRequest.setFastestInterval(3000); //3 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if(currLocationMarker != null) {
            currLocationMarker.remove();
        }
        if(location != null) {
            uLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        }
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(uLatLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        currLocationMarker = googleMap.addMarker(markerOptions);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    @Override
    public void onDestroy() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        if(mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        super.onDestroy();
    }

    public void attachDataToView() {
        initActivityRefs();
        if(attractionDetail == null)
            return;
        //Log.d("SHAKIL", "attachDataToView is clled for map Fragment and dataloaded = "+attractionDetail.dataLoaded);
        if(attractionDetail.dataLoaded && mapReady) {
            Double lat = Double.parseDouble(attractionDetail.latitude);
            Double lng = Double.parseDouble(attractionDetail.longitude);
            if(lat != 0 && lng != 0) {
                attractionLocation = new LatLng(lat, lng);
                Log.d("SHAKIL", "yep attraction loc is not null "+attractionLocation);
            } else {
                attractionLocation = new LatLng(23.789048, 90.376143);
            }
            if(locMarker != null)
               locMarker.remove();
            locMarker = googleMap.addMarker(new MarkerOptions()
                    .position(attractionLocation)
                    .title(attractionDetail.title)
                    .snippet(attractionDetail.title+", "+attractionDetail.name));
            animateCameraToLocation(attractionLocation);

            // setting the recyclerView
            if(attractionDetail.title != null && attractionDetail.description != null) {
                if(listMap.size() == 0)
                    listMap.add(new MapModel(attractionDetail.title, attractionDetail.description));

                recv_map.setAdapter(null);
                adapterMap = new AdapterMap(getActivity(), listMap);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                recv_map.setLayoutManager(mLayoutManager);
                recv_map.setItemAnimator(new DefaultItemAnimator());
                recv_map.setAdapter(adapterMap);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //Log.d("SHAKIL", "from onDestroy of Gallery");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mCallback.setDataToMap();
        //Log.d("SHAKIL", "from onActivityCreated of Review");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnActivityCreatedMap) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }

    }

    public interface OnActivityCreatedMap {
        public void setDataToMap();
    }

    private void animateCameraToLocation(LatLng location) {
        // For zooming automatically to the location of the marker for zooming into the location and also for the last name we
        CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(10).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        if(uLatLng != null)

            //LatLngBounds bound = toBounds(origin, SphericalUtil.computeDistanceBetween(origin, dest));
        try{
            Log.d("SHAKIL", "attractionLatlng = "+attractionLocation+" ulatLng = "+uLatLng);
            googleMap.setPadding(20, 20, 20, 20);
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(new LatLngBounds(attractionLocation, uLatLng), 0);
            googleMap.animateCamera(cu, 1000, null); } catch (Exception e) {Log.d("SHAKIL", "err = "+e.toString()); }
    }

    public void initActivityRefs() {
        if(attractionDetail == null)
            attractionDetail = (AttractionDetail) getActivity();
    }

    private void setRecyclerView() {
        if(listMap == null || listMap.size() == 0)
            listMap = new ArrayList<MapModel>();

        adapterMap = new AdapterMap(getActivity(), listMap);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recv_map.setLayoutManager(mLayoutManager);
        recv_map.setItemAnimator(new DefaultItemAnimator());
        recv_map.setAdapter(adapterMap);
    }
}
