package Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import java.util.ArrayList;
import java.util.List;

import Adapter.AdapterArticle;
import Adapter.AdapterGallery;
import Adapter.AdapterReview;
import ModelClass.EmnoModel;
import ModelClass.GalleryModel;
import discoverbd.robi.com.discoverbangladesh.AttractionDetail;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by Shakil on 22-Dec-16.
 */

public class FragmentGallery extends Fragment{

    private RecyclerView recv_gallery;
    private List<GalleryModel> listUsers;
    private AdapterGallery adapterGallery;

    // for youtube video
    private WebView webv_video;

    // for on Data loading
    private AttractionDetail attractionDetail;
    OnActivityCreatedGallery mCallback;

    public FragmentGallery() {
        // Required empty public constructor
    }

    public static FragmentGallery newInstance() {

        Bundle args = new Bundle();

        FragmentGallery fragment = new FragmentGallery();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);
        recv_gallery = (RecyclerView) rootView.findViewById(R.id.recv_gallery);
        webv_video = (WebView) rootView.findViewById(R.id.webv_video);
        //Log.d("SHAKIL", "yep from oncreateview of article fragment and recv_article = "+recv_article);
        setRecyclerView();
        // for ondata loaded
        attractionDetail = (AttractionDetail) getActivity();
        Log.d("SHAKIL", "from onCreateView of Gallery");
        return rootView;
    }

    private void setRecyclerView() {
        if(listUsers == null || listUsers.size() == 0)
            listUsers = new ArrayList<GalleryModel>();
        listUsers.add(new GalleryModel("name", "sdfsf"));
        listUsers.add(new GalleryModel("name", "sdfsf"));
        listUsers.add(new GalleryModel("name", "sdfsf"));
        listUsers.add(new GalleryModel("name", "sdfsf"));
        listUsers.add(new GalleryModel("name", "sdfsf"));
        listUsers.add(new GalleryModel("name", "sdfsf"));
        listUsers.add(new GalleryModel("name", "sdfsf"));

        adapterGallery = new AdapterGallery(getActivity(), listUsers);
        GridLayoutManager glm = new GridLayoutManager(getActivity(), 2);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recv_gallery.setLayoutManager(glm);
        recv_gallery.setItemAnimator(new DefaultItemAnimator());
        recv_gallery.setAdapter(adapterGallery);
        adapterGallery.notifyDataSetChanged();
        //new UserCmntAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        //Log.d("SHAKIL", "yep setting the recyclerview thing is just finished");
        String url = "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/xHHto0cJOqs\" frameborder=\"0\" allowfullscreen></iframe>";
        String urls = String.valueOf(Html.fromHtml(url));
        webv_video.getSettings().setLoadsImagesAutomatically(true);
        webv_video.getSettings().setJavaScriptEnabled(true);
        webv_video.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webv_video.loadData(url, "text/html; charset=UTF-8", null);
    }

    public void attachDataToView() {
        initActivityRefs();
        if(attractionDetail == null)
            return;
        Log.d("SHAKIL", "attachDataToView is clled for review Fragment and dataloaded = "+attractionDetail.dataLoaded);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //Log.d("SHAKIL", "from onDestroy of Gallery");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mCallback.setDataToGallery();
        //Log.d("SHAKIL", "from onActivityCreated of Review");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnActivityCreatedGallery) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }

    }

    public interface OnActivityCreatedGallery{
        public void setDataToGallery();
    }

    public void initActivityRefs() {
        if(attractionDetail == null)
            attractionDetail = (AttractionDetail) getActivity();
    }

}
