package Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import Adapter.AdapterArticle;
import Adapter.AdapterReview;
import ModelClass.EmnoModel;
import ModelClass.OverviewModel;
import ModelClass.ReviewModel;
import discoverbd.robi.com.discoverbangladesh.AttractionDetail;
import discoverbd.robi.com.discoverbangladesh.R;

import static android.R.style.Animation;

/**
 * Created by Shakil on 22-Dec-16.
 */

public class FragmentReview extends Fragment implements AdapterReview.AdapterCallback{

    private RecyclerView recv_review;
    private List<ReviewModel> listReview;
    private AdapterReview adapterReview;

    // hiding linearv_mid for giving more space for recv_review
    private LinearLayout linearv_mid;
    private int linearv_mid_Height;
    android.support.v7.widget.LinearLayoutManager mLayoutManager;
    private android.view.animation.Animation anim_reviewform_hides;

    // for scroll and collapse toolbar other than recyclerview scroll
    OnReviewTopOutOfSpace mCallbacks;
    Helper.CustomLinearLayout linearv_sp;
    private boolean touchTaken = false;
    private float firstTouchY = 0;

    // for on Data loading
    private AttractionDetail attractionDetail;
    OnActivityCreatedReview mCallback;

    public FragmentReview() {
        // Required empty public constructor
    }

    public static FragmentReview newInstance() {

        Bundle args = new Bundle();

        FragmentReview fragment = new FragmentReview();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_review, container, false);
        recv_review = (RecyclerView) rootView.findViewById(R.id.recv_review);
        linearv_mid = (LinearLayout) rootView.findViewById(R.id.linearv_mid);
        linearv_mid.post(new Runnable() {
            @Override
            public void run() {
                linearv_mid_Height = linearv_mid.getMeasuredHeight();
                Log.d("SHAKIL", "linear v height = "+linearv_mid_Height);
            }
        });
        anim_reviewform_hides = AnimationUtils.loadAnimation(getContext(), R.anim.anim_reviewform_hides);
        //Log.d("SHAKIL", "yep from oncreateview of article fragment and recv_article = "+recv_article);
        setRecyclerView();
        // for scroll and collapse toolbar other than recyclerview scroll
        linearv_sp = (Helper.CustomLinearLayout) rootView.findViewById(R.id.linearv_top);
        setLayout4collapseToolabrOtherThanRecv();
        // for ondata loaded
        attractionDetail = (AttractionDetail) getActivity();
        Log.d("SHAKIL", "from onCreateView of Review");
        return rootView;
    }

    private void setRecyclerView() {
        if(listReview == null || listReview.size() == 0)
            listReview = new ArrayList<ReviewModel>();
        /*listUsers.add(new ReviewModel("Tahmidul Shakil", "sdfsf" , "", getString(R.string.test_description2), "This Place is Awesome", "", "", ""));
        listUsers.add(new ReviewModel("Tahmidul Shakil", "sdfsf" , "", "comment 2", "This Place is Awesome", "", "", ""));
        listUsers.add(new ReviewModel("Tahmidul Shakil", "sdfsf" , "", getString(R.string.test_description), "This Place is Awesome", "", "", ""));
        listUsers.add(new ReviewModel("Tahmidul Shakil", "sdfsf" , "", getString(R.string.test_description), "This Place is Awesome", "", "", ""));
        listUsers.add(new ReviewModel("Tahmidul Shakil", "sdfsf" , "", "", "This Place is Awesome", "", "", ""));
        listUsers.add(new ReviewModel("Tahmidul Shakil", "sdfsf" , "", getString(R.string.test_description2), "This Place is Awesome", "", "", ""));
        listUsers.add(new ReviewModel("Tahmidul Shakil", "sdfsf" , "", "last comment", "This Place is Awesome", "", "", ""));
        */
        adapterReview = new AdapterReview(getActivity(), listReview, this);

        mLayoutManager = new LinearLayoutManager(getContext());
        recv_review.setLayoutManager(mLayoutManager);
        recv_review.setItemAnimator(new DefaultItemAnimator());
        recv_review.setAdapter(adapterReview);
        //adapterReview.notifyDataSetChanged();

        //new UserCmntAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        //Log.d("SHAKIL", "yep setting the recyclerview thing is just finished");
        //recv_review.getViewTreeObserver().addOnScrollChangedListener(new ScrollPositionObserver());
        recv_review.addOnChildAttachStateChangeListener(new ChildAttachListener(mLayoutManager));
        /*recv_review.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0 ) {
                    linearv_mid.post(new Runnable() {
                        @Override
                        public void run() {
                            linearv_mid.setVisibility(View.GONE);
                            linearv_mid.startAnimation(anim_reviewform_hides);
                        }
                    });
                    //linearv_mid.startAnimation(anim_reviewform_hides);
                } else {

                    if(mLayoutManager.findViewByPosition(0) != null) {
                        View v = mLayoutManager.findViewByPosition(0);
                        if(v.getVisibility() == View.VISIBLE) {
                            if(linearv_mid.getVisibility() == View.GONE) {
                                linearv_mid.setVisibility(View.VISIBLE);
                            }
                            Log.d("SHAKIL", "yep first item is visible");
                        } else {
                            Log.d("SHAKIL", "yep first item is not visible");
                        }

                        Log.d("SHAKIL", "yep first item is visiblity = "+mLayoutManager.findViewByPosition(0).getVisibility());
                    }
                    /*linearv_mid.post(new Runnable() {
                        @Override
                        public void run() {
                            linearv_mid.setVisibility(View.VISIBLE);
                        }
                    });
                }
                Log.d("SHAKIL", "scrolly = "+dy);
                int scrollY = Math.min(Math.max(dy, 0), linearv_mid_Height);
                Log.d("SHAKIL", "yep scroll y "+scrollY);

                // changing position of ImageView
                linearv_mid.setScaleY(scrollY);

                // alpha you could set to ActionBar background
                float alpha = scrollY / (float) linearv_mid_Height;
            }
        });*/
    }

    @Override
    public void onMethodCallback() {
        Log.d("SHAKIL", "get called position 0 and visibility = "+linearv_mid.getVisibility());
        /*if(linearv_mid.getVisibility() == View.GONE) {
            linearv_mid.setVisibility(View.VISIBLE);
        }*/
    }

    private class ChildAttachListener implements RecyclerView.OnChildAttachStateChangeListener {
        android.support.v7.widget.LinearLayoutManager llm;

        public ChildAttachListener(android.support.v7.widget.LinearLayoutManager llm){
            super();
            this.llm = llm;
        }

        @Override
        public void onChildViewAttachedToWindow(View view) {
            Log.d("SHAKIL", "findFirstVisibleItemPos = "+llm.findFirstVisibleItemPosition());
            if(llm.findFirstVisibleItemPosition() == -1) {
                if(!isLinearvMidVisible())
                    showLinearvMid();
                Log.d("SHAKIL", "yep first item is visible from attach CHild listener");
            } else {
                if(isLinearvMidVisible())
                    hideLinearvMid();
            }

            Log.d("SHAKIL", "Items size, first Visible Item = "+llm.findFirstVisibleItemPosition());
        }

        @Override
        public void onChildViewDetachedFromWindow(View view) {

        }
    }

    private class ScrollPositionObserver implements ViewTreeObserver.OnScrollChangedListener {

        private int mImageViewHeight;

        public ScrollPositionObserver() {
            mImageViewHeight = linearv_mid_Height;
        }

        @Override
        public void onScrollChanged() {
            int scrollY = Math.min(Math.max(recv_review.getScrollY(), 0), mImageViewHeight);
            Log.d("SHAKIL", "yep scroll y "+recv_review.getScrollY());

            // changing position of ImageView
            linearv_mid.setTranslationY(scrollY / 2);

            // alpha you could set to ActionBar background
            float alpha = scrollY / (float) mImageViewHeight;
        }
    }

    public void collapseOtherLayoutReview(boolean collapse) {
        if(collapse) {
            if(mLayoutManager.findFirstVisibleItemPosition() != 0 && isLinearvMidVisible()) {
                hideLinearvMid();
            }
            //linearv_mid.setVisibility(View.GONE);
            //linearv_mid.startAnimation(anim_reviewform_hides);
        } else {
            if(!isLinearvMidVisible())
                showLinearvMid();
            //linearv_mid.setVisibility(View.VISIBLE);
        }
    }

    private void hideLinearvMid() {
        linearv_mid.post(new Runnable() {
            @Override
            public void run() {
                linearv_mid.setVisibility(View.GONE);
                //linearv_mid.startAnimation(anim_reviewform_hides);
            }
        });
    }

    private void showLinearvMid() {
        linearv_mid.post(new Runnable() {
            @Override
            public void run() {
                linearv_mid.setVisibility(View.VISIBLE);
            }
        });
    }

    private boolean isLinearvMidVisible() {
        if(linearv_mid.getVisibility() == View.GONE) {
            return false;
        }
        return true;
    }

    public void attachDataToView() {
        initActivityRefs();
        if(attractionDetail == null)
            return;
        Log.d("SHAKIL", "attachDataToView is clled for review Fragment and dataloaded = "+attractionDetail.dataLoaded);
        if(attractionDetail.dataLoaded) {
            //Log.d("SHAKIL", "yep data loaded in attractionDetail "+attractionDetail.dataLoaded);
            // set the recv_overview
            if(attractionDetail.listReviews != null) {
                //Log.d("SHAKIL", "yep listreviews not null of attraction detail and now setting adapter");
                recv_review.setAdapter(null);
                adapterReview = new AdapterReview(getActivity(), attractionDetail.listReviews, this);

                mLayoutManager = new LinearLayoutManager(getContext());
                recv_review.setLayoutManager(mLayoutManager);
                recv_review.setItemAnimator(new DefaultItemAnimator());
                recv_review.setAdapter(adapterReview);
                adapterReview.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //Log.d("SHAKIL", "from onDestroy of Review");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //attachDataToView(); for the same of the view is not real
        mCallback.setDataToReview();
        //Log.d("SHAKIL", "from onActivityCreated of Review");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnActivityCreatedReview) activity;
            mCallbacks = (OnReviewTopOutOfSpace) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }

    }

    public interface OnActivityCreatedReview {
        public void setDataToReview();
    }

    public void initActivityRefs() {
        if(attractionDetail == null)
            attractionDetail = (AttractionDetail) getActivity();
    }

    public interface OnReviewTopOutOfSpace {
        public void collapseToolbarOnReviewTopOutOfSpace(boolean collapse);
    }

    private void setLayout4collapseToolabrOtherThanRecv() {
        linearv_sp.initLayout(getContext());
        linearv_sp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent ev) {
                float initTouchY = 0, finalY = 0;
                boolean scrollUp;
                switch (ev.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initTouchY = ev.getRawY();
                        //Log.d("SHAKIL", "yep down from custom linearv at "+ev.getRawY()+" ...................");
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if(!touchTaken) {
                            firstTouchY = ev.getRawY();
                            //Log.d("SHAKIL", "first touchY = "+firstTouchY);
                            touchTaken = true;
                        }
                        finalY = Math.abs(ev.getRawY() - firstTouchY);
                        if(convertPixelsToDp(finalY, getContext()) > 50) {
                            if(firstTouchY < ev.getRawY()) {
                                scrollUp = false;
                            } else {
                                scrollUp = true;
                            }
                            if(scrollUp) {
                                mCallbacks.collapseToolbarOnReviewTopOutOfSpace(true);
                            }
                            else {
                                mCallbacks.collapseToolbarOnReviewTopOutOfSpace(false);
                            }
                        }
                        //Log.d("SHAKIL", "yep move from custom linearv at "+finalY);
                        break;
                    case MotionEvent.ACTION_UP:
                        touchTaken = false;
                        firstTouchY = 0;
                        //Log.d("SHAKIL", "yep up from custom diff =  "+(ev.getRawY() - initTouchY)+" ////////////////////");
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }
}

