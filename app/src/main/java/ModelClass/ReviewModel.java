package ModelClass;

/**
 * Created by Tahmidul on 10/8/2016.
 */

public class ReviewModel {
        private String uName;           // not found
        private String locRate;         // rating
        private String reviewTime;      // created_at
        private String reviewTitle;     // title
        private String reviewDetails;   // description
        private String upVote;          // likes_count
        private String downVote;        // dislikes_count
        private String shareLink;       // not found yet



        public ReviewModel(String uName, String locRate,
                           String reviewTime, String reviewDetails, String reviewTitle,
                           String upVote, String downVote, String shareLink) {
            this.uName = uName;
            this.locRate = locRate;
            this.reviewTime = reviewTime;
            this.reviewDetails = reviewDetails;
            this.reviewTitle = reviewTitle;
            this.upVote = upVote;
            this.downVote = downVote;
            this.shareLink = shareLink;
    }

    public String getuName() {
        return uName;
    }

    public String getLocRate() {
        return locRate;
    }

    public String getReviewTime() {
        return reviewTime;
    }

    public String getReviewDetails() {
        return reviewDetails;
    }

    public String getReviewTitle() {
        return reviewTitle;
    }

    public String getUpVote() {
        return upVote;
    }

    public String getDownVote() {
        return downVote;
    }

    public String getShareLink() {
        return shareLink;
    }

    public void setuName(String emnobankName) {
            this.uName = emnobankName;
        }

    public void setLocRate(String emno) {
            this.locRate = emno;
        }

    public void setReviewTime(String emnobankName) {
        this.reviewTime = emnobankName;
    }

    public void setReviewDetails(String emno) {
        this.reviewDetails = emno;
    }

    public void setReviewTitle(String emno) {
        this.reviewTitle = emno;
    }

    public void setUpVote(String emnobankName) {
        this.upVote = emnobankName;
    }

    public void setDownVote(String emno) {
        this.downVote = emno;
    }

    public void setShareLink(String emno) {
        this.shareLink = emno;
    }



}
