package ModelClass;

/**
 * Created by Tahmidul on 10/8/2016.
 */

public class MapModel {
        private String locTitle;
        private String locDetails;



        public MapModel(String locTitle, String locDetails) {
        this.locTitle = locTitle;
        this.locDetails = locDetails;
    }

        public String getLocTitle() {
            return locTitle;
        }

        public String getLocDetails() {
            return locDetails;
        }


        public void setLocTitle(String emnobankName) {
            this.locTitle = emnobankName;
        }

        public void setLocDetails(String emno) {
            this.locDetails = emno;
        }



}
