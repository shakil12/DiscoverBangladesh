package ModelClass;

/**
 * Created by Tahmidul on 10/8/2016.
 */

public class EmnoModel {
        private String emnobankName;
        private String emno;



        public EmnoModel(String emnobankName, String emno) {
        this.emnobankName = emnobankName;
        this.emno = emno;
    }

        public String getEmnobankName() {
            return emnobankName;
        }

        public String getEmno() {
            return emno;
        }


        public void setEmnobankName(String emnobankName) {
            this.emnobankName = emnobankName;
        }

        public void setEmno(String emno) {
            this.emno = emno;
        }



}
