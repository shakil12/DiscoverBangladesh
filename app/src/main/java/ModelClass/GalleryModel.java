package ModelClass;

/**
 * Created by Tahmidul on 10/8/2016.
 */

public class GalleryModel {
        private String galleryId;
        private String shareLink;



        public GalleryModel(String galleryId, String shareLink) {
        this.shareLink = shareLink;
        this.galleryId = galleryId;
    }

        public String getGalleryId() {
            return galleryId;
        }

        public String getShareLink() {
            return shareLink;
        }


        public void setGalleryId(String emnobankName) {
            this.galleryId = emnobankName;
        }

        public void setShareLink(String emno) {
            this.shareLink = emno;
        }



}
