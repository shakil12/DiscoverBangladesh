package ModelClass;

/**
 * Created by Tahmidul on 10/8/2016.
 */

public class OverviewModel {

        private String metaId;
        private String titleQuery;
        private String titleAns;
        private String metaIconLink;

        public OverviewModel(String metaId, String titleQuery, String titleAns, String metaIconLink) {
            this.metaId = metaId;
            this.titleQuery = titleQuery;
            this.titleAns = titleAns;
            this.metaIconLink = metaIconLink;
        }

        public String getMetaId() {
            return metaId;
        }

        public String getTitleQuery() {
            return titleQuery;
        }

        public String getTitleAns() {
            return titleAns;
        }

        public String getMetaIconLink() {
            return metaIconLink;
        }

        public void setMetaId(String emnobankName) {
            this.metaId = emnobankName;
        }

        public void setTitleQuery(String emno) {
            this.titleQuery = emno;
        }

        public void setTitleAns(String emnobankName) {
            this.titleAns = emnobankName;
        }

        public void setMetaIconLink(String emno) {
            this.metaIconLink = emno;
        }



}
