package Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import Adapters.ArticleRecycleViewAdapter;
import Adapters.HorizontalCategoryRecycleViewAdapter;
import AllStatic.AddToLikedList;
import AllStatic.AllStaticDataHolder;
import AllStatic.VectorToBitmap;
import Holder.ArticlesRecycleViewDatasetHolder;
import Holder.HorizontalCategoryRecycleViewDatasetHolder;
import discoverbd.robi.com.discoverbangladesh.ArticleDetailActivity;
import discoverbd.robi.com.discoverbangladesh.R;


public class ArticlesHomeFragment extends Fragment implements HorizontalCategoryRecycleViewAdapter.OnHorizontalCategoryRecycleViewItemClickListener, ArticleRecycleViewAdapter.OnArticleClickListener {


    RecyclerView rcyView_horizontal_locations;
    ArrayList<HorizontalCategoryRecycleViewDatasetHolder> categoryDatasetHolder;

    RecyclerView rcyView_articleList;
    private ArrayList<ArticlesRecycleViewDatasetHolder> articleDatasetHolder;

    OnArticlesMainFragmentListener mCallback;
    Context context;

   // ArrayList<ArticleListModelClass> articleList;

    public ArticlesHomeFragment() {
        categoryDatasetHolder = new ArrayList<>();
        articleDatasetHolder = new ArrayList<>();
    }

    public void add_context(Context c){
        context = c;
        try {
            mCallback = (OnArticlesMainFragmentListener) context;
        }
        catch (Exception e){}
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_articles_home, container, false);

     /*   Bitmap bitmap = VectorToBitmap.getBitmapFromVectorDrawable(getContext(),R.drawable.drawable_default_image);

        if(articleDatasetHolder.size()>0)
            articleDatasetHolder.clear();*/
    /*    for(int i = 0; i<10; i++) {
            HorizontalCategoryRecycleViewDatasetHolder hrvdsh = new HorizontalCategoryRecycleViewDatasetHolder();
            hrvdsh.setText("tag");
            hrvdsh.setSelected(true);
            categoryDatasetHolder.add(hrvdsh);*/
/*
            ArticlesRecycleViewDatasetHolder arvdsh = new ArticlesRecycleViewDatasetHolder();
            arvdsh.setTitle("Are you even trying?");
            arvdsh.setAuthor("Wokong");
            arvdsh.setLike_num("999");
            arvdsh.setTime("5 days ago");
            arvdsh.setShareLink(Integer.toString(i));
            arvdsh.setBookmarked(false);
            arvdsh.setMain_image(bitmap);
            articleDatasetHolder.add(arvdsh);*/
            //if(AllStaticDataHolder.ALL_ARTICLES != null)
  //          update_list();
//        }





        // category rcy view
        rcyView_horizontal_locations = (RecyclerView) view.findViewById(R.id.rcyView_horizontal_locations);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rcyView_horizontal_locations.setLayoutManager(layoutManager);
        rcyView_horizontal_locations.setHasFixedSize(false);
        rcyView_horizontal_locations.setAdapter(new HorizontalCategoryRecycleViewAdapter(this,categoryDatasetHolder, 0));

        //article rcy view
        rcyView_articleList = (RecyclerView) view.findViewById(R.id.rcyView_articles);
        rcyView_articleList.setLayoutManager(new LinearLayoutManager(getContext()));
        rcyView_articleList.setHasFixedSize(false);
        rcyView_articleList.setAdapter(new ArticleRecycleViewAdapter(getContext(),this,articleDatasetHolder,true));
        //rcyView_articleList.setAdapter(new ArticleListViewAdapter(getContext(),R.layout.article_rcyview_item,articleList));


        if(AllStaticDataHolder.ALL_ARTICLES != null)
            update_list();

        return view;
    }

    private boolean is_tag_selected(ArticlesRecycleViewDatasetHolder dt){
        for (HorizontalCategoryRecycleViewDatasetHolder cat :
                categoryDatasetHolder) {

            for (HorizontalCategoryRecycleViewDatasetHolder dt_cat :
                    dt.getTag_list()) {
                if (dt_cat.getId() == cat.getId() && cat.getSelected())
                    return true;
            }
        }
        return false;
    }

    private boolean is_already_in_list(ArticlesRecycleViewDatasetHolder dt){
        for (ArticlesRecycleViewDatasetHolder d :
                articleDatasetHolder) {
            if (d.getId() == dt.getId())
                return true;
        }
        return false;
    }

    public void update_list(){

        if(AllStaticDataHolder.ALL_ARTICLES != null) {
            if (articleDatasetHolder.size() > 0)
                articleDatasetHolder.clear();

            for (ArticlesRecycleViewDatasetHolder dt :
                    AllStaticDataHolder.ALL_ARTICLES) {
                if (!is_already_in_list(dt) && is_tag_selected(dt))
                    articleDatasetHolder.add(dt);
            }
            if (articleDatasetHolder != null && rcyView_articleList != null) {
                rcyView_articleList.getAdapter().notifyDataSetChanged();
            }
        }
    }

    public void update_tag_list(){
        if(AllStaticDataHolder.ALL_ARTICLES_TAGS != null){
            if(categoryDatasetHolder.size() > 0)
                categoryDatasetHolder.clear();
            for (HorizontalCategoryRecycleViewDatasetHolder dt :
                    AllStaticDataHolder.ALL_ARTICLES_TAGS) {

                categoryDatasetHolder.add(dt);
            }
            if(rcyView_horizontal_locations != null)
                rcyView_horizontal_locations.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void onArticleClick(int position) {
        Intent intent = new Intent(getContext(), ArticleDetailActivity.class);
      /*  intent.putExtra("author",articleDatasetHolder.get(position).getAuthor());
        intent.putExtra("time",articleDatasetHolder.get(position).getTime());
        intent.putExtra("title",articleDatasetHolder.get(position).getTitle());
        intent.putExtra("like",articleDatasetHolder.get(position).getLike_num());
        intent.putExtra("shareLink",articleDatasetHolder.get(position).getShareLink());
        intent.putExtra("bookmarked",articleDatasetHolder.get(position).isBookmarked()); */
        intent.putExtra("id", (int) articleDatasetHolder.get(position).getId());
        startActivity(intent);
    }


    @Override
    public void onBookmarkClick(int position, boolean isBookmared) {
        articleDatasetHolder.get(position).setBookmarked(isBookmared);

        double id = articleDatasetHolder.get(position).getId();
        for (ArticlesRecycleViewDatasetHolder dt :
                AllStaticDataHolder.ALL_ARTICLES) {
            if(id == dt.getId()){
                dt.setBookmarked(isBookmared);
            }
        }
        if(mCallback != null)
            mCallback.onArticlesFragmentBookmarkedClicked();
    }

    @Override
    public void onHorizontalCategoryRecycleViewItemClick(int tag, int position) {
        update_list();
    }

    //////interface
    public interface OnArticlesMainFragmentListener {
        public void onArticlesFragmentBookmarkedClicked();
    }
}
