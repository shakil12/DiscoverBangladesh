package Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import Adapters.HorizontalCategoryRecycleViewAdapter;
import Holder.HorizontalCategoryRecycleViewDatasetHolder;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoUploadFragment extends Fragment implements HorizontalCategoryRecycleViewAdapter.OnHorizontalCategoryRecycleViewItemClickListener{

    RecyclerView rcyView_horizontal_locations, rcyView_horizontal_videos;
    ArrayList<HorizontalCategoryRecycleViewDatasetHolder> locationDatasetHolder, videoDatasetHolder;

    public VideoUploadFragment() {
        locationDatasetHolder = new ArrayList<>();
        videoDatasetHolder = new ArrayList<>();

        for(int i = 0; i<10; i++) {
            // st.add("adfsydgasifh");
            HorizontalCategoryRecycleViewDatasetHolder hrvdsh = new HorizontalCategoryRecycleViewDatasetHolder();
            hrvdsh.setText("tag");
            hrvdsh.setSelected(false);
            locationDatasetHolder.add(hrvdsh);

            HorizontalCategoryRecycleViewDatasetHolder vhrvdsh = new HorizontalCategoryRecycleViewDatasetHolder();
            vhrvdsh.setText("video tag");
            vhrvdsh.setSelected(false);
            videoDatasetHolder.add(vhrvdsh);
        }
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_video_upload, container, false);

        rcyView_horizontal_locations = (RecyclerView) v.findViewById(R.id.rcyView_horizontal_locations);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rcyView_horizontal_locations.setLayoutManager(layoutManager);
        rcyView_horizontal_locations.setHasFixedSize(false);
        rcyView_horizontal_locations.setAdapter(new HorizontalCategoryRecycleViewAdapter(this,locationDatasetHolder, 0));

        rcyView_horizontal_videos = (RecyclerView) v.findViewById(R.id.rcyView_horizontal_videos);
        LinearLayoutManager layoutManager1
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rcyView_horizontal_videos.setLayoutManager(layoutManager1);
        rcyView_horizontal_videos.setHasFixedSize(false);
        rcyView_horizontal_videos.setAdapter(new HorizontalCategoryRecycleViewAdapter(this, videoDatasetHolder, 1));

        return v;
    }


    @Override
    public void onHorizontalCategoryRecycleViewItemClick(int tag, int position) {
        if(tag == 0){

        }
        else if(tag == 1){

        }
    }
}
