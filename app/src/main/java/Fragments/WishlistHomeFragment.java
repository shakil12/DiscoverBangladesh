package Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import Adapters.WishlistRecycleViewAdapter;
import AllStatic.AddToWishListAsync;
import AllStatic.AllStaticDataHolder;
import AllStatic.VectorToBitmap;
import Holder.AttractionsGridViewDatasetHolder;
import discoverbd.robi.com.discoverbangladesh.AttractionDetail;
import discoverbd.robi.com.discoverbangladesh.R;


public class WishlistHomeFragment extends Fragment implements WishlistRecycleViewAdapter.OnWishlistRecycleViewItemClickListener{

    RecyclerView rcyView_wishList;
    ArrayList<AttractionsGridViewDatasetHolder> wishlistDatasetHolder;

    Context context;
    OnWishlistMainFragmentListener mCallback;

    public WishlistHomeFragment() {
        wishlistDatasetHolder = new ArrayList<>();

    }

    public void add_context(Context c){
        context = c;
        try {
            mCallback = (OnWishlistMainFragmentListener) context;
        }
        catch (Exception e){}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_wishlist_home, container, false);

/*
        Bitmap bitmap = VectorToBitmap.getBitmapFromVectorDrawable(getContext(),R.drawable.drawable_default_image);
        if(wishlistDatasetHolder.size()>0)
            wishlistDatasetHolder.clear();
        for(int i = 0; i<10; i++) {
            AttractionsGridViewDatasetHolder agvdh = new AttractionsGridViewDatasetHolder();
            agvdh.setTitle("Andhar Nagar " + Integer.toString(i));
            agvdh.setLocation("Vuter Goli");
            agvdh.setTag("andhar, danger, black");
            agvdh.setTime("added 1000 years ago");
            agvdh.setShareLink(Integer.toString(i));
            agvdh.setMain_image(bitmap);
            agvdh.setIs_wished(true);
            wishlistDatasetHolder.add(agvdh);
        }
*/


        rcyView_wishList = (RecyclerView) v.findViewById(R.id.rcyView_wishlist);
        rcyView_wishList.setLayoutManager(new LinearLayoutManager(getContext()));
        rcyView_wishList.setHasFixedSize(false);
        rcyView_wishList.setAdapter(new WishlistRecycleViewAdapter(getContext(),this,wishlistDatasetHolder));


      //  if(AllStaticDataHolder.ALL_ATTRACTIONS != null)
        update_wishlist();
        return v;
    }

    @Override
    public void onWishListItemClick(int position, boolean isWished) {
        double id = wishlistDatasetHolder.get(position).getId();
        for (AttractionsGridViewDatasetHolder dt :
                AllStaticDataHolder.ALL_ATTRACTIONS) {
            if(id == dt.getId()){
                dt.setIs_wished(isWished);
                new AddToWishListAsync(getContext(),(int)id,isWished).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            }
        }

        wishlistDatasetHolder.get(position).setIs_wished(isWished);
        wishlistDatasetHolder.remove(position);
        rcyView_wishList.getAdapter().notifyDataSetChanged();

        if(mCallback != null)
            mCallback.onWishlistFragmentWishlistClicked();
    }

    @Override
    public void onAttractionClick(int position) {
        int id = (int) wishlistDatasetHolder.get(position).getId();
        Intent intent = new Intent(getContext(), AttractionDetail.class);
        intent.putExtra("id", id);
        startActivity(intent);
    }


    public void update_wishlist(){

        if(AllStaticDataHolder.ALL_ATTRACTIONS != null) {
            if (wishlistDatasetHolder.size() > 0)
                wishlistDatasetHolder.clear();

            for (AttractionsGridViewDatasetHolder dt :
                    AllStaticDataHolder.ALL_ATTRACTIONS) {
                if (dt.is_wished())
                    wishlistDatasetHolder.add(dt);
            }
            if (wishlistDatasetHolder != null && rcyView_wishList != null) {
                rcyView_wishList.getAdapter().notifyDataSetChanged();
            }
        }
    }

    //////interface
    public interface OnWishlistMainFragmentListener {
        public void onWishlistFragmentWishlistClicked();
    }
}
