package Fragments;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Pattern;

import Adapters.SearchRecycleViewAdapter;
import Adapters.WishlistRecycleViewAdapter;
import AllStatic.AddToWishListAsync;
import AllStatic.AllStaticDataHolder;
import AllStatic.VectorToBitmap;
import Holder.AttractionsGridViewDatasetHolder;
import Holder.HorizontalCategoryRecycleViewDatasetHolder;
import discoverbd.robi.com.discoverbangladesh.AttractionDetail;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements SearchRecycleViewAdapter.OnSearchRecycleViewItemClickListener{



    private int frag_no = 0; // 0-->newest, 1-->rating, 2-->checkedIn, 3-->popularity

    RecyclerView rcyView_search;
    ArrayList<AttractionsGridViewDatasetHolder> searchDatasetHolder;

    private Bitmap bitmap;
    private Context context;
    OnSearchFragmentListener mCallback;

    public SearchFragment() {
        searchDatasetHolder = new ArrayList<>();
        // Required empty public constructor
    }

    public void add_context(Context c){
        context = c;
        try {
            mCallback = (OnSearchFragmentListener) context;
        }
        catch (Exception e){}
    }

    public void setFragmentNo(int i){
        frag_no = i;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_search, container, false);

        bitmap = VectorToBitmap.getBitmapFromVectorDrawable(getContext(),R.drawable.drawable_default_image);

        rcyView_search = (RecyclerView) v.findViewById(R.id.rcyView_search);
        rcyView_search.setLayoutManager(new LinearLayoutManager(getContext()));
        rcyView_search.setHasFixedSize(false);
        rcyView_search.setAdapter(new SearchRecycleViewAdapter(getContext(),this,searchDatasetHolder));
        return v;
    }

   /* public void updateSearch(Context context,int kk){
        if(searchDatasetHolder.size() > 0)
            searchDatasetHolder.clear();

        if(bitmap == null)
            bitmap = VectorToBitmap.getBitmapFromVectorDrawable(context,R.drawable.drawable_default_image);
        for(int i = 0; i<kk; i++){
            AttractionsGridViewDatasetHolder dsh = new AttractionsGridViewDatasetHolder();
            dsh.setTitle("kala Shagor");
            dsh.setTag("shagor, somuddro");
            dsh.setLocation("Chittagong");
            dsh.setShort_des("Its a place of sea. Not river. neverjoijg ergoij rg ergirhjgpr h.");
            dsh.setMain_image(bitmap);
            dsh.setIs_wished(false);
            searchDatasetHolder.add(dsh);
        }

        if(rcyView_search != null)
          rcyView_search.getAdapter().notifyDataSetChanged();
    }*/


    public void update_newsest(String st,ArrayList<HorizontalCategoryRecycleViewDatasetHolder> cat_array){
        if(searchDatasetHolder.size() > 0)
            searchDatasetHolder.clear();
        searchDatasetHolder = update_by_string(st,update_by_category(cat_array));

        Collections.sort(searchDatasetHolder, new Comparator<AttractionsGridViewDatasetHolder>() {
            @Override
            public int compare(AttractionsGridViewDatasetHolder t0, AttractionsGridViewDatasetHolder t1) {
                int i = 0;
                if(t0.getCreated_date().getTime()>t1.getCreated_date().getTime())
                    i = -1;
                else if(t0.getCreated_date().getTime()<t1.getCreated_date().getTime())
                    i = 1;
                return i;
            }
        });

        if(rcyView_search != null) {
            Log.d("Fahim", Integer.toString(searchDatasetHolder.size()));
            rcyView_search.setAdapter(new SearchRecycleViewAdapter(getContext(),this,searchDatasetHolder));
        }
    }


    public void update_rating(String st,ArrayList<HorizontalCategoryRecycleViewDatasetHolder> cat_array){
        if(searchDatasetHolder.size() > 0)
            searchDatasetHolder.clear();
        searchDatasetHolder = update_by_string(st,update_by_category(cat_array));

        Collections.sort(searchDatasetHolder, new Comparator<AttractionsGridViewDatasetHolder>() {
            @Override
            public int compare(AttractionsGridViewDatasetHolder t0, AttractionsGridViewDatasetHolder t1) {
                int i = 0;
                if(t0.getRating_value()>t1.getRating_value())
                    i = -1;
                else if(t0.getRating_value()<t1.getRating_value())
                    i = 1;
                return i;
            }
        });

        if(rcyView_search != null) {
            Log.d("Fahim", Integer.toString(searchDatasetHolder.size()));
            rcyView_search.setAdapter(new SearchRecycleViewAdapter(getContext(),this,searchDatasetHolder));
        }
    }

    public void update_checkedIn(String st,ArrayList<HorizontalCategoryRecycleViewDatasetHolder> cat_array){
        if(searchDatasetHolder.size() > 0)
            searchDatasetHolder.clear();
        ArrayList<AttractionsGridViewDatasetHolder> arrayList = update_by_string(st,update_by_category(cat_array));

        for (AttractionsGridViewDatasetHolder dt :
             arrayList ) {
            if(dt.is_checkedIn())
                searchDatasetHolder.add(dt);
        }

        if(rcyView_search != null) {
            Log.d("Fahim", Integer.toString(searchDatasetHolder.size()));
            rcyView_search.setAdapter(new SearchRecycleViewAdapter(getContext(),this,searchDatasetHolder));
        }
    }

    public void update_popularity(String st,ArrayList<HorizontalCategoryRecycleViewDatasetHolder> cat_array){
        if(searchDatasetHolder.size() > 0)
            searchDatasetHolder.clear();
        searchDatasetHolder = update_by_string(st,update_by_category(cat_array));

        Collections.sort(searchDatasetHolder, new Comparator<AttractionsGridViewDatasetHolder>() {
            @Override
            public int compare(AttractionsGridViewDatasetHolder t0, AttractionsGridViewDatasetHolder t1) {
                int i = 0;
                if(t0.getReviews_value()>t1.getReviews_value())
                    i = -1;
                else if(t0.getReviews_value()<t1.getReviews_value())
                    i = 1;
                return i;
            }
        });

        if(rcyView_search != null) {
            Log.d("Fahim", Integer.toString(searchDatasetHolder.size()));
            rcyView_search.setAdapter(new SearchRecycleViewAdapter(getContext(),this,searchDatasetHolder));
        }
    }


    private ArrayList<AttractionsGridViewDatasetHolder> update_by_string(String st, ArrayList<AttractionsGridViewDatasetHolder> dt_list){
        if(st == null || st.equals("")){
            return dt_list;
        }
        ArrayList<AttractionsGridViewDatasetHolder> list = new ArrayList<>();
        for (AttractionsGridViewDatasetHolder dt :
                dt_list) {
            //if(dt.getTitle().c(st) || dt.getLocation().equalsIgnoreCase(st))
            if(Pattern.compile(Pattern.quote(st),Pattern.CASE_INSENSITIVE).matcher(dt.getTitle()).find()
                    || Pattern.compile(Pattern.quote(st),Pattern.CASE_INSENSITIVE).matcher(dt.getLocation()).find())
                list.add(dt);
        }
        return list;
    }

    private ArrayList<AttractionsGridViewDatasetHolder> update_by_category(ArrayList<HorizontalCategoryRecycleViewDatasetHolder> cat_array){
        ArrayList<AttractionsGridViewDatasetHolder> arrayList = new ArrayList<>();
        //Log.d("Fahim", Integer.toString(AllStaticDataHolder.ALL_ATTRACTIONS.size()));
        if(cat_array.size() == 0 && AllStaticDataHolder.ALL_ATTRACTIONS != null){
            for (AttractionsGridViewDatasetHolder dt :
                    AllStaticDataHolder.ALL_ATTRACTIONS) {
                add_to_list(dt,arrayList);
            }
            //arrayList = AllStaticDataHolder.ALL_ATTRACTIONS;
        }
        else if(cat_array.size()>0){
            for (AttractionsGridViewDatasetHolder dt :
                    AllStaticDataHolder.ALL_ATTRACTIONS) {
                for (HorizontalCategoryRecycleViewDatasetHolder cat :
                        cat_array) {
                    if (dt.getTag().contains(cat.getText())) {
                        if(!is_already_added(dt.getId(),arrayList)){
                           // arrayList.add(dt);
                            add_to_list(dt,arrayList);
                        }
                    }
                }
            }
        }

        return arrayList;
    }

    private void add_to_list(AttractionsGridViewDatasetHolder dt, ArrayList<AttractionsGridViewDatasetHolder> dt_list){
        AttractionsGridViewDatasetHolder dd = new AttractionsGridViewDatasetHolder();
        dd.setId(dt.getId());
        dd.setIs_wished(dt.is_wished());
        dd.setTitle(dt.getTitle());
        dd.setLocation(dt.getLocation());
        dd.setTag(dt.getTag());
        dd.setTime(dt.getTime());
        dd.setShareLink(dt.getShareLink());
        dd.setImage_link(dt.getImage_link());
        dd.setCreated_date(dt.getCreated_date());
        dd.setRating_value(dt.getRating_value());
        dd.setReviews_value(dt.getReviews_value());
        dd.setShort_des(dt.getShort_des());
        dt_list.add(dd);

    }

    private boolean is_already_added(double id, ArrayList<AttractionsGridViewDatasetHolder> dt_list){
        for (AttractionsGridViewDatasetHolder dt :
                dt_list) {
            if (id == dt.getId())
                return true;
        }
        return false;
    }

    @Override
    public void onWishListItemClick(int position, boolean isWished) {
        searchDatasetHolder.get(position).setIs_wished(isWished);

        for (AttractionsGridViewDatasetHolder dt :
                AllStaticDataHolder.ALL_ATTRACTIONS) {
            if (dt.getId() == searchDatasetHolder.get(position).getId()){
                dt.setIs_wished(isWished);
                new AddToWishListAsync(getContext(),(int)dt.getId(),isWished).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                if(mCallback != null)
                    mCallback.onSearchFragmentBookmarkedClicked();
                break;
            }
        }
    }

    @Override
    public void onAttractionClick(int position) {
        int id = (int) searchDatasetHolder.get(position).getId();
        Intent intent = new Intent(getContext(), AttractionDetail.class);
        intent.putExtra("id", id);
        startActivity(intent);
    }


    //////interface
    public interface OnSearchFragmentListener {
        public void onSearchFragmentBookmarkedClicked();
    }
}
