package Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Date;

import Adapters.AttractionsGridViewAdapter;
import Adapters.AttractionsRecycleViewAdapter;
import Adapters.HorizontalCategoryRecycleViewAdapter;
import Adapters.RecycleViewConstants;
import AllStatic.AddToWishListAsync;
import AllStatic.AllStaticDataHolder;
import AllStatic.AllStaticMethods;
import AllStatic.VectorToBitmap;
import Holder.AttractionsGridViewDatasetHolder;
import Holder.HorizontalCategoryRecycleViewDatasetHolder;
import discoverbd.robi.com.discoverbangladesh.AttractionDetail;
import discoverbd.robi.com.discoverbangladesh.HomeScreenActivity;
import discoverbd.robi.com.discoverbangladesh.R;
import discoverbd.robi.com.discoverbangladesh.SearchActivity;
import discoverbd.robi.com.discoverbangladesh.UploadActivity;
import in.srain.cube.views.GridViewWithHeaderAndFooter;


public class AttractionsHomeFragment extends Fragment implements HorizontalCategoryRecycleViewAdapter.OnHorizontalCategoryRecycleViewItemClickListener, AttractionsRecycleViewAdapter.OnAttractionsRecycleViewItemClickListener {

    RecyclerView rcyView_horizontal_locations, rcyView_attractions;
   // GridViewWithHeaderAndFooter gv_all_attractions;

   // AttractionsGridViewAdapter gridViewAdapter;

    Context context;
    OnAttractionsMainFragmentListener mCallback;

    ArrayList<HorizontalCategoryRecycleViewDatasetHolder> categoryDatasetHolder;
    ArrayList<AttractionsGridViewDatasetHolder> gridDatasetHolder;

    AttractionsRecycleViewAdapter viewAdapter;

    //variables
    private boolean is_feature_wished = false;
    // references
    //ImageButton imgBtn_feature_wishlist;

    public AttractionsHomeFragment() {
        categoryDatasetHolder = new ArrayList<>();

        gridDatasetHolder = new ArrayList<>();

        /*for(int i = 0; i<10; i++) {
            // st.add("adfsydgasifh");
            HorizontalCategoryRecycleViewDatasetHolder hrvdsh = new HorizontalCategoryRecycleViewDatasetHolder();
            hrvdsh.setId((double) i);
            if(i == 2)
                hrvdsh.setText("Hiking");
            else if(i==3)
                hrvdsh.setText("Camping");
            else if(i == 6)
                hrvdsh.setText("Nature");
            else if(i == 8)
                hrvdsh.setText("Family");
            else
                hrvdsh.setText("tag");
            hrvdsh.setSelected(true);
            categoryDatasetHolder.add(hrvdsh);
        }
*/
    }

    public void add_context(Context c){
        context = c;
        try {
            mCallback = (OnAttractionsMainFragmentListener) context;
        }
        catch (Exception e){}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_attractions_home, container, false);

       // Bitmap bitmap = VectorToBitmap.getBitmapFromVectorDrawable(getContext(),R.drawable.drawable_default_image);

       // if(gridDatasetHolder.size()>0)
         //   gridDatasetHolder.clear();


     //   Log.d("Fahim", Integer.toString(categoryDatasetHolder.size()));
         /*   AttractionsGridViewDatasetHolder agvdh = new AttractionsGridViewDatasetHolder();
            agvdh.setIs_wished(false);
            agvdh.setTitle("Andhar Nagar");
            agvdh.setLocation("Vuter Goli");
            agvdh.setTag("andhar, danger, black");
            agvdh.setShareLink(Integer.toString(i));
            agvdh.setMain_image(bitmap);
            gridDatasetHolder.add(agvdh);*/
       // }
   /*     AttractionsGridViewDatasetHolder agvdh = new AttractionsGridViewDatasetHolder();
        agvdh.setIs_wished(false);
        agvdh.setTitle("Vishon Aronno");
        agvdh.setLocation("Vuter Goli");
        agvdh.setTag("andhar, danger, black");
        agvdh.setShareLink("feature.vom");
        agvdh.setMain_image(bitmap);
        agvdh.setShort_des("the description is here");
        gridDatasetHolder.add(0,agvdh);
*/

        //references
        //imgBtn_feature_wishlist = (ImageButton)view.findViewById(R.id.imgBtn_feature_wishlist_btn);
       // imgBtn_feature_wishlist.setOnClickListener(this);

        //category
        rcyView_horizontal_locations = (RecyclerView) view.findViewById(R.id.rcyView_horizontal_locations);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rcyView_horizontal_locations.setLayoutManager(layoutManager);
        rcyView_horizontal_locations.setHasFixedSize(false);
        rcyView_horizontal_locations.setAdapter(new HorizontalCategoryRecycleViewAdapter(this,categoryDatasetHolder, 0));


        //grid view
      /*  gv_all_attractions = (GridViewWithHeaderAndFooter) view.findViewById(R.id.gv_attractions);
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View footerView = layoutInflater.inflate(R.layout.layout_footer, null);
        gv_all_attractions.addFooterView(footerView);
        gridViewAdapter = new AttractionsGridViewAdapter(getContext(),gridDatasetHolder);
        //gv_all_attractions.setAdapter(gridViewAdapter);*/





        //attractions rcyView
        rcyView_attractions = (RecyclerView) view.findViewById(R.id.rcyView_attractions);
        add_adapter_main_list();

        //if(AllStaticDataHolder.ALL_ATTRACTIONS != null)
        update_list();

        //floating action button
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), UploadActivity.class);
                startActivity(intent);


            }
        });


        return view;
    }


    @Override
    public void onHorizontalCategoryRecycleViewItemClick(int tag, int position) {
        update_list();
    }

   /* @Override
    public void onClick(View view) {
        if(view.getId() == R.id.imgBtn_feature_wishlist_btn){
            if(is_feature_wished){
                imgBtn_feature_wishlist.setImageResource(R.drawable.drawable_like_num);
                is_feature_wished = false;
            }
            else {
                imgBtn_feature_wishlist.setImageResource(R.drawable.drawable_wishlist_positive);
                is_feature_wished = true;
            }
        }
    }*/

    @Override
    public void onWishListItemClick(int position, boolean isWished) {
        gridDatasetHolder.get(position).setIs_wished(isWished);

        double id = gridDatasetHolder.get(position).getId();
        for (AttractionsGridViewDatasetHolder dt :
                AllStaticDataHolder.ALL_ATTRACTIONS) {
            if(id == dt.getId()){
                dt.setIs_wished(isWished);
                new AddToWishListAsync(getContext(),(int)id,isWished).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            }
        }
        if(mCallback != null)
            mCallback.onAttractionFragmentWishlistClicked();
    }

    @Override
    public void onAttractionClick(int position) {
        int id = (int) gridDatasetHolder.get(position).getId();
        Intent intent = new Intent(getContext(), AttractionDetail.class);
        intent.putExtra("id", id);
        startActivity(intent);
    }


    private boolean is_tag_selected(AttractionsGridViewDatasetHolder dt){
        if(AllStaticDataHolder.ALL_ATTRACTIONS_TAGS != null) {
            for (HorizontalCategoryRecycleViewDatasetHolder cat :
                    categoryDatasetHolder) {

                for (HorizontalCategoryRecycleViewDatasetHolder dt_cat :
                        dt.getTag_list()) {
                    if (dt_cat.getId() == cat.getId() && cat.getSelected())
                        return true;
                }
            }
        }
        else
            return true;
        return false;
    }

    private boolean is_already_in_list(AttractionsGridViewDatasetHolder dt){
        for (AttractionsGridViewDatasetHolder d :
                gridDatasetHolder) {
            if (d.getId() == dt.getId())
                return true;
        }
        return false;
    }

    public void update_list(){
        if(AllStaticDataHolder.ALL_ATTRACTIONS != null) {
            if (gridDatasetHolder.size() > 0)
                gridDatasetHolder.clear();

            add_header();

            for (AttractionsGridViewDatasetHolder dt :
                    AllStaticDataHolder.ALL_ATTRACTIONS) {
                if (!is_already_in_list(dt) && is_tag_selected(dt))
                    gridDatasetHolder.add(dt);
            }
            //  gridDatasetHolder = AllStaticDataHolder.ALL_ATTRACTIONS;


           // add_adapter_main_list();
            if(rcyView_attractions != null && gridDatasetHolder != null) {
                rcyView_attractions.getAdapter().notifyDataSetChanged();
            }
        }
    }

    private void add_header(){
        if(AllStaticDataHolder.FEATURED_ATTRACTION !=null){
            boolean is_exist = false;
            if(AllStaticDataHolder.ALL_ATTRACTIONS != null){
                for (AttractionsGridViewDatasetHolder dt :
                        AllStaticDataHolder.ALL_ATTRACTIONS) {
                    if(dt.getId() == AllStaticDataHolder.FEATURED_ATTRACTION.getId()){
                        is_exist = true;
                        break;
                    }
                }
                if(!is_exist) {
                    AllStaticDataHolder.ALL_ATTRACTIONS.add(0, AllStaticDataHolder.FEATURED_ATTRACTION);
                }
            }
            if(is_exist)
                gridDatasetHolder.add(0,AllStaticDataHolder.FEATURED_ATTRACTION);
        }
        else {
            AttractionsGridViewDatasetHolder agvdh = new AttractionsGridViewDatasetHolder();
            agvdh.setId(0);
            agvdh.setTitle("");
            agvdh.setShort_des("");
            agvdh.setCreated_date(new Date());
            agvdh.setTime("");
            agvdh.setRating_value(0);
            agvdh.setReviews_value(0);
            agvdh.setShareLink("");
            agvdh.setIs_checkedIn(false);
            agvdh.setImage_link("");
            agvdh.setLocation("");
            agvdh.setIs_wished(false);
            gridDatasetHolder.add(0, agvdh);
        }
    }

    private void add_adapter_main_list(){
        if(rcyView_attractions != null && gridDatasetHolder != null) {
            //  rcyView_attractions.getAdapter().notifyDataSetChanged();
            viewAdapter = new AttractionsRecycleViewAdapter(getContext(),this,gridDatasetHolder);
            GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    switch(viewAdapter.getItemViewType(position)){
                        case RecycleViewConstants.TYPE_HEADER:
                            return 2;
                        case RecycleViewConstants.TYPE_ITEM:
                            return 1;
                        case RecycleViewConstants.TYPE_FOOTER:
                            return 2;
                        default:
                            return -1;
                    }
                }
            });

            rcyView_attractions.setLayoutManager(mLayoutManager);
            rcyView_attractions.setHasFixedSize(false);
            rcyView_attractions.setAdapter(viewAdapter);

        }
    }


    public void update_tag_list(){
        if(AllStaticDataHolder.ALL_ATTRACTIONS_TAGS != null){
            if(categoryDatasetHolder.size() > 0)
                categoryDatasetHolder.clear();
            for (HorizontalCategoryRecycleViewDatasetHolder dt :
                    AllStaticDataHolder.ALL_ATTRACTIONS_TAGS) {
                categoryDatasetHolder.add(dt);
            }
            if(rcyView_horizontal_locations != null)
                rcyView_horizontal_locations.getAdapter().notifyDataSetChanged();
        }
    }

    //////interface
    public interface OnAttractionsMainFragmentListener {
        public void onAttractionFragmentWishlistClicked();
    }
}
