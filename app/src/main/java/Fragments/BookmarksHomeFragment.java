package Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import Adapters.ArticleRecycleViewAdapter;
import AllStatic.AllStaticDataHolder;
import AllStatic.VectorToBitmap;
import Holder.ArticlesRecycleViewDatasetHolder;
import discoverbd.robi.com.discoverbangladesh.ArticleDetailActivity;
import discoverbd.robi.com.discoverbangladesh.R;


public class BookmarksHomeFragment extends Fragment implements ArticleRecycleViewAdapter.OnArticleClickListener{

    RecyclerView rcyView_articleList;
    private ArrayList<ArticlesRecycleViewDatasetHolder> articleDatasetHolder;


    OnBookmarksMainFragmentListener mCallback;
    Context context;

    public BookmarksHomeFragment() {
        articleDatasetHolder = new ArrayList<>();

    }

    public void add_context(Context c){
        context = c;
        try {
            mCallback = (OnBookmarksMainFragmentListener) context;
        }
        catch (Exception e){}
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_bookmarks_home, container, false);

      /*  Bitmap bitmap = VectorToBitmap.getBitmapFromVectorDrawable(getContext(),R.drawable.drawable_default_image);

        if(articleDatasetHolder.size()>0)
            articleDatasetHolder.clear();
        for(int i = 0; i<10; i++) {
            ArticlesRecycleViewDatasetHolder arvdsh = new ArticlesRecycleViewDatasetHolder();
            arvdsh.setTitle("Are you even trying?" + Integer.toString(i));
            arvdsh.setAuthor("Wokong");
            arvdsh.setLike_num("999");
            arvdsh.setTime("10001 years ago");
            arvdsh.setShareLink(Integer.toString(i));
            arvdsh.setBookmarked(true);


            arvdsh.setMain_image(bitmap);
          //  Log.d("Fahim",Integer.toString(i));
            articleDatasetHolder.add(arvdsh);

        }
*/


        //article rcy view
        rcyView_articleList = (RecyclerView) view.findViewById(R.id.rcyView_bookmarks);
        rcyView_articleList.setLayoutManager(new LinearLayoutManager(getContext()));
        rcyView_articleList.setHasFixedSize(false);

        rcyView_articleList.setAdapter(new ArticleRecycleViewAdapter(getContext(),this,articleDatasetHolder,false));


        //if(AllStaticDataHolder.ALL_ARTICLES != null)
            update_list();

        return view;
    }


    public void update_list(){
        if(AllStaticDataHolder.ALL_ARTICLES != null) {
            if (articleDatasetHolder.size() > 0)
                articleDatasetHolder.clear();

            for (ArticlesRecycleViewDatasetHolder dt :
                    AllStaticDataHolder.ALL_ARTICLES) {
                if (dt.isBookmarked())
                    articleDatasetHolder.add(dt);
            }
            if (articleDatasetHolder != null && rcyView_articleList != null) {
                rcyView_articleList.getAdapter().notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onArticleClick(int position) {
        Intent intent = new Intent(getContext(), ArticleDetailActivity.class);
     /*   intent.putExtra("author",articleDatasetHolder.get(position).getAuthor());
        intent.putExtra("time",articleDatasetHolder.get(position).getTime());
        intent.putExtra("title",articleDatasetHolder.get(position).getTitle());
        intent.putExtra("like",articleDatasetHolder.get(position).getLike_num());
        intent.putExtra("shareLink",articleDatasetHolder.get(position).getShareLink());
        intent.putExtra("bookmarked",articleDatasetHolder.get(position).isBookmarked()); */
        intent.putExtra("id", (int) articleDatasetHolder.get(position).getId());
        startActivity(intent);
    }

    @Override
    public void onBookmarkClick(int position, boolean isBookmared) {
        double id = articleDatasetHolder.get(position).getId();
        for (ArticlesRecycleViewDatasetHolder dt :
                AllStaticDataHolder.ALL_ARTICLES) {
            if(id == dt.getId()){
                dt.setBookmarked(isBookmared);
            }
        }

        articleDatasetHolder.get(position).setBookmarked(isBookmared);
        articleDatasetHolder.remove(position);
        rcyView_articleList.getAdapter().notifyDataSetChanged();

        if(mCallback != null)
            mCallback.onBookmarksFragmentBookmarkedClicked();
    }

    //////interface
    public interface OnBookmarksMainFragmentListener {
        public void onBookmarksFragmentBookmarkedClicked();
    }
}
