package Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import Holder.AttractionsGridViewDatasetHolder;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by macpro on 12/24/16.
 */

public class WishlistRecycleViewAdapter extends RecyclerView.Adapter<WishlistRecycleViewAdapter.WishlistRecycleViewHolder> implements View.OnClickListener {

    private ArrayList<AttractionsGridViewDatasetHolder> datasetHolder;

    private Context context;
    OnWishlistRecycleViewItemClickListener mCallback;

    public WishlistRecycleViewAdapter(Context con, Fragment fragment, ArrayList<AttractionsGridViewDatasetHolder> dsh){
        datasetHolder = dsh;
        context = con;

        try {
            mCallback = (OnWishlistRecycleViewItemClickListener) fragment;
        }
        catch (Exception e){}
    }

    @Override
    public int getItemViewType(int position) {
        if(position == datasetHolder.size()){
            return RecycleViewConstants.TYPE_FOOTER;
        }
        return RecycleViewConstants.TYPE_ITEM;
    }

    @Override
    public WishlistRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if(viewType == RecycleViewConstants.TYPE_ITEM) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.wishlist_rcyview_item, parent, false);
        }
        else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_footer, parent, false);
        }

        WishlistRecycleViewHolder vh = new WishlistRecycleViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(WishlistRecycleViewHolder holder, int position) {
        switch(getItemViewType(position)){
            case RecycleViewConstants.TYPE_ITEM:
                holder.tv_title.setText(datasetHolder.get(position).getTitle());
                holder.tv_location.setText(datasetHolder.get(position).getLocation());
                holder.tv_time.setText(datasetHolder.get(position).getTime());
               // holder.iv_main.setImageBitmap(datasetHolder.get(position).getMain_image());
                Picasso.with(context)
                        .load(datasetHolder.get(position).getImage_link())
                        .into(holder.iv_main);

                holder.imgBtn_share.setTag(R.id.position,position);
                holder.imgBtn_share.setOnClickListener(this);

                holder.imgBtn_wishlist.setTag(R.id.position,position);

                if(datasetHolder.get(position).is_wished()){
                    holder.imgBtn_wishlist.setTag(R.id.selected,"yes");
                    holder.imgBtn_wishlist.setImageResource(R.drawable.drawable_wishlist_positive);
                }
                else {
                    holder.imgBtn_wishlist.setTag(R.id.selected, "no");
                    holder.imgBtn_wishlist.setImageResource(R.drawable.drawable_like_num);
                }

                holder.imgBtn_wishlist.setOnClickListener(this);

                holder.rl_main.setTag(position);
                holder.rl_main.setOnClickListener(this);

                break;
        }
    }

    @Override
    public int getItemCount() {
        return datasetHolder.size() + 1;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.imgBtn_wishlist_item_share){
            int tag = (int) view.getTag(R.id.position);
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT, datasetHolder.get(tag).getShareLink());
            context.startActivity(Intent.createChooser(share, "Share with..."));
        }
        else if(view.getId() == R.id.imgBtn_wishlist_item_wishlist) {
            ImageButton bt = (ImageButton) view;
            int tag = (int) view.getTag(R.id.position);
            if (mCallback != null) {
                String st = (String) view.getTag(R.id.selected);
                if (st.equals("no")) {
                    view.setTag(R.id.selected, "yes");
                    bt.setImageResource(R.drawable.drawable_wishlist_positive);
                    datasetHolder.get(tag).setIs_wished(true);
                    mCallback.onWishListItemClick(tag, true);
                } else {
                    view.setTag(R.id.selected, "no");
                    bt.setImageResource(R.drawable.drawable_like_num);
                    datasetHolder.get(tag).setIs_wished(false);
                    mCallback.onWishListItemClick(tag, false);
                }
            }
        }

        else if(view.getId() == R.id.layout_main){
            int tag = (int) view.getTag();
            if(mCallback != null){
                mCallback.onAttractionClick(tag);
            }
        }
    }

    public class WishlistRecycleViewHolder extends RecyclerView.ViewHolder{

        public TextView tv_title, tv_location, tv_time;
        public ImageButton imgBtn_share, imgBtn_wishlist;
        public ImageView iv_main;
        public RelativeLayout rl_main;

        public WishlistRecycleViewHolder(View itemView) {
            super(itemView);
            tv_title = (TextView) itemView.findViewById(R.id.tv_wishlist_item_title);
            tv_location =(TextView) itemView.findViewById(R.id.tv_wishlist_item_location);
            tv_time = (TextView) itemView.findViewById(R.id.tv_wishlist_item_time);
            imgBtn_share = (ImageButton) itemView.findViewById(R.id.imgBtn_wishlist_item_share);
            imgBtn_wishlist = (ImageButton) itemView.findViewById(R.id.imgBtn_wishlist_item_wishlist);
            iv_main = (ImageView) itemView.findViewById(R.id.iv_main);

            rl_main = (RelativeLayout) itemView.findViewById(R.id.layout_main);
        }
    }

    //////interface
    public interface OnWishlistRecycleViewItemClickListener {
        public void onWishListItemClick(int position, boolean isWished);
        public void onAttractionClick(int position);
    }
}
