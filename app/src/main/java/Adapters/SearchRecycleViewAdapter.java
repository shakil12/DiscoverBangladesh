package Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import Holder.AttractionsGridViewDatasetHolder;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by macpro on 12/27/16.
 */

public class SearchRecycleViewAdapter extends RecyclerView.Adapter<SearchRecycleViewAdapter.SearchRecycleViewHolder> implements View.OnClickListener {

    private ArrayList<AttractionsGridViewDatasetHolder> datasetHolder;

    OnSearchRecycleViewItemClickListener mCallback;

    Context context;

    public SearchRecycleViewAdapter(Context c,Fragment fragment, ArrayList<AttractionsGridViewDatasetHolder> dsh){
        datasetHolder = dsh;
        context = c;

        try {
            mCallback = (OnSearchRecycleViewItemClickListener) fragment;
        }
        catch (Exception e){}
    }

    @Override
    public int getItemViewType(int position) {
        if(position == datasetHolder.size()){
            return RecycleViewConstants.TYPE_FOOTER;
        }
        return RecycleViewConstants.TYPE_ITEM;
    }

    @Override
    public SearchRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if(viewType == RecycleViewConstants.TYPE_ITEM) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_rcyview_item, parent, false);
        }
        else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_footer, parent, false);
        }

        SearchRecycleViewHolder vh = new SearchRecycleViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(SearchRecycleViewHolder holder, int position) {
        switch(getItemViewType(position)){
            case RecycleViewConstants.TYPE_ITEM:
                holder.tv_name.setText(datasetHolder.get(position).getTitle());
                holder.tv_location.setText(datasetHolder.get(position).getLocation());
                holder.tv_tag.setText(datasetHolder.get(position).getTag());
                holder.tv_detail.setText(datasetHolder.get(position).getShort_des());
              //  holder.iv_main.setImageBitmap(datasetHolder.get(position).getMain_image());
                Picasso.with(context)
                        .load(datasetHolder.get(position).getImage_link())
                        .into(holder.iv_main);

                holder.imgBtn_wishlist.setTag(R.id.position,position);

                if(datasetHolder.get(position).is_wished()){
                    holder.imgBtn_wishlist.setTag(R.id.selected,"yes");
                    holder.imgBtn_wishlist.setImageResource(R.drawable.drawable_wishlist_positive);
                }
                else {
                    holder.imgBtn_wishlist.setTag(R.id.selected, "no");
                    holder.imgBtn_wishlist.setImageResource(R.drawable.drawable_like_num);
                }

                holder.imgBtn_wishlist.setOnClickListener(this);

                holder.rl_main.setTag(position);
                holder.rl_main.setOnClickListener(this);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return datasetHolder.size() + 1;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.imgBtn_wishlist_btn) {
            ImageButton bt = (ImageButton) view;
            int tag = (int) view.getTag(R.id.position);
            if (mCallback != null) {
                String st = (String) view.getTag(R.id.selected);
                if (st.equals("no")) {
                    view.setTag(R.id.selected, "yes");
                    bt.setImageResource(R.drawable.drawable_wishlist_positive);
                    datasetHolder.get(tag).setIs_wished(true);
                    mCallback.onWishListItemClick(tag, true);
                } else {
                    view.setTag(R.id.selected, "no");
                    bt.setImageResource(R.drawable.drawable_like_num);
                    datasetHolder.get(tag).setIs_wished(false);
                    mCallback.onWishListItemClick(tag, false);
                }
            }
        }

        else if(view.getId() == R.id.layout_main){
            int tag = (int) view.getTag();
            if(mCallback != null){
                mCallback.onAttractionClick(tag);
            }
        }
    }

    public class SearchRecycleViewHolder extends RecyclerView.ViewHolder {
        public ImageView iv_main;
        public ImageButton imgBtn_wishlist;
        public TextView tv_tag, tv_detail, tv_name, tv_location;
        public RelativeLayout rl_main;

        public SearchRecycleViewHolder(View itemView) {
            super(itemView);

            iv_main = (ImageView) itemView.findViewById(R.id.iv_main);
            imgBtn_wishlist = (ImageButton) itemView.findViewById(R.id.imgBtn_wishlist_btn);
            tv_tag = (TextView) itemView.findViewById(R.id.tv_tag);
            tv_detail = (TextView) itemView.findViewById(R.id.tv_detail);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_location = (TextView) itemView.findViewById(R.id.tv_location);

            rl_main = (RelativeLayout) itemView.findViewById(R.id.layout_main);
        }
    }

    //////interface
    public interface OnSearchRecycleViewItemClickListener {
        public void onWishListItemClick(int position, boolean isWished);
        public void onAttractionClick(int position);
    }
}
