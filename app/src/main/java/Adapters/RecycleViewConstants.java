package Adapters;

/**
 * Created by macpro on 12/24/16.
 */

public class RecycleViewConstants {

    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;
    public static final int TYPE_FOOTER = 2;
}
