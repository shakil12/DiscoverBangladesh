package Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

import Holder.HorizontalCategoryRecycleViewDatasetHolder;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by macpro on 12/22/16.
 */

public class HorizontalCategoryRecycleViewAdapter extends RecyclerView.Adapter<HorizontalCategoryRecycleViewAdapter.HorizontalCategoryRecycleViewHolder> implements View.OnClickListener {

   // private ArrayList<String> dataset;
    private ArrayList<HorizontalCategoryRecycleViewDatasetHolder> datasetHolder;
    private int item_tag = 0;

    OnHorizontalCategoryRecycleViewItemClickListener mCallback;

   // Context context;


    public HorizontalCategoryRecycleViewAdapter(Fragment fragment, ArrayList<HorizontalCategoryRecycleViewDatasetHolder> dsh, int i_tag)
    {
        //context = con;

        item_tag = i_tag;
        datasetHolder = dsh;
       // dataset = st;
        try {
            mCallback = (OnHorizontalCategoryRecycleViewItemClickListener) fragment;
        }
        catch (Exception e){}
    }

    public HorizontalCategoryRecycleViewAdapter(AppCompatActivity context, ArrayList<HorizontalCategoryRecycleViewDatasetHolder> dsh, int i_tag)
    {
        //context = con;

        item_tag = i_tag;
        datasetHolder = dsh;
        // dataset = st;
        try {
            mCallback = (OnHorizontalCategoryRecycleViewItemClickListener) context;
        }
        catch (Exception e){}
    }

    @Override
    public void onBindViewHolder(HorizontalCategoryRecycleViewHolder holder, int position) {
        holder.b.setText(datasetHolder.get(position).getText());
        holder.b.setTag(R.id.position,position);

        if(datasetHolder.get(position).getSelected()){
            holder.b.setTag(R.id.selected,"yes");
            holder.b.setBackgroundResource(R.drawable.horizontal_category_rcyview_item_select);
        }
        else {
            holder.b.setTag(R.id.selected, "no");
            holder.b.setBackgroundResource(R.drawable.horizontal_category_rcyview_item_disselect);
        }

        holder.b.setOnClickListener(this);
    }

    @Override
    public HorizontalCategoryRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_category_rcyview_item, parent, false);
        HorizontalCategoryRecycleViewHolder vh = new HorizontalCategoryRecycleViewHolder(v);
        return vh;
    }

    @Override
    public int getItemCount() {
        return datasetHolder.size();
    }

    @Override
    public void onClick(View view) {
        int tag = (int)view.getTag(R.id.position);
      //  Log.d("Fahim","1st");
        if(mCallback != null){
         //   Log.d("Fahim","2nd");


            String st = (String) view.getTag(R.id.selected);
            if(st.equals("no"))
            {
                view.setTag(R.id.selected,"yes");
                view.setBackgroundResource(R.drawable.horizontal_category_rcyview_item_select);
                datasetHolder.get(tag).setSelected(true);
            }
            else {
                view.setTag(R.id.selected,"no");
                view.setBackgroundResource(R.drawable.horizontal_category_rcyview_item_disselect);
                datasetHolder.get(tag).setSelected(false);
            }

            mCallback.onHorizontalCategoryRecycleViewItemClick(item_tag,tag);
        }
    }


    /////////// holder
    public static class HorizontalCategoryRecycleViewHolder extends RecyclerView.ViewHolder {

        public Button b;

        public HorizontalCategoryRecycleViewHolder(View v)
        {
            super(v);
            b = (Button) v.findViewById(R.id.btn_rcy_horizontal_itm);
            b.setBackgroundResource(R.drawable.horizontal_category_rcyview_item_disselect);
        }
    }


    //////interface
    public interface OnHorizontalCategoryRecycleViewItemClickListener {
        public void onHorizontalCategoryRecycleViewItemClick(int tag, int position);
    }
}
