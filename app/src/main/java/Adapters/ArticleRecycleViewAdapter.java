package Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import Holder.ArticlesRecycleViewDatasetHolder;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by macpro on 12/24/16.
 */

public class ArticleRecycleViewAdapter extends RecyclerView.Adapter<ArticleRecycleViewAdapter.ArticleRecycleViewHolder> implements View.OnClickListener {


    private ArrayList<ArticlesRecycleViewDatasetHolder> datasetHolder;
    private boolean is_article;

    OnArticleClickListener mCallback;

    private Context context;

    public ArticleRecycleViewAdapter(Context con,Fragment fragment, ArrayList<ArticlesRecycleViewDatasetHolder> dsh, boolean bb)
    {
        datasetHolder = dsh;
        is_article = bb;
        context = con;

        try {
            mCallback = (OnArticleClickListener) fragment;
        }
        catch (Exception e){}
    }


    @Override
    public int getItemViewType(int position) {
        if(position == datasetHolder.size()){
            return RecycleViewConstants.TYPE_FOOTER;
        }
        return RecycleViewConstants.TYPE_ITEM;
    }

    @Override
    public ArticleRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if(viewType == RecycleViewConstants.TYPE_ITEM) {
            if(is_article)
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_rcyview_item, parent, false);
            else
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.bookmark_rcyview_item, parent, false);
        }
        else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_footer, parent, false);
        }

        ArticleRecycleViewHolder vh = new ArticleRecycleViewHolder(v);
        return vh;


    }

    @Override
    public void onBindViewHolder(ArticleRecycleViewHolder holder, int position) {
        switch(getItemViewType(position)){
            case RecycleViewConstants.TYPE_ITEM:
                holder.iv_main.setTag(R.id.position,position);
                holder.iv_main.setOnClickListener(this);

                holder.tv_title.setText(datasetHolder.get(position).getTitle());
                holder.tv_author.setText(datasetHolder.get(position).getAuthor());
            //    holder.iv_main.setImageBitmap(datasetHolder.get(position).getMain_image());

                Picasso.with(context)
                        .load(datasetHolder.get(position).getImage_link())
                        .into(holder.iv_main);

                if(holder.tv_like != null)
                    holder.tv_like.setText(datasetHolder.get(position).getLike_num());
                if(holder.tv_time != null)
                    holder.tv_time.setText(datasetHolder.get(position).getTime());

                if(holder.ib_share != null) {
                    holder.ib_share.setTag(R.id.position,position);
                    holder.ib_share.setOnClickListener(this);
                }

                if(holder.ib_bookmark != null){
                    holder.ib_bookmark.setTag(R.id.position,position);

                    if(datasetHolder.get(position).isBookmarked()){
                        holder.ib_bookmark.setTag(R.id.selected,"yes");
                        holder.ib_bookmark.setImageResource(R.drawable.ic_bookmark_black_48px);
                    }
                    else {
                        holder.ib_bookmark.setTag(R.id.selected, "no");
                        holder.ib_bookmark.setImageResource(R.drawable.ic_bookmark_border_black_48px);
                    }

                    holder.ib_bookmark.setOnClickListener(this);
                }

                break;
        }

        //holder.tv_title.setText(datasetHolder.getTitle(position));
       // holder.tv_author.setText(datasetHolder.getAuthor(position));
        //holder.tv_like.setText(datasetHolder.getLike(position));
    }



    @Override
    public int getItemCount() {
        return datasetHolder.size() + 1;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.imgView_article_item_main_img)
        {
            int tag = (int)view.getTag(R.id.position);
            if(mCallback != null)
                mCallback.onArticleClick(tag);
        }
        else if(view.getId() == R.id.imgBtn_bookmark_item_share){
            int tag = (int) view.getTag(R.id.position);
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT, datasetHolder.get(tag).getShareLink());
            context.startActivity(Intent.createChooser(share, "Share with..."));
        }

        else if(view.getId() == R.id.imgBtn_article_item_bookmark) {
            ImageButton bt = (ImageButton) view;
            int tag = (int) view.getTag(R.id.position);
            if (mCallback != null) {
                String st = (String) view.getTag(R.id.selected);
                if (st.equals("no")) {
                    view.setTag(R.id.selected, "yes");
                    bt.setImageResource(R.drawable.ic_bookmark_black_48px);
                    datasetHolder.get(tag).setBookmarked(true);
                    mCallback.onBookmarkClick(tag, true);
                } else {
                    view.setTag(R.id.selected, "no");
                    bt.setImageResource(R.drawable.ic_bookmark_border_black_48px);
                    datasetHolder.get(tag).setBookmarked(false);
                    mCallback.onBookmarkClick(tag, false);
                }
            }
        }
    }


    //view holder
    public class ArticleRecycleViewHolder extends RecyclerView.ViewHolder {

        public ImageView iv_main;
        public TextView tv_author,tv_title,tv_like, tv_time;
        public ImageButton ib_bookmark, ib_share;

        public ArticleRecycleViewHolder(View v){
            super(v);

            iv_main =(ImageView) v.findViewById(R.id.imgView_article_item_main_img);
            tv_author = (TextView) v.findViewById(R.id.tv_article_item_author);
            tv_title = (TextView) v.findViewById(R.id.tv_article_item_title);
            tv_like = (TextView) v.findViewById(R.id.tv_article_item_like_num);
            ib_bookmark = (ImageButton) v.findViewById(R.id.imgBtn_article_item_bookmark);
            ib_share = (ImageButton) v.findViewById(R.id.imgBtn_bookmark_item_share);
            tv_time = (TextView) v.findViewById(R.id.tv_bookmark_item_time);
        }
    }


    //interface
    public  interface OnArticleClickListener {
        public void onArticleClick(int position);
        public void onBookmarkClick(int position, boolean isBookmared);
    }
}
