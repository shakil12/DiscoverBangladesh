package Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import Holder.AttractionsGridViewDatasetHolder;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by macpro on 12/28/16.
 */

public class AttractionsRecycleViewAdapter extends RecyclerView.Adapter<AttractionsRecycleViewAdapter.AttractionsRecycleViewHolder> implements View.OnClickListener {

    private ArrayList<AttractionsGridViewDatasetHolder> datasetHolder;

    OnAttractionsRecycleViewItemClickListener mCallback;
    Context context;

    public AttractionsRecycleViewAdapter(Context c,Fragment fragment, ArrayList<AttractionsGridViewDatasetHolder> dsh){
        context = c;
        datasetHolder = dsh;

        try {
            mCallback = (OnAttractionsRecycleViewItemClickListener) fragment;
        }
        catch (Exception e){}
    }

    @Override
    public int getItemViewType(int position) {
        if(position == datasetHolder.size()){
            return RecycleViewConstants.TYPE_FOOTER;
        }
        else if(position == 0)
            return RecycleViewConstants.TYPE_HEADER;

        return RecycleViewConstants.TYPE_ITEM;
    }

    @Override
    public AttractionsRecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if(viewType == RecycleViewConstants.TYPE_ITEM) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.attractions_gridview_item, parent, false);
        }
        else if(viewType == RecycleViewConstants.TYPE_HEADER) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.attraction_rcy_header_featureattraction, parent, false);
        }
        else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_footer, parent, false);
        }

        AttractionsRecycleViewHolder vh = new AttractionsRecycleViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(AttractionsRecycleViewHolder holder, int position) {
        switch(getItemViewType(position)){

            case RecycleViewConstants.TYPE_HEADER:
                holder.tv_title.setText(datasetHolder.get(position).getTitle());
                holder.tv_location.setText(datasetHolder.get(position).getLocation());
                holder.tv_tag.setText(datasetHolder.get(position).getTag());
                holder.tv_shortDetail.setText(datasetHolder.get(position).getShort_des());

               // holder.imgView_main.setImageBitmap(datasetHolder.get(position).getMain_image());

                holder.imgBtn_wishlist.setTag(R.id.position,position);

                if(datasetHolder.get(position).is_wished()){
                    holder.imgBtn_wishlist.setTag(R.id.selected,"yes");
                    holder.imgBtn_wishlist.setImageResource(R.drawable.drawable_wishlist_positive);
                }
                else {
                    holder.imgBtn_wishlist.setTag(R.id.selected, "no");
                    holder.imgBtn_wishlist.setImageResource(R.drawable.drawable_like_num);
                }

                holder.imgBtn_wishlist.setOnClickListener(this);

                holder.rl_main.setTag(position);
                holder.rl_main.setOnClickListener(this);

                break;

            case RecycleViewConstants.TYPE_ITEM:
                holder.tv_title.setText(datasetHolder.get(position).getTitle());
                holder.tv_location.setText(datasetHolder.get(position).getLocation());
                holder.tv_tag.setText(datasetHolder.get(position).getTag());

                Picasso.with(context)
                        .load(datasetHolder.get(position).getImage_link())
                        .into(holder.imgView_main);
               // holder.imgView_main.setImageBitmap(datasetHolder.get(position).getMain_image());

                holder.imgBtn_wishlist.setTag(R.id.position,position);

                if(datasetHolder.get(position).is_wished()){
                    holder.imgBtn_wishlist.setTag(R.id.selected,"yes");
                    holder.imgBtn_wishlist.setImageResource(R.drawable.drawable_wishlist_positive);
                }
                else {
                    holder.imgBtn_wishlist.setTag(R.id.selected, "no");
                    holder.imgBtn_wishlist.setImageResource(R.drawable.drawable_like_num);
                }

                holder.imgBtn_wishlist.setOnClickListener(this);

                holder.rl_main.setTag(position);
                holder.rl_main.setOnClickListener(this);

                break;
        }
    }

    @Override
    public int getItemCount() {
        return datasetHolder.size() + 1;
    }



    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.imgBtn_grid_item_wishlist_btn) {
            ImageButton bt = (ImageButton) view;
            int tag = (int) view.getTag(R.id.position);
            if (mCallback != null) {
                String st = (String) view.getTag(R.id.selected);
                if (st.equals("no")) {
                    view.setTag(R.id.selected, "yes");
                    bt.setImageResource(R.drawable.drawable_wishlist_positive);
                    datasetHolder.get(tag).setIs_wished(true);
                    mCallback.onWishListItemClick(tag, true);
                } else {
                    view.setTag(R.id.selected, "no");
                    bt.setImageResource(R.drawable.drawable_like_num);
                    datasetHolder.get(tag).setIs_wished(false);
                    mCallback.onWishListItemClick(tag, false);
                }
            }
        }

        else if(view.getId() == R.id.layout_main){
            int tag = (int) view.getTag();
            if(mCallback != null){
                mCallback.onAttractionClick(tag);
            }
        }
    }

    public class AttractionsRecycleViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_tag, tv_location, tv_title, tv_shortDetail;
        public ImageButton imgBtn_wishlist;
        public ImageView imgView_main;
        public RelativeLayout rl_main;

        public AttractionsRecycleViewHolder(View v) {
            super(v);

            tv_tag = (TextView) v.findViewById(R.id.tv_grid_item_tag);

            tv_location = (TextView) v.findViewById(R.id.tv_grid_item_location);

            tv_title = (TextView) v.findViewById(R.id.tv_grid_item_name);

            tv_shortDetail = (TextView) v.findViewById(R.id.tv_feature_detail);

            imgBtn_wishlist = (ImageButton) v.findViewById(R.id.imgBtn_grid_item_wishlist_btn);

            imgView_main = (ImageView) v.findViewById(R.id.iv_grid_item_main_img);

            rl_main = (RelativeLayout) v.findViewById(R.id.layout_main);
        }
    }


    //////interface
    public interface OnAttractionsRecycleViewItemClickListener {
        public void onWishListItemClick(int position, boolean isWished);
        public void onAttractionClick(int position);
    }
}
