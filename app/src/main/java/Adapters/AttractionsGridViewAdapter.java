package Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import Holder.AttractionsGridViewDatasetHolder;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by macpro on 12/22/16.
 */

public class AttractionsGridViewAdapter extends BaseAdapter {

    private ArrayList<AttractionsGridViewDatasetHolder> datasetHolder;
    private Context context;

    public AttractionsGridViewAdapter(Context con, ArrayList<AttractionsGridViewDatasetHolder> dsh){
        context = con;
        datasetHolder = dsh;
    }

    @Override
    public int getCount() {
        return datasetHolder.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(context).inflate(R.layout.attractions_gridview_item, viewGroup, false);

        TextView tv_tag = (TextView) v.findViewById(R.id.tv_grid_item_tag);
        tv_tag.setText(datasetHolder.get(i).getTag());

        TextView tv_location = (TextView) v.findViewById(R.id.tv_grid_item_location);
        tv_location.setText(datasetHolder.get(i).getLocation());

        TextView tv_title = (TextView) v.findViewById(R.id.tv_grid_item_name);
        tv_title.setText(datasetHolder.get(i).getTitle());
        return v;
    }

    public void updateList()
    {
        this.notifyDataSetChanged();
    }
}
