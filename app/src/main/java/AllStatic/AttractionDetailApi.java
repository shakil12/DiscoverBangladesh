package AllStatic;

/**
 * Created by Shakil on 31-Dec-16.
 */

public class AttractionDetailApi {
    public static final String ATTRACTION_DETAIL_API_FIRST = "http://db.softwindtech-dev.com/api/attractions/";
    public static final String ATTRACTION_DETAIL_API_LAST = "?api_token=";

    public static String getAttractionDetailApi(String attraction_id, String userToken) {
        return ATTRACTION_DETAIL_API_FIRST + attraction_id + ATTRACTION_DETAIL_API_LAST + userToken;
    }
}
