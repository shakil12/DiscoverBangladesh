package AllStatic;

import java.util.ArrayList;

import Holder.ArticlesRecycleViewDatasetHolder;
import Holder.AttractionsGridViewDatasetHolder;
import Holder.HorizontalCategoryRecycleViewDatasetHolder;

/**
 * Created by macpro on 1/2/17.
 */

public class AllStaticDataHolder {
    public static ArrayList<AttractionsGridViewDatasetHolder> ALL_ATTRACTIONS;

    public static ArrayList<ArticlesRecycleViewDatasetHolder> ALL_ARTICLES;

    public static ArrayList<HorizontalCategoryRecycleViewDatasetHolder> ALL_ATTRACTIONS_TAGS;

    public static ArrayList<HorizontalCategoryRecycleViewDatasetHolder> ALL_ARTICLES_TAGS;

    public static AttractionsGridViewDatasetHolder FEATURED_ATTRACTION;
}
