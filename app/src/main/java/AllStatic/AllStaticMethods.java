package AllStatic;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;

import com.squareup.picasso.Picasso;

import java.security.MessageDigest;
import java.util.regex.Pattern;

import Holder.AttractionsGridViewDatasetHolder;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by macpro on 12/29/16.
 */

public class AllStaticMethods {

    private static final String SHARED_PREFERENCE_NAME = "DiscoverBD";
    private static final String PREFERENCE_TOKEN = "api_token";
    private static final String PREFERENCE_NAME = "name";
    private static final String PREFERENCE_EMAIL = "email";

    public static boolean is_valid_email(String email){
        Pattern EMAIL_ADDRESS
                = Pattern.compile(
                "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                        "\\@" +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                        "(" +
                        "\\." +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                        ")+"
        );
        return EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean is_valid_userName(String name){
        Pattern USERNAME
                = Pattern.compile(
                "[a-zA-Z0-9_]{1,256}"
        );
        return USERNAME.matcher(name).matches();
    }

    public static void show_alert(Context context, String title, String des){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(des);
        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.create().show();
    }

    public static AlertDialog show_wait_alert(Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(R.layout.wait_alert_dialouge_layout);
        builder.setCancelable(false);
        AlertDialog alertDialog_wait = builder.create();
        alertDialog_wait.show();
        return alertDialog_wait;
    }


    public static void set_user_login_info(Context context, String api_token, String name, String email) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREFERENCE_TOKEN, api_token);
        editor.putString(PREFERENCE_NAME, name);
        editor.putString(PREFERENCE_EMAIL, email);
        editor.apply();
    }

    public static String get_api_token(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCE_NAME, 0);
        return preferences.getString(PREFERENCE_TOKEN, null);
    }

    public static String get_user_name(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCE_NAME, 0);
        return preferences.getString(PREFERENCE_NAME, null);
    }

    public static String get_user_email(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCE_NAME, 0);
        return preferences.getString(PREFERENCE_EMAIL, null);
    }

    public static void set_user_logout(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREFERENCE_TOKEN, null);
        editor.putString(PREFERENCE_NAME, null);
        editor.putString(PREFERENCE_EMAIL, null);
        editor.apply();
    }



    public static String get_gravatar_link(String email)
    {
        String s = email;

        try {
            // Create MD5 Hash
            MessageDigest digest;
            digest = MessageDigest.getInstance("MD5");
            digest.reset();
            digest.update(s.getBytes());
            byte[] a = digest.digest();
            int len = a.length;
            StringBuilder sb = new StringBuilder(len << 1);
            for (int i = 0; i < len; i++) {
                sb.append(Character.forDigit((a[i] & 0xf0) >> 4, 16));
                sb.append(Character.forDigit(a[i] & 0x0f, 16));
            }


            String gravatarUrl = "http://www.gravatar.com/avatar/" + sb.toString() + "?s=204&d=404";
            return gravatarUrl;

        } catch (Exception e) {
            return null;
        }
    }

    public static void add_to_wishlist(int id, boolean isWished){
        double ii = (double) id;

        if(AllStaticDataHolder.ALL_ATTRACTIONS != null){
            for (AttractionsGridViewDatasetHolder dt :
                    AllStaticDataHolder.ALL_ATTRACTIONS) {
                if(dt.getId() == ii){
                    dt.setIs_wished(isWished);
                }
            }
        }
    }

    public static void add_to_checkIn(int id, boolean isChecked){
        double ii = (double) id;

        if(AllStaticDataHolder.ALL_ATTRACTIONS != null){
            for (AttractionsGridViewDatasetHolder dt :
                    AllStaticDataHolder.ALL_ATTRACTIONS) {
                if(dt.getId() == ii){
                    dt.setIs_checkedIn(isChecked);
                }
            }
        }
    }
}
