package AllStatic;

/**
 * Created by macpro on 12/29/16.
 */

public class LoginLinks {
    public static final int RC_SIGN_IN = 9001;

    public static final String BASE_URL = "http://db.softwindtech-dev.com";

    public static final String SIGN_UP_LINK = "/api/users/store";

    public static final String LOGIN_LINK = "/api/users/login";

    public static final String LOGIN_FB = "/api/users/social_login";
}
