package AllStatic;

/**
 * Created by macpro on 1/2/17.
 */

public class ArticlesLink {
    public static final String ALL_ARTICLES_LINK = "http://db.softwindtech-dev.com/api/articles?api_token=";
    public static final String IMAGE_FILE_NAME = "/images/";
}
