package AllStatic;

/**
 * Created by macpro on 1/1/17.
 */

public class AttractionsLink {
    public static final String ALL_ATTRACTIONS_LINK = "http://db.softwindtech-dev.com/api/attractions?api_token=";
    public static final String ALL_ATTRACTIONS_TAGS_LINK = "http://db.softwindtech-dev.com/api/tags?api_token=";
    public static final String FEATURED_ATTRACTION_LINK ="";

}
