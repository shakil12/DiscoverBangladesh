package Holder;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by macpro on 12/22/16.
 */

public class AttractionsGridViewDatasetHolder {

    private double id;
    private String title, location, tag, time, shareLink, short_des, image_link;
    private boolean is_wished, is_checkedIn;
    private Bitmap main_image;
    private Date created_date;
    private int rating_value, reviews_value;

    private ArrayList<HorizontalCategoryRecycleViewDatasetHolder> tag_list;

    public AttractionsGridViewDatasetHolder(){
        tag_list = new ArrayList<>();
        tag = "";
    }

    public double getId() {
        return id;
    }

    public boolean is_checkedIn() {
        return is_checkedIn;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public void setRating_value(int rating_value) {
        this.rating_value = rating_value;
    }

    public void setReviews_value(int views_value) {
        this.reviews_value= views_value;
    }

    public String getImage_link() {
        return image_link;
    }

    public boolean is_wished() {
        return is_wished;
    }

    public Bitmap getMain_image() {
        return main_image;
    }

    public String getTitle() {
        return title;
    }

    public String getLocation() {
        return location;
    }

    public String getTag() {
        return tag;
    }

    public String getTime() {
        return time;
    }

    public String getShareLink() {
        return shareLink;
    }

    public String getShort_des() {
        return short_des;
    }


    public void setId(double id) {
        this.id = id;
    }

    public void setIs_wished(boolean is_wished) {
        this.is_wished = is_wished;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setMain_image(Bitmap main_image) {
        this.main_image = main_image;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    public void setShort_des(String short_des) {
        this.short_des = short_des;
    }

    public void setIs_checkedIn(boolean is_checkedIn) {
        this.is_checkedIn = is_checkedIn;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public int getRating_value() {
        return rating_value;
    }

    public int getReviews_value() {
        return reviews_value;
    }

    public void add_to_tag_list(HorizontalCategoryRecycleViewDatasetHolder dst){
        tag_list.add(dst);
        tag += dst.getText() + "\t";
    }

    public ArrayList<HorizontalCategoryRecycleViewDatasetHolder> getTag_list() {
        return tag_list;
    }
}
