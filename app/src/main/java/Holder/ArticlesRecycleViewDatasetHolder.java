package Holder;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by macpro on 12/24/16.
 */

public class ArticlesRecycleViewDatasetHolder {

    private double id;
    private String title, author, author_email, like_num, time, shareLink,  image_link, description;
    private boolean isBookmarked, isLiked;
    private Bitmap main_image;

    private ArrayList<HorizontalCategoryRecycleViewDatasetHolder> tag_list;

    public ArticlesRecycleViewDatasetHolder(){
        tag_list = new ArrayList<>();
    }

    public double getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public String getImage_link() {
        return image_link;
    }

    public String getShareLink() {
        return shareLink;
    }

    public Bitmap getMain_image() {
        return main_image;
    }

    public boolean isBookmarked() {
        return isBookmarked;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getAuthor_email() {
        return author_email;
    }

    public String getLike_num() {
        return like_num;
    }

    public String getTime() {
        return time;
    }

    public void setId(double id) {
        this.id = id;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    public void setBookmarked(boolean bookmarked) {
        isBookmarked = bookmarked;
    }

    public void setMain_image(Bitmap main_image) {
        this.main_image = main_image;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setAuthor_email(String author_email) {
        this.author_email = author_email;
    }

    public void setLike_num(String like_num) {
        this.like_num = like_num;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void add_to_tag_list(HorizontalCategoryRecycleViewDatasetHolder dst){
        tag_list.add(dst);
    }

    public ArrayList<HorizontalCategoryRecycleViewDatasetHolder> getTag_list() {
        return tag_list;
    }
}
