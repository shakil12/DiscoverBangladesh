package Holder;

import java.util.ArrayList;

import Adapters.HorizontalCategoryRecycleViewAdapter;

/**
 * Created by macpro on 12/22/16.
 */

public class HorizontalCategoryRecycleViewDatasetHolder {

    private double id;
    private String text;
    private Boolean isSelected;


    public double getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setId(double id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }
}
