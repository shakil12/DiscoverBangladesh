package Helper;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by Shakil-PC on 12/26/2016.
 */

public class CustomLinearLayout extends LinearLayout{

    private Context context;

    public CustomLinearLayout(Context context) {
        super(context);
    }

    public CustomLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void initLayout(Context context) {
        this.context = context;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        //Log.d("SHAKIL", "on intercept touch event and passing it to the touchevent");
        float initTouchX = 0, initTouchY = 0,
                finalX = 0, finalY = 0;
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //Log.d("SHAKIL", "from action down touchevent");
                initTouchX = ev.getRawX();
                initTouchY = ev.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                //Log.d("SHAKIL", "from action move touchevent");
                //finalY = Math.abs(ev.getRawY() - initTouchY);
                finalY = Math.abs(ev.getRawY() - initTouchY);
                //Log.d("SHAKIL", "difference in y = "+finalY+" indp"+
                //        convertPixelsToDp(finalY, getContext()));
                if(convertPixelsToDp(finalY, context) > 200) {
                    //if(getVisiblePercent(txtv_locDetails) < 100) {
                        //Log.d("SHAKIL", "now textview is not fully visible toolbar should be collapsed");
                        //mCallback.collapseToolbaronTextOutOfScreen();
                    //}
                    //Log.d("SHAKIL", "yep custom gesture detected and toolbar should be collapsed");
                    return false;
                }

                break;
            case MotionEvent.ACTION_UP:
                //Log.d("SHAKIL", "from action up touchevent");
                break;
        }
        //onTouchEvent(ev);
        return false;
    }

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }
}
