package Helper;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ScrollView;

/**
 * Created by Shakil-PC on 12/25/2016.
 */

public class CustomScrollView extends ScrollView {

    public CustomScrollView(Context context) {
        super(context);
    }

    public CustomScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onScrollChanged(int w, int h, int ow, int oh) {
        Log.d("SHAKIL", "yep scrolling");
        int diff = Math.abs(h - oh);
        if(diff>100) {
            if(h>oh) {
                Log.d("SHAKIL", "now scrolling up");
            } else {
                Log.d("SHAKIL", "now scrolling down");
            }
        }


    }
}