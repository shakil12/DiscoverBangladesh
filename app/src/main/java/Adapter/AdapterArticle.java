package Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;


import java.util.List;

import ModelClass.EmnoModel;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by Tahmidul on 6/20/2016.
 */

public class AdapterArticle extends RecyclerView.Adapter<AdapterArticle.MyViewHolder> {

    private final Activity context;
    private static List<EmnoModel> moviesList;
    private int positionPic;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView text1, text2, text3;
        private ImageView imgv1, imgv2;
        private ToggleButton tb_bookmark;

        public MyViewHolder(View view) {
            super(view);
            text1 = (TextView) view.findViewById(R.id.txtv_name);
            text2 = (TextView) view.findViewById(R.id.txtv_title);
            text3 = (TextView) view.findViewById(R.id.txtv_num);
            imgv1 = (ImageView) view.findViewById(R.id.imgv_articleLogo);
            imgv2 = (ImageView) view.findViewById(R.id.imgv_num);
            tb_bookmark = (ToggleButton) view.findViewById(R.id.tb_bookmark);
        }
    }


    public AdapterArticle(Activity context, List<EmnoModel> moviesList) {
        this.moviesList = moviesList;
        this.context = context;
        //Log.d("SHAKIL", "YEP FROM adapter article being initialized");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.listitem_article, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        EmnoModel movie = moviesList.get(position);
        String ufbid = movie.getEmnobankName();
        holder.text1.setText(ufbid);
        holder.text2.setText("Why you should visit this village ?");
        holder.text3.setText("20");
        //Log.d("SHAKIL", "YEP FROM ONbINDvIEWHOLDER OF ARTICLEaDAPTER");
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
