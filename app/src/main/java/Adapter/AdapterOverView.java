package Adapter;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.google.android.gms.vision.text.Line;

import java.util.List;

import Fragment.FragmentOverview;
import ModelClass.EmnoModel;
import ModelClass.OverviewModel;
import discoverbd.robi.com.discoverbangladesh.AttractionDetail;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by Tahmidul on 6/20/2016.
 */

public class AdapterOverView extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity context;
    private static List<OverviewModel> moviesList;
    private int positionPic;
    private ViewGroup recv;

    public static final int VIEW_TYPE_HEADER = 0;
    public static final int VIEW_TYPE_RECV = 1;

    OnItemExpanded onItemExpanded;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView text1, text2;
        private ImageView imgv1;
        private ToggleButton tb_expand;
        private LinearLayout linearv_query;

        public MyViewHolder(View view) {
            super(view);
            text1 = (TextView) view.findViewById(R.id.txtv_query);
            text2 = (TextView) view.findViewById(R.id.txtv_queryAns);
            imgv1 = (ImageView) view.findViewById(R.id.imgv_metaIcon);
            tb_expand = (ToggleButton) view.findViewById(R.id.tb_expand);
            linearv_query = (LinearLayout) view.findViewById(R.id.linearv_query);
        }
    }

    public class MyViewHolder2 extends RecyclerView.ViewHolder {
        public TextView txtv_locDetails, txtv_locDetailsFull;
        private HorizontalScrollView hscrollv_locTag;
        private LinearLayout linearv_mid;
        private ToggleButton tb_readMore;

        public MyViewHolder2(View view) {
            super(view);
            txtv_locDetails = (TextView) view.findViewById(R.id.txtv_locDetails);
            txtv_locDetailsFull = (TextView) view.findViewById(R.id.txtv_locDetailsFull);
            hscrollv_locTag = (HorizontalScrollView) view.findViewById(R.id.hscrollv_locTag);
            linearv_mid = (LinearLayout) view.findViewById(R.id.linearv_mid);
            tb_readMore = (ToggleButton) view.findViewById(R.id.tb_readMore);
        }
    }


    public AdapterOverView(Activity context, List<OverviewModel> moviesList, OnItemExpanded onItemExpandeds) {
        this.moviesList = moviesList;
        this.context = context;
        this.onItemExpanded = onItemExpandeds;
        Log.d("SHAKIL", "YEP FROM adapter article being initialized");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        recv = parent;
        if(viewType == VIEW_TYPE_RECV) {
            View itemView = LayoutInflater.from(context)
                    .inflate(R.layout.listitem_overview, parent, false);
            Log.d("SHAKIL", "width = "+parent.getMeasuredWidth());
            return new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(context)
                    .inflate(R.layout.listitem_overview_header, parent, false);

            return new MyViewHolder2(itemView);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        OverviewModel movie = moviesList.get(position);
        if(viewHolder.getItemViewType() == VIEW_TYPE_RECV) {
            String titleQuery = movie.getTitleQuery();
            String titleAns = movie.getTitleAns();
            final MyViewHolder holder = (MyViewHolder) viewHolder;
            holder.text1.setText(titleQuery);
            holder.text2.setText(titleAns);
            Glide.with(context)
                    .load(movie.getMetaIconLink())
                    .into(holder.imgv1);
            holder.linearv_query.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.tb_expand.performClick();
                }
            });
            holder.tb_expand.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b) {
                        View v = recv.getChildAt(position);
                        //Log.d("SHAKIL", " height = "+v.getMeasuredHeight()+" no of child of recv = "+recv.getChildCount());
                        holder.text2.setVisibility(View.VISIBLE);
                        Log.d("SHAKIL", "yep expnaded item pos "+position);
                        onItemExpanded.scrollToPos(position);
                        //int visible = getVisiblePercent(holder.text2);
                        //int toScroll = (holder.text2.getMeasuredHeight()/100)*(100 - visible);
                        //Log.d("SHAKIL", "yep visible = "+visible+ "toscroll = "+toScroll);
                    } else {
                        holder.text2.setVisibility(View.GONE);
                    }
                }
            });
        } else {
            String locDescription = movie.getTitleQuery();
            String locTags = movie.getTitleAns();
            final MyViewHolder2 holder = (MyViewHolder2) viewHolder;
            holder.txtv_locDetails.setText(locDescription);
            holder.txtv_locDetailsFull.setText(locDescription);
            final TextView textView = holder.txtv_locDetails;
            // readmore button
            holder.txtv_locDetails.post(new Runnable() {
                @Override
                public void run() {
                    int lineNo = holder.txtv_locDetails.getLineCount();
                    //Log.d("SHAKIL", "Yep lineno = "+lineNo);
                    if(lineNo > 3) {
                        setTbReadMore(holder.tb_readMore, textView, holder.txtv_locDetailsFull);
                    } else {
                        holder.tb_readMore.setVisibility(View.GONE);
                    }
                }
            });
            //if(textView.getLineCount() > 3) {
            /*    holder.tb_readMore.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if(b) {
                            holder.txtv_locDetails.setVisibility(View.GONE);
                            holder.txtv_locDetailsFull.setVisibility(View.VISIBLE);
                            //expandTextView(textView);
                            /*textView.post(new Runnable() {
                                @Override
                                public void run() {

                                    int height = textView.getLineCount()*textView.getLineHeight();
                                    int visible = textView.getLayout().getLineCount();
                                    int hiddenLine = textView.getLineCount() - visible;
                                    Log.d("SHAKIL", "hiddenLine = "+hiddenLine);
                                }
                            });
                        } else {
                            holder.txtv_locDetails.setVisibility(View.VISIBLE);
                            holder.txtv_locDetailsFull.setVisibility(View.GONE);
                            //collapseTextView(textView, 3);
                        }
                    }
                });*/
            //}
            String [] locTagArr = locTags.split(",");
            for(String tag : locTagArr) {
                //Log.d("SHAKIL", "tag "+tag);
                addButtonTagOnOverView(tag, holder.linearv_mid);
            }
            /*addButtonTagOnOverView("Hiking", holder.linearv_mid);
            addButtonTagOnOverView("Adventure", holder.linearv_mid);
            addButtonTagOnOverView("Mountain", holder.linearv_mid);

            addButtonTagOnOverView("Hiking", holder.linearv_mid);
            addButtonTagOnOverView("Adventure", holder.linearv_mid);
            addButtonTagOnOverView("Mountain", holder.linearv_mid);*/

        }
        //Log.d("SHAKIL", "YEP FROM ONbINDvIEWHOLDER OF ARTICLEaDAPTER");
    }

    public void setTbReadMore(ToggleButton tb_readMore, final TextView txtv_locDetails, final TextView txtv_locDetailsFull) {
        tb_readMore.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    txtv_locDetails.setVisibility(View.GONE);
                    txtv_locDetailsFull.setVisibility(View.VISIBLE);
                    //expandTextView(textView);
                            /*textView.post(new Runnable() {
                                @Override
                                public void run() {

                                    int height = textView.getLineCount()*textView.getLineHeight();
                                    int visible = textView.getLayout().getLineCount();
                                    int hiddenLine = textView.getLineCount() - visible;
                                    Log.d("SHAKIL", "hiddenLine = "+hiddenLine);
                                }
                            });*/
                } else {
                    txtv_locDetails.setVisibility(View.VISIBLE);
                    txtv_locDetailsFull.setVisibility(View.GONE);
                    //collapseTextView(textView, 3);
                }
            }
        });
    }

    private void addButtonTagOnOverView(String tagText, LinearLayout linearv_mid) {
        Button button = new Button(context);
        button.setBackgroundResource(R.drawable.rounded_btn);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams.leftMargin = 3;
        layoutParams.rightMargin = 3;
        button.setLayoutParams(layoutParams);
        button.setText(tagText);
        button.setTextSize(11);
        button.setTextColor(Color.WHITE);
        linearv_mid.addView(button);
    }

    public static int getVisiblePercent(View v) {
        if (v.isShown()) {
            Rect r = new Rect();
            v.getGlobalVisibleRect(r);
            double sVisible = r.width() * r.height();
            double sTotal = v.getWidth() * v.getHeight();
            double hidden = sTotal - sVisible;
            Log.d("SHAKIL", "yep hidden is "+(v.getMeasuredHeight() - r.height())+" measuredHeight= "+v.getMeasuredHeight()
            + " and only height = "+v.getHeight()+" and visible r height ="+r.height()+" hidden = "+hidden);
            return (int) (100 * sVisible / sTotal);
        } else {
            return -1;
        }
    }


    private void expandTextView(TextView tv) {
        ObjectAnimator animation = ObjectAnimator.ofInt(tv, "maxLines", tv.getLineCount());
        animation.setDuration(200).start();

    }

    private void collapseTextView(TextView tv, int numLines) {
        ObjectAnimator animation = ObjectAnimator.ofInt(tv, "maxLines", numLines);
        animation.setDuration(200).start();
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0)
            return VIEW_TYPE_HEADER;
        else
            return VIEW_TYPE_RECV;
    }

    public interface OnItemExpanded {
        public void scrollToPos(int pos);
    }
}
