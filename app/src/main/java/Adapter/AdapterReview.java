package Adapter;

import android.app.Activity;
import android.media.Rating;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;

import java.util.List;

import AllStatic.AllStaticMethods;
import ModelClass.EmnoModel;
import ModelClass.ReviewModel;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by Tahmidul on 6/20/2016.
 */

public class AdapterReview extends RecyclerView.Adapter<AdapterReview.MyViewHolder> {

    private final Activity context;
    private static List<ReviewModel> moviesList;
    private int positionPic;

    private AdapterCallback mAdapterCallback;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtv_uname, txtv_reviewDetail, txtv_reviewDetailFull, txtv_reviewTitle, txtv_reviewAt, txtv_upVote, txtv_downVote;
        private de.hdodenhof.circleimageview.CircleImageView imgv_upic;
        private ImageButton imbtn_share;
        private ToggleButton tb_readMore;
        private RatingBar rb_loc;

        public MyViewHolder(View view) {
            super(view);
            txtv_uname = (TextView) view.findViewById(R.id.txtv_uname);
            txtv_reviewDetail = (TextView) view.findViewById(R.id.txtv_reviewDetail);
            txtv_reviewDetailFull = (TextView) view.findViewById(R.id.txtv_reviewDetailFull);
            txtv_reviewTitle = (TextView) view.findViewById(R.id.txtv_reviewTitle);
            txtv_reviewAt = (TextView) view.findViewById(R.id.txtv_reviewAt);
            //txtv_upVote = (TextView) view.findViewById(R.id.txtv_upVote);
            //txtv_downVote = (TextView) view.findViewById(R.id.txtv_downVote);
            imgv_upic = (de.hdodenhof.circleimageview.CircleImageView) view.findViewById(R.id.imgv_upic);
            imbtn_share = (ImageButton) view.findViewById(R.id.imbtn_share);
            tb_readMore = (ToggleButton) view.findViewById(R.id.tb_readMore);
            rb_loc = (RatingBar) view.findViewById(R.id.rb_loc);
        }
    }


    public AdapterReview(Activity context, List<ReviewModel> moviesList, AdapterCallback mAdapterCallback) {
        this.moviesList = moviesList;
        this.context = context;
        this.mAdapterCallback = mAdapterCallback;
        Log.d("SHAKIL", "YEP FROM adapter article being initialized");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.listitem_review, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        ReviewModel movie = moviesList.get(position);
        String uName = movie.getuName();
        String uEmail = movie.getShareLink();
        holder.txtv_uname.setText(uName);
        holder.txtv_reviewTitle.setText(movie.getReviewTitle());
        holder.txtv_reviewDetail.setText(movie.getReviewDetails());
        holder.txtv_reviewDetailFull.setText(movie.getReviewDetails());
        holder.txtv_reviewAt.setText(movie.getReviewTime());
        holder.rb_loc.setRating(Float.parseFloat(movie.getLocRate()));
        final TextView textView = holder.txtv_reviewDetail;
        holder.txtv_reviewDetail.post(new Runnable() {
            @Override
            public void run() {
                int lineNo = holder.txtv_reviewDetail.getLineCount();
                Log.d("SHAKIL", "Yep lineno = "+lineNo);
                if(lineNo > 3) {
                    setTbReadMore(holder.tb_readMore, textView, holder.txtv_reviewDetailFull);
                } else {
                    holder.tb_readMore.setVisibility(View.GONE);
                }
            }
        });
        Glide.with(context)
                .load(AllStaticMethods.get_gravatar_link(uEmail))
                //.load("https://graph.facebook.com/" + "1030861020375813" + "/picture?\=large")
                .into(holder.imgv_upic);
        //if(position == 0) {
        //    try{
        //   mAdapterCallback.onMethodCallback();}catch (ClassCastException e) {e.printStackTrace();}
        //}
        //Log.d("SHAKIL", "YEP FROM ONbINDvIEWHOLDER OF ARTICLEaDAPTER");
    }

    public interface AdapterCallback {
        void onMethodCallback();
    }

    public void setTbReadMore(ToggleButton tb_readMore, final TextView txtv_locDetails, final TextView txtv_locDetailsFull) {
        tb_readMore.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    txtv_locDetails.setVisibility(View.GONE);
                    txtv_locDetailsFull.setVisibility(View.VISIBLE);
                } else {
                    txtv_locDetails.setVisibility(View.VISIBLE);
                    txtv_locDetailsFull.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
