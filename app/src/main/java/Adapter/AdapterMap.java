package Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.List;

import ModelClass.EmnoModel;
import ModelClass.MapModel;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by Tahmidul on 6/20/2016.
 */

public class AdapterMap extends RecyclerView.Adapter<AdapterMap.MyViewHolder> {

    private final Activity context;
    private static List<MapModel> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView text1, text2;

        public MyViewHolder(View view) {
            super(view);
            text1 = (TextView) view.findViewById(R.id.txtv_locTitle);
            text2 = (TextView) view.findViewById(R.id.txtv_locDetails);
        }
    }


    public AdapterMap(Activity context, List<MapModel> moviesList) {
        this.moviesList = moviesList;
        this.context = context;
        //Log.d("SHAKIL", "YEP FROM adapter article being initialized");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.listitem_map, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MapModel movie = moviesList.get(position);
        String locTitle = movie.getLocTitle();
        String locDetails = movie.getLocDetails();
        holder.text1.setText(locTitle);
        holder.text2.setText(locDetails);
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
