package Adapter;

import android.app.Activity;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.List;

import ModelClass.EmnoModel;
import ModelClass.GalleryModel;
import discoverbd.robi.com.discoverbangladesh.R;

/**
 * Created by Tahmidul on 6/20/2016.
 */

public class AdapterGallery extends RecyclerView.Adapter<AdapterGallery.MyViewHolder> {

    private final Activity context;
    private static List<GalleryModel> moviesList;
    private int positionPic;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgv_galleryItem;
        private ImageButton imbtn_share;

        public MyViewHolder(View view) {
            super(view);
            imgv_galleryItem = (ImageView) view.findViewById(R.id.imgv_galleryItem);
            imbtn_share = (ImageButton) view.findViewById(R.id.imbtn_share);
        }
    }


    public AdapterGallery(Activity context, List<GalleryModel> moviesList) {
        this.moviesList = moviesList;
        this.context = context;
        //Log.d("SHAKIL", "YEP FROM adapter article being initialized");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.listitem_gallery, parent, false);
        int height = parent.getMeasuredHeight() / 4;
        itemView.setMinimumHeight(height);
        Log.d("SHAKIL", "yep width "+height);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        GalleryModel movie = moviesList.get(position);
        String ufbid = movie.getGalleryId();
        String shareLink = movie.getShareLink();
        //Log.d("SHAKIL", "YEP FROM ONbINDvIEWHOLDER OF ARTICLEaDAPTER");
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
