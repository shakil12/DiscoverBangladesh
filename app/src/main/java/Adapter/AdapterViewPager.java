package Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import Fragment.FragmentOverview;
import Fragment.FragmentGallery;
import Fragment.FragmentArticle;
import Fragment.FragmentMap;
import Fragment.FragmentReview;

/**
 * Created by Shakil on 22-Dec-16.
 */

public class AdapterViewPager extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private static HashMap<Integer, Fragment>  refFrag = new HashMap<>();

    private FragmentOverview fragmentOverview;
    private FragmentReview fragmentReview;
    private FragmentGallery fragmentGallery;
    private FragmentMap fragmentMap;
    private FragmentArticle fragmentArticle;

    public AdapterViewPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;
        switch (position) {
            case 0:
                frag = FragmentOverview.newInstance();
                break;
            case 1:
                frag = FragmentReview.newInstance();
                break;
            case 2:
                frag = FragmentGallery.newInstance();
                break;
            case 3:
                frag = FragmentMap.newInstance();
                break;
            case 4:
                frag = FragmentArticle.newInstance();
                break;

        }
        refFrag.put(position, frag);
        return frag;
    }

    public void addFrag(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public Fragment getFragment(int key) {
        return refFrag.get(key);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        refFrag.remove(position);
    }

    // Here we can finally safely save a reference to the created
    // Fragment, no matter where it came from (either getItem() or
    // FragmentManger). Simply save the returned Fragment from
    // super.instantiateItem() into an appropriate reference depending
    // on the ViewPager position.
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
        // save the appropriate reference depending on position
        switch (position) {
            case 0:
                fragmentOverview = (FragmentOverview) createdFragment;
                break;
            case 1:
                fragmentReview = (FragmentReview) createdFragment;
                break;
            case 2:
                fragmentGallery = (FragmentGallery) createdFragment;
                break;
            case 3:
                fragmentMap = (FragmentMap) createdFragment;
                break;
            case 4:
                fragmentArticle = (FragmentArticle) createdFragment;
                break;
            default:
                break;
        }
        return createdFragment;
    }

    public void setOtherLayout(boolean collapse, int pos) {
        //Log.d("SHAKIL", "yep setOtherLayout is called with collapse = "+collapse);
        if(fragmentOverview != null && pos == 0) {
            //fragmentOverview.collapseOtherLayout(collapse);
            //Log.d("SHAKIL", "yep fragment overview is not null ");
        } else if (fragmentReview != null && pos == 1) {
            fragmentReview.collapseOtherLayoutReview(collapse);
        }
    }


    /**
     * @param pos is not meaning anything
     *            from this function i can get the active fragments reference both
     *            are now selected and also which are not selected but now active means which
     *            are ready to attach data to their views as their onCreateView and
     *            onActivityCreated has
     *            got called
     *            fragment != null means active fragments whose onCreateView and onActivityCreated is finished
     */
    public void setDataOnFragment(int pos) {
        Log.d("SHAKIL", "yep setDataOnFragment is called");
        if(fragmentOverview != null) {
            //Log.d("SHAKIL", "yep fragment overview is not null");
            fragmentOverview.attachDataToView();
        }
        if (fragmentReview != null) {
            //Log.d("SHAKIL", "yep fragment fragmentReview is not null");
            fragmentReview.attachDataToView();
        }
        if (fragmentGallery != null) {
            fragmentGallery.attachDataToView();
        }
        if (fragmentMap != null) {
            fragmentMap.attachDataToView();
        }
        if (fragmentArticle != null) {
            fragmentArticle.attachDataToView();
        }
    }

}
